/*
 * Grownup Software Limited.
 */
package gusl.doc.generate.extensions;

import gusl.doc.BaseDocumentor;
import gusl.doc.BlastEvent;
import gusl.doc.utils.DocUtils;
import org.asciidoctor.ast.StructuralNode;
import org.asciidoctor.extension.Reader;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;

import static gusl.doc.BaseDocumentor.*;

/**
 *
 * @author dhudson - Apr 19, 2017 - 11:14:04 AM
 */
public class EventMacro extends AbstractMacro {

    public EventMacro(String name, Map<String, Object> config) {
        super(name, config);
    }

    @Override
    public Object process(StructuralNode parent, Reader reader, Map<String, Object> attributes) {

        logger.info("Event Macro ... {}", attributes);

        // Lets see if its a source or a class
        String className = (String) attributes.get("class");
        if (className != null) {

            String content = generateClassContent(className);
            return createBlock(parent, "pass", Arrays.asList(content), attributes);
        }

        generateTypeContent(parent, (String) attributes.get("source"));
        return null;
//        return createBlock(parent, "pass", Arrays.asList(content), attributes);
    }

    private void generateTypeContent(StructuralNode parent, String type) {
        Set<Class<?>> classes = DocUtils.getAnnotatedClasses("blast", BlastEvent.class);
        StringBuilder builder = new StringBuilder(400);

        builder.append("Events generated from ").append(BOLD).append(type.toUpperCase()).append(BOLD).append(NL);
        builder.append(BaseDocumentor.BEGIN_ANCHOR);
        builder.append(type.toUpperCase());
        builder.append("-events");
        builder.append(BaseDocumentor.END_ANCHOR);
        builder.append(NL);

        //parseContent(parent, Arrays.asList(builder.toString().split("\n")));
//        builder.append("<caption class=\"title\">Events generated from <strong>");
//        builder.append(type.toUpperCase());
//        builder.append("</strong>");
//        builder.append("</caption>");
//        builder.append(NL);
//        builder.append("<br></br>");
        builder.append(OPEN_TABLE).append(NL);
        builder.append(HDR_COL).append("Class")
                .append(HDR_COL).append("Description")
                .append(NL);

//        builder.append("<table style=\"width:100%\">");
//        builder.append("<tr><th>Classs</th>");
//        builder.append("<th>Description</th><tr>");
//        builder.append(NL);
        for (Class<?> clazz : classes) {
            BlastEvent anno = clazz.getAnnotation(BlastEvent.class);
            if (anno.source().toString().equalsIgnoreCase(type)) {
                builder.append(docEvent(clazz, anno));
            }
        }
        builder.append(CLOSE_TABLE).append(NL);

        parseContent(parent, Arrays.asList(builder.toString().split("\n")));

    }

    private String generateClassContent(String className) {
        try {
            Class<?> clazz = Class.forName(className);
            BlastEvent annotation = clazz.getAnnotation(BlastEvent.class);
            return docEvent(clazz, annotation);
        } catch (ClassNotFoundException ex) {
            logger.warn("Can't find class {}", className);
        }
        return "";
    }

    private String docEventOld(Class event, BlastEvent annotation) {
        StringBuilder builder = new StringBuilder(100);
        builder.append("<tr>").append("<td width=\"25%\">").append(event.getSimpleName()).append("</td>");
        builder.append("<td width=\"75%\">").append(annotation.description()).append("</td>").append("</tr>").append(NL);
        return builder.toString();
    }

    private String docEvent(Class event, BlastEvent annotation) {
        StringBuilder builder = new StringBuilder();
        // Style
        builder.append("a");
        builder.append(COL);
        builder.append(BaseDocumentor.BEGIN_ANCHOR);
        builder.append(event.getSimpleName());
        builder.append(BaseDocumentor.END_ANCHOR);
        builder.append(event.getSimpleName());
        builder.append(NL).append(NL);

        for (Method method : event.getMethods()) {
            String methodName = method.getName();
            if (methodName.equals("getClass")) {
                continue;
            }

            if (methodName.startsWith("get") || methodName.startsWith("is")) {
                builder.append(" * ");

                Type type = method.getGenericReturnType();
                String typeName = type.getTypeName();
                if (typeName.startsWith("java.lang") || typeName.startsWith("java.util")) {
                    typeName = typeName.substring(10);
                }

                builder.append(typeName);
                builder.append(" ");
                builder.append(method.getName());
                builder.append("()");
                builder.append(NL);
            }
        }

        builder.append(COL).append(annotation.description());
        builder.append(NL);

        return builder.toString();

    }
}
