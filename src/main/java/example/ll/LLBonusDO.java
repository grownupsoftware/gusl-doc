package example.ll;

import gusl.core.annotations.DocField;
import lombok.*;

import java.util.Date;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class LLBonusDO {

    @DocField(description = "Unique id")
    private Long id;

    @DocField(description = "Bonus name")
    private String name;

    private Long ruleBookId;

    private String headerText;

    private String imageUrl;

    private String logoUrl;

    private String strapLine;

    private String articleUrl;

    private Integer rank;

    private Money minimumDeposit;

    private Long wageringProfileId;

    private Integer validity;

    private Money conversionCap;

    private Money minStake;

    private Money maxStake;

    private LLBonusWithdrawAction withdrawAction;

    private Boolean withdrawNullification;

    private LLBonusRedemptionType redemptionType;

    private Integer redemptionLimit;

    private LLPeriod periodUnit;

    private Integer periodLimit;


    private Date startDate;

    private Date endDate;


    private LLBonusStatus status;

    private Boolean useAmount;

    private Money amount;

    private Double percentageOfDeposit;

    private Money valueCap;

    private LLBonusRangesDO percRanges;

    private Money minBonusIncrement;

    private Money autoConversion;

    private Long targetMultiple;

    private String salientTerms;

    private LLValidityType validityType;

    private Boolean limitExclusion;

    private LLLimitExclusionType limitExclusionType;

    private Boolean excludeOtherPromos;

    private Boolean excludeOtherBonus;

    private Integer displayOrder;

/*
    private Boolean fasttrackViewable;
    private MarketingType marketingType;
    private Integer fasttrackDaysToClaim;
    private PromotionItemDO promotionItem;
    private PromotionBannerDO banner;
*/

}
