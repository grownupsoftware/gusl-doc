package gusl.doc.generate.builders;

import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.core.utils.Utils;
import gusl.doc.generate.extensions.*;
import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Attributes;
import org.asciidoctor.Placement;
import org.asciidoctor.SafeMode;
import org.asciidoctor.extension.JavaExtensionRegistry;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import static org.asciidoctor.Asciidoctor.Factory.create;
import static org.asciidoctor.OptionsBuilder.options;

public class AbstractDocBuilder {

    public static final GUSLLogger logger = GUSLLogManager.getLogger(AbstractDocBuilder.class);
    public static final String DOC_SRC_FOLDER = "doc";

    protected void document(String folder, String title, DocumentationType documentationType) {
        document(folder, title, documentationType, null, null);
    }

    protected void document(String folder, String title, DocumentationType documentationType, String outFileName, String outputDir) {
        document(folder, title, documentationType, null, outFileName, outputDir);
    }

    protected void document(String folder, String adocFile, String title, DocumentationType documentationType, String outFileName, String outputDir) {
        File destFolder = new File(outputDir + File.separator + folder);
        convertFile(DOC_SRC_FOLDER + File.separator + folder + File.separator + adocFile.replace(".adoc", "") + ".adoc", title, destFolder, documentationType, outFileName, outFileName);
    }

    protected void document(String folder, String adocFile, String title, DocumentationType documentationType, String outFileName, String outputDir, boolean tableOfContents) {
        File destFolder = new File(outputDir + File.separator + folder);
        convertFile(DOC_SRC_FOLDER + File.separator + folder + File.separator + adocFile.replace(".adoc", "") + ".adoc", title, destFolder, documentationType, outFileName, outFileName,tableOfContents);
    }

    protected void document(String folder, String title, DocumentationType documentationType, String indexDoc, String outFileName, String outputDir) {
        File destFolder = new File(outputDir + File.separator + folder);

        // note: parent adoc must match the name of the folder e.g. download equals download/download.adoc
        if (indexDoc == null) {
            convertFile(DOC_SRC_FOLDER + File.separator + folder + File.separator + folder + ".adoc", title, destFolder, documentationType, outFileName);
        } else {
            convertFile(DOC_SRC_FOLDER + File.separator + folder + File.separator + indexDoc.replace(".adoc", "") + ".adoc", title, destFolder, documentationType, outFileName);
        }
    }

    protected void convertContent(StringBuilder content, String title, File destFile, String outputDir, DocumentationType... documentationTypes) {
        Utils.safeStream(documentationTypes).forEach(documentationType -> {

            Asciidoctor asciidoctor = createDoctor();
            Map<String, Object> options = createOptions(title, destFile, documentationType, outputDir);

            switch (documentationType) {
                case PDF:
                    options.put("to_file", "modules.pdf");
                    break;
                case HTML:
                case HTML_DECK:
                    options.put("to_file", "modules.html");
                    break;
                default:
                    throw new AssertionError(documentationType.name());
            }

            asciidoctor.convert(content.toString(), options);
        });
    }

    protected void convertFile(String srcFile, String title, File destFile, DocumentationType documentationType, String outputDir) {
        convertFile(srcFile, title, destFile, documentationType, null, outputDir,true);
    }

    protected void convertFile(String srcFile, String title, File destFile, DocumentationType documentationType, String outputDir, boolean tableOfContents) {
        convertFile(srcFile, title, destFile, documentationType, null, outputDir,tableOfContents);
    }

    protected void convertFile(String srcFile, String title, File destFile, DocumentationType documentationType, String outFileName, String outputDir) {
        convertFile(srcFile, title, destFile, documentationType, outFileName, outputDir,true);
    }

    protected void convertFile(String srcFile, String title, File destFile, DocumentationType documentationType, String outFileName, String outputDir, boolean tableOfContents) {
        Asciidoctor asciidoctor = createDoctor();
        Map<String, Object> options = createOptions(title, destFile, documentationType, outputDir,tableOfContents);

        File aDoc = new File(srcFile);
        if (aDoc.exists()) {
            logger.info("-- converting: {}", aDoc.getName());
            asciidoctor.convertFile(aDoc, options);

            if (documentationType == DocumentationType.HTML || documentationType == DocumentationType.PDF) {
                String suffix = ".html";
                if (documentationType == DocumentationType.PDF) {
                    suffix = ".pdf";
                }

                final File out = new File(destFile, aDoc.getName().replace(".adoc", "") + suffix);

                logger.info("File: {} exist: {}", out.getAbsolutePath(), out.exists());

                if (out.exists()) {
                    File newFile = new File(destFile, outFileName + suffix);

                    logger.info("new file: {}", newFile.getAbsolutePath());
                    out.renameTo(newFile);
                }
            }

        } else {
            logger.error("file does not exist: {}", aDoc.getAbsoluteFile());
        }
    }
    protected Map<String, Object> createOptions(String title, File destFile, DocumentationType documentationType, String outputDir) {
        return createOptions(title,destFile,documentationType,outputDir,true);
    }

    protected Map<String, Object> createOptions(String title, File destFile, DocumentationType documentationType, String outputDir, boolean tableOfContents) {
//        String rootPrefix = calcRootPrefix(theOutputDir, destFile);
        String rootPrefix = calcRootPrefix(outputDir, destFile);

        Attributes attributes = new Attributes();
        if (documentationType != DocumentationType.HTML_DECK) {
            if (tableOfContents) {
                attributes.setTableOfContents(true);
                attributes.setTableOfContents2(Placement.LEFT);
            } else {
                attributes.setTableOfContents(false);
            }
            attributes.setLinkCss(true);
        }
        attributes.setTitle(title);

        Map<String, Object> attributeMap = attributes.map();
        if (documentationType == DocumentationType.HTML_DECK) {
            attributeMap.put("deckjsdir", rootPrefix + "js/deck.js");
        }

        attributeMap.put("icons", "font");
        // attributeMap.put("stylesdir", rootPrefix + "css");
        attributeMap.put("stylesdir", "https://npm.lottomart.app/npmpkg/css/");
        attributeMap.put("stylesheet", "lottomart-asciidoctor.css");

        attributeMap.put("source-highlighter", "coderay");
        if (tableOfContents) {
            attributeMap.put("toc-title", "Lottomart");
            attributeMap.put("toclevels", "3");
        }
        attributeMap.put(":docinfo:", "shared");
        attributeMap.put(":pdf-page-layout:", "landscape");


        Map<String, Object> options = options()
                .safe(SafeMode.UNSAFE)
                .toDir(destFile)
                .mkDirs(true)
                .attributes(attributes)
                .attributes(attributeMap)
                .option("backend", documentationType.getType())
                .asMap();

        if (documentationType == DocumentationType.HTML_DECK) {
            List<String> templateDirs = new ArrayList<>();
            templateDirs.add("./doc/templates/haml");
            options.put("template_dirs", templateDirs);
        }

        return options;
    }

    protected Asciidoctor createDoctor() {
        Asciidoctor asciidoctor = create();

        JavaExtensionRegistry extensionRegistry = asciidoctor.javaExtensionRegistry();

        extensionRegistry.preprocessor(ChangeAttributeValuePreprocessor.class);

        extensionRegistry.block("tree", TreeMacro.class);
        extensionRegistry.block("dir_listing", NewDirListing.class);
        extensionRegistry.block("yuml", YumlMacro.class);
        extensionRegistry.block("web_sequence", WebSequenceDiagramsMacro.class);

        // use deprecated functionality - noy used
//        extensionRegistry.block("epic", EpicMacro.class);
//        extensionRegistry.block("epic_list", EpicListMacro.class);
//        extensionRegistry.block("story", JiraStoryMacro.class);
//        extensionRegistry.block("ticket", JiraTicketMacro.class);

        extensionRegistry.block("ui_table", UiTableMacro.class);

        extensionRegistry.block("field_doc", FieldsExtension.class);
        extensionRegistry.block("angular_class", AngularClassMacro.class);
        extensionRegistry.block("angular_all", AngularAllMacro.class);
        extensionRegistry.block("rest_doc", RestExtension.class);
        extensionRegistry.block("event_doc", EventMacro.class);
        extensionRegistry.block("example_doc", ExamplesExtension.class);
        extensionRegistry.block("angular_localised", AngularLocalisedMacro.class);

        return asciidoctor;
    }

    /**
     * 3rd party js libraries are in the web sites root folder - need to
     * calculate ".." to get back to root folder
     *
     * @param outputDir
     * @param destFile
     * @return
     */
    private String calcRootPrefix(String outputDir, File destFile) {

        File outDir = new File(outputDir);
        String dest = destFile.getAbsolutePath().replace(outDir.getAbsolutePath(), "");

        StringTokenizer stOR = new StringTokenizer(dest, "/");

        StringBuilder builder = new StringBuilder();
        for (int x = 0; x < stOR.countTokens(); x++) {
            builder.append("../");
        }

        logger.info("outputDir: {} destFile: {} builder: {}", outputDir, destFile.getAbsolutePath(), builder.toString());

        return builder.toString();
    }

}
