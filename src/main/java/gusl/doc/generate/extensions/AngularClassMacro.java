/* Copyright lottomart */
package gusl.doc.generate.extensions;

import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import org.asciidoctor.ast.StructuralNode;
import org.asciidoctor.extension.Reader;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static gusl.doc.BaseDocumentor.BOLD;

/**
 *
 * @author grant
 */
public class AngularClassMacro extends AbstractAngular {

    public static final GUSLLogger logger = GUSLLogManager.getLogger(AngularClassMacro.class);

    private static String UI_HOME;

    public AngularClassMacro(String name, Map<String, Object> config) {
        super(name, config);
    }

    public static void setUiHome(String uiHome) {
        UI_HOME = new File(uiHome).getAbsolutePath() + "/src";
    }

    @Override
    public Object process(StructuralNode parent, Reader reader, Map<String, Object> attributes) {

        String sourceCode = (String) attributes.get("code");

        StringBuilder builder = new StringBuilder();

        String[] lines = readFile(UI_HOME + File.separator + sourceCode);

        // logger.info("File: {} SourceCode: {} Lines: {}", (UI_HOME + File.separator + sourceCode), sourceCode, lines.length);
        // start - source block
        builder.append("[source,java,role=\"angular\"]").append(NL).append("====").append(NL);

        builder.append(BOLD).append("SOURCE CODE").append(BOLD).append(NL).append(NL);

        builder.append(BOLD).append("Source: ").append(sourceCode).append(BOLD).append(NL).append(NL);

        if (lines.length == 0) {
            builder.append("CAUTION: source code file not found [").append(sourceCode).append("]").append(NL).append(NL);
        } else {

            for (int x = 0; x < lines.length; x++) {
                if (lines[x].indexOf("@constructor") > 0) {
                    doMethod(builder, lines, x);
                }
                if (lines[x].indexOf("@type") > 0) {
                    doType(builder, lines, x);
                }
                if (lines[x].indexOf("@name") > 0) {
                    doName(builder, lines, x);
                }
                if (lines[x].indexOf("@method") > 0) {
                    doMethod(builder, lines, x);
                }
            }
        }

        // end - source block
        builder.append("====").append(NL).append(NL).append(NL);

        List<String> content = new ArrayList<>();

        content.addAll(Arrays.asList(builder.toString().split("\n")));

        parseContent(parent, content);

        return null;
    }

    private String[] readFile(String filename) {
        try {
            Path filePath = new File(filename).toPath();
            Charset charset = Charset.defaultCharset();
            List<String> stringList = Files.readAllLines(filePath, charset);
            String[] stringArray = stringList.toArray(new String[]{});
            return stringArray;

        } catch (IOException ex) {
            return new String[0];
        }
    }

}
