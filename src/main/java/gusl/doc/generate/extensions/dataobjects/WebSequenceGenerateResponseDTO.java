/* Copyright lottomart */
package gusl.doc.generate.extensions.dataobjects;

import java.util.List;

/**
 *
 * @author grant
 */
public class WebSequenceGenerateResponseDTO {

    private String img;

    private List<String> errors;

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    @Override
    public String toString() {
        return "WebSequenceGenerateResponseDTO{" + "img=" + img + ", errors=" + errors + '}';
    }

}
