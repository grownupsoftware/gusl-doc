package example.ll;

import gusl.core.annotations.DocField;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Money {
    @DocField(description = "Currency")
    private String currency;

    @DocField(description = "Value")
    private Long value;
}
