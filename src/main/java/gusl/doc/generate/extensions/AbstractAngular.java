/* Copyright lottomart */
package gusl.doc.generate.extensions;

import gusl.core.utils.Utils;
import gusl.doc.generate.extensions.angular.AngularMethod;
import gusl.doc.generate.extensions.angular.AngularParam;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static gusl.doc.BaseDocumentor.*;

/**
 *
 * @author grant
 */
public abstract class AbstractAngular extends AbstractMacro {

    public AbstractAngular(String name, Map<String, Object> config) {
        super(name, config);
    }

    protected void doType(StringBuilder builder, String[] lines, int x) {
        int startIndex = lines[x].indexOf("@type");
        String type = lines[x].substring(startIndex + 5);
        
        // NOTE - not adding type
        //builder.append("Type: ").append(type).append(NL).append(NL);

//        builder
//                .append(HDR_COL).append("Type")
//                .append(HDR_COL).append(type)
//                .append(NL);
    }

    protected void doName(StringBuilder builder, String[] lines, int x) {
        int startIndex = lines[x].indexOf("@name");
        String typeName = lines[x].substring(startIndex + 5);

        builder.append("Component: ").append(typeName).append(NL).append(NL);
//        builder
//                .append(HDR_COL).append("Component")
//                .append(HDR_COL).append(typeName)
//                .append(NL);
    }

    protected void doDescription(StringBuilder builder, boolean isHeader, String[] lines, int x) {
        int startIndex = lines[x].indexOf("@description");
        String description = lines[x].substring(startIndex + 12);
        if (isHeader) {
            builder.append("Description: ").append(description).append(NL).append(NL);
//            builder
//                    .append(HDR_COL).append("Description")
//                    .append(HDR_COL).append(description)
//                    .append(NL);
        } else {
            builder.append(HDR_COL).append(description);
        }
    }

    protected void doConstructor(StringBuilder builder, String[] lines, int x) {
        buildMethod(true, builder, lines, x);
    }

    protected void doMethod(StringBuilder builder, String[] lines, int x) {
        buildMethod(false, builder, lines, x);
    }

    private void buildMethod(boolean isConstructor, StringBuilder builder, String[] lines, int x) {
        AngularMethod angularMethod = getMethod(lines, x);

        if (isConstructor) {
            builder.append(BOLD).append("Constructor").append(BOLD).append(NL).append(NL);
        } else {
            builder.append(BOLD).append("Method: ").append(angularMethod.getName()).append(BOLD).append(NL).append(NL);
        }

        if (angularMethod.getDescription() != null && !angularMethod.getDescription().isEmpty()) {
            builder.append("Description: ").append(angularMethod.getDescription()).append(NL).append(NL);
        }
        if (angularMethod.getReturnType() != null && (angularMethod.getReturnType().getParam() != null && !angularMethod.getReturnType().getParam().isEmpty())) {
            builder.append("Return: ").append(angularMethod.getReturnType().getParam()).append(NL).append(NL);
        }

        if (angularMethod.getParams() != null && !angularMethod.getParams().isEmpty()) {
            builder.append("[options=\"header\",cols=\"50%,50%\"]\n|===").append(NL);
            builder
                    .append(HDR_COL).append("Param")
                    .append(HDR_COL).append("Type")
                    .append(NL);

            Utils.safeStream(angularMethod.getParams()).forEach(param -> {
                builder
                        .append(HDR_COL).append(param.getName())
                        .append(HDR_COL).append(param.getParam())
                        .append(NL);

            });

            builder.append(CLOSE_TABLE).append(NL);

        }

    }

    protected AngularMethod getMethod(String[] lines, int x) {
        AngularMethod angularMethod = new AngularMethod();
        angularMethod.setDescription(getDescription(lines, x));

        String content = getAllToBraces(lines, x);

        angularMethod.setName(content.substring(0, content.indexOf("(")).trim());
        if (content.indexOf("(") > 0 && content.indexOf(")") > 0) {
            angularMethod.setParams(getParams(content.substring(content.indexOf("("), content.indexOf(")"))));
        } else {
            // logger.info("=========== A: {}", content, content.indexOf("("), content.indexOf(")"));
        }

        if (content.indexOf(")") > 0) {
            angularMethod.setReturnType(getParam(content.substring(content.indexOf(")"))));
        } else {
            // logger.info("=========== B: {}", content, content.indexOf(")"));
        }

        return angularMethod;

    }

    private String getDescription(String[] lines, int x) {
        StringBuilder builder = new StringBuilder();
        boolean processing = false;
        for (int y = x + 1; y < lines.length; y++) {

            if (lines[y].indexOf("@description") > 0) {
                processing = true;
            }

            if (lines[y].indexOf("*/") > 0) {
                if (processing) {
                return builder.substring(builder.indexOf("@description") + 12).replace("*", "").trim();
                } else {
                    return null;
                }
            } else {
                if (processing) {
                    builder.append(lines[y]);
                }
            }
        }
        return null;
    }

    private String getAllToBraces(String[] lines, int x) {
        int firstCodeLine = getIndexOfFirstLineAfterEndOfComment(lines, x);
        StringBuilder builder = new StringBuilder();
        if (firstCodeLine > 0) {

            for (int y = firstCodeLine; y < lines.length; y++) {
                if (lines[y].indexOf("{") > 0) {
                    builder.append(lines[y].substring(0, lines[y].indexOf("{")));
                    break;
                } else {
                    builder.append(lines[y]);
                }
            }
        }
        return builder.toString();
    }

    private int getIndexOfFirstLineAfterEndOfComment(String[] lines, int x) {

        for (int y = x; y < lines.length; y++) {
            if (lines[y].indexOf("*/") > 0) {
                return y + 1;
            }
        }

        return -1;

    }

    private AngularMethod getMethodParamsAsContent(String[] lines, int firstCodeLine, int bracketStartIndex) {
        AngularMethod angularMethod = new AngularMethod();
        StringBuilder builder = new StringBuilder();
        for (int y = firstCodeLine; y < lines.length; y++) {
            if (lines[y].indexOf(")") > 0) {
                builder.append(lines[y].substring(0, lines[y].indexOf(")")));

                List<AngularParam> params = getParams(builder.substring(bracketStartIndex + 1));
                angularMethod.setParams(params);

                if (lines[y].indexOf("{") > 0) {
                    AngularParam returnType = getParam(lines[y].substring(lines[y].indexOf(")"), lines[y].indexOf("{")));
                    angularMethod.setReturnType(returnType);

                }

                return angularMethod;
            } else {
                builder.append(lines[y].replace("\n", ""));
            }
        }
        return angularMethod;
    }

    private List<AngularParam> getParams(String content) {

        List<AngularParam> list = new ArrayList<>();

        String[] params = content.split(",");
        for (String param : params) {
            AngularParam angularParam = getParam(param);
            if (angularParam.getName() != null & !angularParam.getName().isEmpty()) {
                list.add(angularParam);
            }
        }
        return list;

    }

    private AngularParam getParam(String content) {
        String[] elements = content.split(":");

        String paramName = "";
        if (elements.length > 0) {
            paramName = elements[0].replace("(", "").trim();
        }
        String paramType = "";
        if (elements.length > 1) {
            paramType = elements[1].trim();
        }
        return new AngularParam(paramName, paramType);
    }

}
