/* Copyright lottomart */
package example.lm.dataobjects.registration;

import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@DocClass(description = "Currency Information for player registration")
public class CurrencyRegistrationInfoDTO {

    @DocField(description = "Currency Name")
    private String name;

    @DocField(description = "Currency Code")
    private String code;

    @DocField(description = "Currency Symbol")
    private String symbol;

    @Override
    public String toString() {
        return "CurrencyRegistrationInfoDTO{" + "name=" + name + ", code=" + code + ", symbol=" + symbol + '}';
    }

}
