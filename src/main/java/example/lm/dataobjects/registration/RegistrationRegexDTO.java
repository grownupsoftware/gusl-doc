package example.lm.dataobjects.registration;

import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;
import gusl.validators.ValidatorsRegex;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@AllArgsConstructor
@Getter
@Setter
@DocClass(description = "Registration UI validation Regex")
public class RegistrationRegexDTO {

    @DocField(description = "Email validation Regex")
    public final String emailRegex = ValidatorsRegex.EMAIL_REGEX;

    @DocField(description = "Address validation Regex")
    public final String addressRegex = ValidatorsRegex.ADDRESS_LINE_REGEX;

    @DocField(description = "Post Code validation Regex")
    public final String postcodeRegex = ValidatorsRegex.POSTCODE_REGEX;

    @DocField(description = "Name validation Regex")
    public final String nameRegex = ValidatorsRegex.SURNAME_REGEX;

    @DocField(description = "Mobile Number Regex")
    public final String mobileRegex = ValidatorsRegex.MOBILE_TELEPHONE_NUMBER_REGEX;

    @DocField(description = "Password Regex")
    public final String passwordRegex = ValidatorsRegex.PASSWORD_REGEX;


}
