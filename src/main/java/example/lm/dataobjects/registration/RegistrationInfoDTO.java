/* Copyright lottomart */
package example.lm.dataobjects.registration;

import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@DocClass(description = "Registration Information")
public class RegistrationInfoDTO {

    @DocField(description = "List of available countries")
    private List<CountryRegistrationInfoDTO> countries;

    @DocField(description = "List of available currencies")
    private List<CurrencyRegistrationInfoDTO> currencies;

    @DocField(description = "PCA configuration")
    private PCAConfigDTO pcaConfig;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
