package gusl.doc.generate;

import gusl.doc.AbstractDocumentation;
import gusl.doc.rest.RestDocumentor;

/**
 *
 * @author grant
 */
public class GenerateRestDocumentation extends AbstractDocumentation {

    private static final String OUTPUT_FILE = "generated/rest.adoc";

    public GenerateRestDocumentation() {
        super(OUTPUT_FILE, new RestDocumentor());
    }

    public static void main(String... args) throws Exception {

//        Options options = new Options();
//        options.addOption("src_root_dir", true, "Source Root Directory");
//
//        String srcRootDir;
//        try {
//            CommandLineParser parser = new DefaultParser();
//            CommandLine commandLine = parser.parse(options, args);
//            srcRootDir = commandLine.getOptionValue("src_root_dir");
//            if (srcRootDir == null || srcRootDir.isEmpty()) {
//                logger.error("No root project directory specified");
//                throw new DocException("No root project directory specified");
//            }
//
//            logger.info("Project directory: {}", srcRootDir);
//        } catch (ParseException ex) {
//            throw new DocException("Error parsing options");
//        }

        GenerateRestDocumentation generator = new GenerateRestDocumentation();
        generator.generate();
    }

}
