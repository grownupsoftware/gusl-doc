/* Copyright lottomart */
package example.lm.dataobjects.registration;

import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@DocClass(description = "Country Information for player registration")
public class CountryRegistrationInfoDTO {

    @DocField(description = "Country Name")
    private String name;

    @DocField(description = "ISO3166-1-Alpha-2")
    private String alpha2;

    @DocField(description = "Dial Code")
    private String dialCode;

    @DocField(description = "Is it a 'top' country")
    private Boolean topCountry;

    @DocField(description = "Block country in registration")
    private Boolean blockInRegistration;

    @DocField(description = "Country has Post Codes")
    private Boolean supportPostCode;

    @DocField(description = "Ordering for top countries")
    private Integer topOrder;

    @DocField(description = "Base64 image of country's flag")
    private String flag;

    @DocField(description = "Country has Post Codes")
    private Boolean hideInRegistration;

    @DocField(description = "Country has Post Codes")
    private Boolean mobileLookup;

}
