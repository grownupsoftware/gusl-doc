package gusl.doc.generate.builders;

import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import org.asciidoctor.ast.Document;
import org.asciidoctor.extension.Preprocessor;
import org.asciidoctor.extension.PreprocessorReader;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Map;

/**
 * @author grant
 */
public class ChangeAttributeValuePreprocessor extends Preprocessor {

    public static final GUSLLogger logger = GUSLLogManager.getLogger(ChangeAttributeValuePreprocessor.class);

    public ChangeAttributeValuePreprocessor(Map<String, Object> config) {
        super(config);
    }

    @Override
    public void process(Document document, PreprocessorReader reader) {
        logger.info("document: {}", document.getAttributes());

        // String workingDir = Paths.get("./doc").toAbsolutePath().normalize().toString();
        String workingDir = Paths.get(".").toAbsolutePath().normalize().toString();

//        File directory = new File(".");
//        try {
//            logger.info("DOT:{} {}", directory.getAbsolutePath(), directory.getCanonicalPath());
//        } catch (IOException e) {
//            logger.error("really", e);
//        }
//
//        logger.info("HOME: {}", Paths.get("./doc").toAbsolutePath().normalize().toString());

        String sourceDir = Paths.get("src/main/java").toAbsolutePath().normalize().toString();
        // String imageDir = Paths.get("src/main/resources/webapp/doc/img").toAbsolutePath().normalize().toString();
        String imageDir = Paths.get("doc").toAbsolutePath().normalize().toString();
        String imageHtmlDir = Paths.get("src/main/resources/webapp").toAbsolutePath().normalize().toString();

        //docdir=/Volumes/Striped/Users/grant/dev/gusl/master/blast-parent/blast-doc/doc/developer_manual        
        //docfile=/Volumes/Striped/Users/grant/dev/gusl/master/blast-parent/blast-doc/doc/developer_manual/developer_manual.adoc        
        String docDir = (String) document.getAttributes().get("docdir");

//        logger.info("==> docDir: {}",docDir);

//        String docFile = (String) document.getAttributes().get("docfile");

        String sourceRelative = new File(docDir).toPath().relativize(new File(sourceDir).toPath()).toString();
        String imageRelative = new File(docDir).toPath().relativize(new File(imageDir).toPath()).toString();
        String imageHtmlRelative = new File(docDir).toPath().relativize(new File(workingDir).toPath()).toString();

//        logger.info("src rel: {}", sourceRelative);
//        logger.info("img rel: {}", imageRelative);
//        logger.info("img html rel: {}", imageHtmlRelative);

        String filetype = (String) document.getAttributes().get("filetype");
        if ("html".equals(filetype)) {
            // 'dirs' are relevant to server hone
            document.getAttributes().put("sourcedir", sourceRelative);
            // document.getAttributes().put("imagesdir", imageHtmlRelative + "/doc/img");
            // document.getAttributes().put("imagesdir", imageHtmlRelative + "/img");
            // document.getAttributes().put("imagesdir", imageHtmlRelative + "../assets/images");
            // document.getAttributes().put("imagesdir", "../assets/images/");
            document.getAttributes().put("imagesdir", "./assets/images/");

        }
        if ("pdf".equals(filetype)) {

            // created at run time so 'dirs' are relevant to code dirs
            document.getAttributes().put("sourcedir", sourceRelative);
            document.getAttributes().put("imagesdir", imageDir);
        }
    }

}
