/* Copyright lottomart */
package gusl.doc.helpers;

import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.core.utils.Utils;
import org.apache.commons.io.FilenameUtils;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.IntStream;

/**
 *
 * @author grant
 */
public class ConvertJiraImages {

    public static final GUSLLogger logger = GUSLLogManager.getLogger(ConvertJiraImages.class);

    private static final String BASE_DIR = "/Users/grant/Downloads/jira_images/";

    private void convertTiff() {
        List<File> fileNames = new ArrayList<>();
        getTiffFiles(new File(BASE_DIR), fileNames);

        convertImages(fileNames);
    }

    private void getTiffFiles(File dir, List<File> fileNames) {
        File[] directoryListing = dir.listFiles();
        if (directoryListing != null) {
            for (File child : directoryListing) {
                if (child.isDirectory()) {
                    getTiffFiles(child, fileNames);
                } else {
                    if (child.getAbsolutePath().endsWith(".tiff")) {
                        fileNames.add(child);
                    }
                }
            }
        }
    }

    public void convertTiffToPng(File file) {
        try {
            try (InputStream is = new FileInputStream(file)) {
                try (ImageInputStream imageInputStream = ImageIO.createImageInputStream(is)) {
                    Iterator<ImageReader> iterator = ImageIO.getImageReaders(imageInputStream);
                    if (iterator == null || !iterator.hasNext()) {
                        throw new RuntimeException("Image file format not supported by ImageIO: " + file.getAbsolutePath());
                    }

                    // We are just looking for the first reader compatible:
                    ImageReader reader = iterator.next();
                    reader.setInput(imageInputStream);

                    int numPage = reader.getNumImages(true);

                    // it uses to put new png files, close to original example n0_.tiff will be in /png/n0_0.png
                    String name = FilenameUtils.getBaseName(file.getAbsolutePath());
                    String parentFolder = file.getParentFile().getAbsolutePath();

                    IntStream.range(0, numPage).forEach(v -> {
                        try {
                            final BufferedImage tiff = reader.read(v);
                            File newImage = new File(parentFolder + "/" + name + ".png");
                            ImageIO.write(tiff, "png", newImage);

//                            int idx = newImage.getAbsolutePath().indexOf(BASE_DIR);
//                            String img = newImage.getAbsolutePath().substring(idx + BASE_DIR.length());
//                            img = "./backoffice/" + img;
//                            logger.info("image::{}[title=\"{}\",width=\"200px\"]", img, newImage.getName());

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void convertImages(List<File> fileNames) {

        Utils.safeStream(fileNames).forEach(fileName -> {
            try {
                convertTiffToPng(fileName);
                fileName.delete();
            } catch (Exception ex) {
                logger.error("Error", ex);
            }

        });

    }

    public static void main(String... args) throws IOException, InterruptedException {
        ConvertJiraImages converter = new ConvertJiraImages();
        converter.convertTiff();
        Thread.sleep(2000);
    }

}
