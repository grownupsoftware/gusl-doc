package example.lm.dataobjects.profile;

import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@DocClass(description = "Verification provider")
public class VerificationProviderDO {

    @DocField(description = "Unique ID of provider")
    private Long id;

    @DocField(description = "Name of providers")
    private String name;

    @DocField(description = "Provider specific properties")
    private VerificationProviderPropertyDO properties;

    @DocField(description = "Status")
    private VerificationProviderStatus status;

}
