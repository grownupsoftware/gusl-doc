package example.api;

import example.dataobjects.AdminLoggedInUser;
import example.dataobjects.BonusFundsDetailDTO;
import example.dataobjects.BonusPanelDTO;
import gusl.core.annotations.DocApi;
import gusl.core.exceptions.GUSLErrorException;

import javax.annotation.security.RolesAllowed;
import javax.inject.Singleton;
import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

@Singleton
@Path(value = "")
public class BonusResource {

    public static final String GET_BONUS = "/bonus/{id}";
    public static final String UPDATE_BONUS = "/bonus/{id}/details";

    @GET
    @RolesAllowed({"READ"})
    @Path(GET_BONUS)
    @DocApi(description = "Get an Cart by id")
    public BonusFundsDetailDTO getBonusFunds(AdminLoggedInUser adminLoggedInUserDo, @PathParam("id") Long playerId) throws GUSLErrorException {
        return null;
    }

    @PUT
    @RolesAllowed({"READ", "WRITE"})
    @Path(UPDATE_BONUS)
    @DocApi(description = "Update an Bonus")
    public BonusPanelDTO updateBonus(AdminLoggedInUser adminLoggedInUserDo, @PathParam("id") Long id, @Valid BonusPanelDTO requestDTO) throws GUSLErrorException {
        return null;
    }

}
