package example.ll;

public enum LLBonusRedemptionType {

    CAP_IS_PER_BONUS,
    CAP_IS_PER_PERIOD,
    CAP_IS_PER_TARGET_LIST,
    NO_LIMIT,
    ONE_BONUS_PER_PLAYER,
    ONE_PER_TARGET_LIST

}
