/* Copyright lottomart */
package example.lm.dataobjects.anon;

import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@DocClass(description = "Token for an anonymous player")
public class AnonymousTokenDO {

    @DocField(description = "Unique anonymous id")
    private Long anonId;

    @DocField(description = "Unique country id (based on IP)")
    private Long countryId;

    @DocField(description = "Unique currency id (based on country via IP)")
    private Long currencyId;

    @DocField(description = "Build id - not version number (Edge only)")
    private Integer buildId;

    // default anon tier
    private Long diamondTierId;

    private String utmSource;
    private String utmMedium;
    private String utmCampaign;
    private String utmTerm;
    private String utmContent;
    private String utmOther;
    private String utmSet;

    private Boolean mobileWeb;

    private Boolean desktop;

    private Boolean ios;

    private Boolean android;

    private Long cohortId;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
