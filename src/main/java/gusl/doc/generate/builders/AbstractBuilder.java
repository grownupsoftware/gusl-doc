/* Copyright lottomart */
package gusl.doc.generate.builders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.collect.Lists;
import gusl.core.json.ObjectMapperFactory;
import gusl.doc.BaseDocumentor;
import gusl.doc.generate.extensions.*;
import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Attributes;
import org.asciidoctor.Placement;
import org.asciidoctor.SafeMode;
import org.asciidoctor.extension.JavaExtensionRegistry;
import org.reflections.vfs.Vfs;

import java.io.File;
import java.net.URL;
import java.util.*;

import static org.asciidoctor.Asciidoctor.Factory.create;
import static org.asciidoctor.OptionsBuilder.options;

/**
 * @author grant
 */
public abstract class AbstractBuilder extends BaseDocumentor {

    public static final String DOC_SRC_FOLDER = "doc";

    protected final String theOutputDir;

    public AbstractBuilder(String outputDir) {
        this.theOutputDir = outputDir;
    }

//    protected void document(String folder, String title, DocumentationType documentationType) {
//        File destFolder = new File(theOutputDir + File.separator + folder);
//
//        // note: parent adoc must match the name of the folder e.g. download equals download/download.adoc
//        convertFile(DOC_SRC_FOLDER + File.separator + folder + File.separator + folder + ".adoc", title, destFolder, documentationType);
//    }
//    
//    protected void convertFile(String srcFile, String title, File destFile, DocumentationType documentationType) {
//        Asciidoctor asciidoctor = createDoctor();
//        Map<String, Object> options = createOptions(title, destFile, documentationType);
//
//        File aDoc = new File(srcFile);
//        if (aDoc.exists()) {
//            logger.info("-- converting: {}", aDoc.getName());
//            asciidoctor.convertFile(aDoc, options);
//        } else {
//            logger.error("file does not exist: {}", aDoc.getAbsoluteFile());
//        }
//    }

    protected void createDocument(String title, String subFolder, String filePrefix, StringBuilder builder, DocumentationType documentationType) {
        Asciidoctor asciidoctor = createDoctor();
        Map<String, Object> options = createOptions(theOutputDir, title, new File("src/main/resources/webapp/" + subFolder), documentationType);

        switch (documentationType) {
            case PDF:
                options.put("to_file", filePrefix + ".pdf");
                break;
            case HTML:
            case HTML_DECK:
                options.put("to_file", filePrefix + ".html");
                break;
            default:
                throw new AssertionError(documentationType.name());
        }

        asciidoctor.convert(builder.toString(), options);

    }

    protected <T> void prettyPrint(T value) {
        try {
            logger.info(ObjectMapperFactory.prettyPrint(value));
        } catch (JsonProcessingException ex) {
            logger.error("Error parsing [{}] {}", value.getClass().getSimpleName(), value, ex);
        }
    }

    protected Asciidoctor createDoctor() {
        Asciidoctor asciidoctor = create();

        JavaExtensionRegistry extensionRegistry = asciidoctor.javaExtensionRegistry();

        extensionRegistry.preprocessor(ChangeAttributeValuePreprocessor.class);

        extensionRegistry.block("tree", TreeMacro.class);
        extensionRegistry.block("dir_listing", NewDirListing.class);
        extensionRegistry.block("yuml", YumlMacro.class);
        extensionRegistry.block("field_doc", FieldsExtension.class);
        extensionRegistry.block("rest_doc", RestExtension.class);
        extensionRegistry.block("event_doc", EventMacro.class);
        extensionRegistry.block("example_doc", ExamplesExtension.class);

        return asciidoctor;
    }

    protected String calcRootPrefix(String outputDir, File destFile) {

        File outDir = new File(outputDir);
        String dest = destFile.getAbsolutePath().replace(outDir.getAbsolutePath(), "");

        StringTokenizer stOR = new StringTokenizer(dest, "/");

        StringBuilder builder = new StringBuilder();
        for (int x = 0; x < stOR.countTokens(); x++) {
            builder.append("../");
        }


        return builder.toString();
    }

    protected Map<String, Object> createOptions(String outputDir, String title, File destFile, DocumentationType documentationType) {
        String rootPrefix = calcRootPrefix(outputDir, destFile);

        Attributes attributes = new Attributes();
        if (documentationType != DocumentationType.HTML_DECK) {
            attributes.setTableOfContents(true);
            attributes.setTableOfContents2(Placement.LEFT);
            attributes.setLinkCss(true);
        }
        attributes.setTitle(title);

        Map<String, Object> attributeMap = attributes.map();
        if (documentationType == DocumentationType.HTML_DECK) {
            attributeMap.put("deckjsdir", rootPrefix + "js/deck.js");
        }

        attributeMap.put("icons", "font");
        attributeMap.put("stylesdir", rootPrefix + "css");
        attributeMap.put("stylesheet", "lottomart-asciidoctor.css");
        attributeMap.put("source-highlighter", "coderay");
        attributeMap.put("toc-title", "Lottomart");
        attributeMap.put("toclevels", "3");
        attributeMap.put(":docinfo:", "shared");

        Map<String, Object> options = options()
                .safe(SafeMode.UNSAFE)
                .toDir(destFile)
                .mkDirs(true)
                .attributes(attributes)
                .attributes(attributeMap)
                .option("backend", documentationType.getType())
                .asMap();

        if (documentationType == DocumentationType.HTML_DECK) {
            List<String> templateDirs = new ArrayList<>();
            templateDirs.add("./doc/templates/haml");
            options.put("template_dirs", templateDirs);
        }

        return options;
    }

    protected static class EmptyIfFileEndingsUrlType implements Vfs.UrlType {

        private final List<String> fileEndings;

        public EmptyIfFileEndingsUrlType(final String... fileEndings) {

            this.fileEndings = Lists.newArrayList(fileEndings);
        }

        @Override
        public boolean matches(URL url) {

            final String protocol = url.getProtocol();
            final String externalForm = url.toExternalForm();
            if (!protocol.equals("file")) {
                return false;
            }
            for (String fileEnding : fileEndings) {
                if (externalForm.endsWith(fileEnding)) {
                    return true;
                }
            }
            return false;
        }

        @Override
        public Vfs.Dir createDir(final URL url) throws Exception {

            return emptyVfsDir(url);
        }

        private static Vfs.Dir emptyVfsDir(final URL url) {

            return new Vfs.Dir() {
                @Override
                public String getPath() {
                    return url.toExternalForm();
                }

                @Override
                public Iterable<Vfs.File> getFiles() {
                    return Collections.emptyList();
                }

                @Override
                public void close() {
                }
            };
        }

    }

}
