package gusl.doc.generate.extensions;

import gusl.annotations.form.UiField;
import gusl.core.utils.IOUtils;
import gusl.core.utils.StringUtils;
import gusl.doc.BaseDocumentor;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.PNGTranscoder;
import org.asciidoctor.ast.StructuralNode;
import org.asciidoctor.extension.Reader;

import java.io.*;
import java.lang.reflect.Field;
import java.util.*;

import static gusl.doc.generate.extensions.Macro.IMAGE_DIR;

/**
 * @author grantwallace
 */
public class UiTableMacro extends AbstractMacro {

    private static int imageCounter = 1;

    private final String randomPrefix;

    public UiTableMacro(String name, Map<String, Object> config) {
        super(name, config);
        randomPrefix = UUID.randomUUID().toString().substring(0, 4);
    }

    @Override
    public Object process(StructuralNode parent, Reader reader, Map<String, Object> attributes) {

        String className = (String) attributes.get("class");
        String imageTitle = (String) attributes.get("title");
        String width = (String) attributes.get("width");
        String height = (String) attributes.get("height");
        String colWidthStr = (String) attributes.get("colWidth");
        int colWidth = 70;
        if (colWidthStr != null) {
            colWidth = Integer.valueOf(colWidthStr);
        }

        try {
            StringBuilder hdrBuilder = new StringBuilder();
            StringBuilder builder = new StringBuilder();
            StringBuilder colBuilder = new StringBuilder();
            StringBuilder colBorderBuilder = new StringBuilder();

            hdrBuilder.append("<svg ");
            hdrBuilder.append("xmlns:svg=\"http://www.w3.org/2000/svg\" xmlns=\"http://www.w3.org/2000/svg\"  ");
            // viewBox=\"0 0 1400 800\"
            hdrBuilder.append(" width=\"").append(width).append("\" height=\"").append(height).append("\" >").append(NL);
            // hdrBuilder.append("<title>").append(imageTitle).append("</title>").append(NL);
            hdrBuilder.append("<g id='rowGroup' transform='translate(0, 0)' role=\"table\" >").append(NL);

            builder.append("<text x='30' y='30' font-size='12px' font-weight='bold' fill='#0088cc' text-anchor='middle'  role=\"row\">").append(NL);

            Class<?> clazz = Class.forName(className);

            List<Field> fields = BaseDocumentor.getFieldsFor(clazz);

            int x = 0;
            for (Field field : fields) {
                UiField uiField = field.getAnnotation(UiField.class);
                if (uiField == null) {
                    continue;
                }

                if (uiField.displayInTable()) {
                    builder.append("<tspan role=\"columnheader\" x='").append(x + 60).append("'>").append(parseCodeToLabel(field, uiField)).append("</tspan>").append(NL);
                    // colBuilder.append("<tspan role=\"cell\" x='").append(x).append("'>").append("").append("</tspan>").append(NL);
                    colBorderBuilder.append("<rect x='").append(x + 30).append("' y='10' width='").append(colWidth).append("' height='170' fill='none' stroke='white'/>").append(NL);
                    x += colWidth;
                }

            }
            builder.append("</text>").append(NL);
            // x += 70;
            int lastHeight = 0;
            for (int y = 1; y < 6; y++) {
                StringBuilder rowBuilder = new StringBuilder();
                rowBuilder.append("<text x='30' y='").append((y * 30 + 10)).append("' font-size='8px' text-anchor='middle' role=\"row\">").append(NL);
                rowBuilder.append(colBuilder.toString());
                rowBuilder.append("</text>").append(NL);
                builder.append("<rect x='30' y='").append((y * 30 + 10)).append("' width='").append(x).append("' height='20' fill='gainsboro'/>").append(NL);
                builder.append(rowBuilder.toString());
                lastHeight = (y * 30 + 10);
            }

            hdrBuilder.append("<rect x='25' y='0' width='").append(x).append("' height='").append(lastHeight).append("' fill='#eaeef1'/>").append(NL);

            hdrBuilder.append(builder);
            hdrBuilder.append(colBorderBuilder);
            hdrBuilder.append("</g>").append(NL);
            hdrBuilder.append("</svg>").append(NL);

            IOUtils.writeFile(new File("/tmp/test.svg"), hdrBuilder.toString());

            // logger.info("builder: \n{}", builder.toString());
            Map<String, Object> paramValues = new HashMap<>();

            String imageName = "generated/" + "html_" + randomPrefix + "_" + imageCounter + ".png";

            // logger.info("svg: \n {}", builder.toString());
            convertSvgToPng(hdrBuilder.toString(), IMAGE_DIR + imageName);

//            Html2Image html2Image = Html2Image.fromHtml(builder.toString());
//            html2Image.getImageRenderer().setHeight(200);
//            html2Image.getImageRenderer().saveImage(IMAGE_DIR + imageName);
//            HtmlImageGenerator imageGenerator = new HtmlImageGenerator();
//            imageGenerator.loadHtml("<b>Hello World!</b> Please goto <a title=\"Goto Google\" href=\"http://www.google.com\">Google</a>.");
//            imageGenerator.loadHtml(builder.toString());
//            imageGenerator.saveAsImage(IMAGE_DIR + imageName);
            paramValues.put("target", imageName);
            if (imageTitle != null && !imageTitle.isEmpty()) {
                paramValues.put("title", imageTitle);
            }

            return createBlock(parent, "image", "", paramValues);
        } catch (Throwable ex) {
            logger.error("Failed to create html images", ex);
            return createBlock(parent, "pass", Arrays.asList(new String[]{"Failed to create html image\n"}), attributes);
        }

    }

    private String parseCodeToLabel(Field field, UiField config) {
        String label = config.label();
        if (StringUtils.isNotBlank(label)) {
            return label;
        }

        String name = field.getName();
        if (name.length() <= 1) {
            return name;
        }

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(name.charAt(0));
        for (int i = 0; i < name.length() - 1; i++) {
            char c1 = name.charAt(i);
            char c2 = name.charAt(i + 1);

            if (Character.isLowerCase(c1) && Character.isUpperCase(c2)) {
                stringBuilder.append(" ");
            }
            stringBuilder.append(c2);
        }

        String s = stringBuilder.toString();
        return Character.toUpperCase(s.charAt(0)) + s.substring(1);
    }

    private void convertSvgToPng(String svgContent, String filename) throws FileNotFoundException, TranscoderException, IOException {
//Step -1: We read the input SVG document into Transcoder Input
        //We use Java NIO for this purpose
//        String svg_URI_input = Paths.get("/tmp/sample.svg").toUri().toURL().toString();
//        TranscoderInput input_svg_image = new TranscoderInput(svg_URI_input);
        TranscoderInput input_svg_image = new TranscoderInput(new ByteArrayInputStream(svgContent.getBytes()));

        try ( //Step-2: Define OutputStream to PNG Image and attach to TranscoderOutput
              OutputStream png_ostream = new FileOutputStream(filename)) {
            // OutputStream png_ostream = new FileOutputStream("fred.png")) {
            TranscoderOutput output_png_image = new TranscoderOutput(png_ostream);
            // Step-3: Create PNGTranscoder and define hints if required
            PNGTranscoder my_converter = new PNGTranscoder();
            // Step-4: Convert and Write output
            my_converter.transcode(input_svg_image, output_png_image);
            // Step 5- close / flush Output Stream
            png_ostream.flush();
        }

    }
}
//            StringBuilder divStyle = new StringBuilder();
//            divStyle.append("margin:20px;");
//            divStyle.append("width:").append(width).append(";");
//            // divStyle.append("box-shadow: 0 5px 5px -3px #b5b3b3, 0 8px 10px 1px #b5b3b3, 0 3px 14px 2px#b5b3b3;");
//
//            StringBuilder tableStyle = new StringBuilder();
//            tableStyle.append("width:100%;");
//            //tableStyle.append("border: 1px solid #949393;");
//            //tableStyle.append("border-collapse: collapse;");
//            tableStyle.append("font-family: Roboto,sans-serif;");
//            tableStyle.append("background-color: #f1f9fd;");
//
//            StringBuilder trHdrStyle = new StringBuilder();
//            trHdrStyle.append("color: #868682;");
//            trHdrStyle.append("border-bottom: 1px solid #949393;");
//            //trHdrStyle.append("border: 1px solid #949393;");
//
//            StringBuilder trStyle = new StringBuilder();
//            trStyle.append("border-bottom: 1px solid #949393;");
//            trStyle.append("border-left: 1px solid #949393;");
//            // trStyle.append("border: 1px solid #949393;");
//
//            StringBuilder tdStyle = new StringBuilder();
//            // tdStyle.append("border: 1px solid #949393;");
//            tdStyle.append("border-right: 1px solid #949393;");
//
//            builder.append("<div style=\"").append(divStyle).append("\"><table  style=\"").append(tableStyle).append("\"><tr  style=\"").append(trStyle).append("\">");
//
//            Class<?> clazz = Class.forName(className);
//
//            List<Field> fields = BaseDocumentor.getFieldsFor(clazz);
//
//            for (Field field : fields) {
//                UiField uiField = field.getAnnotation(UiField.class);
//                if (uiField == null) {
//                    continue;
//                }
//
//                if (uiField.displayInTable()) {
//                    builder.append("<th style=\"").append(trHdrStyle).append("\">").append(parseCodeToLabel(field, uiField)).append("</th>");
//                    blankRowBuilder.append("<td  style=\"").append(tdStyle).append("\">").append(" ").append("</td>");
//                }
//
//            }
//            builder.append("</tr>");
//
//            for (int x = 0; x < 10; x++) {
//                builder.append("<tr style=\"").append(trStyle).append("\">").append(blankRowBuilder.toString()).append("</tr>");
//            }
//            builder.append("</table></div>");
