/* Copyright lottomart */
package example.lm.dataobjects.country;

import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@DocClass(description = "Manual field configuration")
public class ManualFieldDO {

    @DocField(description = "Field name")
    private String name;

    @DocField(description = "Type of field, text, option")
    private String type;

    @DocField(description = "Url for lookup e.g counties in UK")
    private String lookupUrl;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
