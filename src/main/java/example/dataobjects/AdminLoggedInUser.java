package example.dataobjects;

import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;

@DocClass(description = "Logged in Back Office user")
public class AdminLoggedInUser {
    @DocField(description = "Email")
    private String email;
}
