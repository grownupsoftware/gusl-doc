package example.ll;

public enum LLBonusStatus {
    PENDING,
    SUSPENDED,
    ACTIVE,
    EXPIRED,
    REMOVE;
}
