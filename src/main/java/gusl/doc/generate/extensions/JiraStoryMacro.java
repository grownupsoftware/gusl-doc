/* Copyright lottomart */
package gusl.doc.generate.extensions;

/**
 *
 * @author grant
 */
public class JiraStoryMacro { // extends AbstractMacro {

    /**
     * **** THIS USED DEPRECATED LIBS, not sure we use this functionality anymore - so commenting out rather than fixing
     */

//    public static final GUSLLogger logger = GUSLLogManager.getLogger(this.getClass());
//
//    private static PropertiesClient propertiesClient;
//    private static JiraOAuthClient jiraOAuthClient;
//    private static boolean configured = false;
//
//    public JiraStoryMacro(String name, Map<String, Object> config) {
//        super(name, config);
//    }
//
//    @Override
//    public Object process(StructuralNode parent, Reader reader, Map<String, Object> attributes) {
//
//        String jiraTicket = (String) attributes.get("ticket");
//        if (jiraTicket == null) {
//            logger.error("No ticket passed - ignoring");
//            return null;
//        }
//        try {
//            JSONObject ticket = getTicket(jiraTicket);
//
//            String epicSummary;
//            JSONObject ticketFields = ticket.getJSONObject("fields");
//            epicSummary = (String) ticketFields.get("summary");
//            JSONObject epicStatus = ticketFields.getJSONObject("status");
//
//            //logger.info("ticket: {}", ticket);
//            JSONArray issues = ticketFields.getJSONArray("subtasks");
//            StringBuilder builder = new StringBuilder();
//
//            String statusCss = (String) epicStatus.get("name");
//            statusCss = "story-" + statusCss.toLowerCase().replace(" ", "-");
//
//            // start - source block
//            builder.append("[source,python,,role=\"").append(statusCss).append("\"]").append(NL).append("====").append(NL);
//            builder.append(BOLD).append("STORY").append(BOLD).append(NL).append(NL);
//
//            builder.append(BOLD).append("").append(jiraTicket).append(" ").append(epicSummary).append(BOLD).append(NL).append(NL);
//            builder.append(BOLD).append("Status: ").append((String) epicStatus.get("name")).append(BOLD).append(NL).append(NL);
//            builder.append("[options=\"header\",cols=\"10%,40%,8%,6%,4%,12%,8%,8%\"]\n|===").append(NL);
//            builder
//                    .append(HDR_COL).append("Ticket")
//                    .append(HDR_COL).append("Description")
//                    .append(HDR_COL).append("Status")
//                    .append(HDR_COL).append("Labels")
//                    .append(HDR_COL).append("Version")
//                    .append(HDR_COL).append("Allocated")
//                    .append(HDR_COL).append("Estimate")
//                    .append(HDR_COL).append("Remaining")
//                    .append(NL);
//
//            for (int x = 0; x < issues.length(); x++) {
//
//                JSONObject issue = issues.getJSONObject(x);
//                JSONObject fields = issue.getJSONObject("fields");
//
//                JSONObject status = fields.getJSONObject("status");
//
//                JSONObject subTicket = getTicket((String) issue.get("key"));
//                //logger.info("subTicket: {}", subTicket);
//
//                JSONObject subTicketFields = subTicket.getJSONObject("fields");
//                JSONObject timeTracking = subTicketFields.getJSONObject("timetracking");
//                JSONArray versions = subTicketFields.getJSONArray("fixVersions");
//                JSONArray labels = subTicketFields.getJSONArray("labels");
//                String tags = "";
//                for (int v = 0; v < labels.length(); v++) {
//                    tags += labels.get(v) + " ";
//                }
//                JSONObject version = null;
//                if (versions.length() > 0) {
//                    version = versions.getJSONObject(0);
//                }
//
////                logger.info("issue: {}", issue);
////                logger.info("fields: {}", fields);
////                logger.info("status: {}", status);
//                String assigneeName;
//
//                if (subTicketFields.isNull("assignee")) {
//                    assigneeName = "unassigned";
//                } else {
//                    JSONObject assignee = subTicketFields.getJSONObject("assignee");
//                    assigneeName = (String) assignee.get("displayName");
//                }
//
//                String description = "*" + (fields.isNull("summary") ? "" : (String) fields.get("summary")) + ".*";
//                if (!subTicketFields.isNull("description")) {
//                    description += "\n" + (String) subTicketFields.get("description");
//                }
//
//                builder
//                        .append(HDR_COL).append((String) issue.get("key"))
//                        //                        .append(HDR_COL).append(fields.isNull("description") ? "" :(String)  fields.get("description"))
//                        .append(HDR_COL).append(description)
//                        .append(HDR_COL).append(status.isNull("name") ? "" : (String) status.get("name"))
//                        .append(HDR_COL).append(tags)
//                        .append(HDR_COL).append(version == null ? "" : (String) version.get("name"))
//                        .append(HDR_COL).append(assigneeName)
//                        .append(HDR_COL).append((String) timeTracking.get("originalEstimate"))
//                        .append(HDR_COL).append((String) timeTracking.get("remainingEstimate"))
//                        .append(NL);
//            }
//
//            builder.append(CLOSE_TABLE).append(NL);
//
//            // end - source block
//            builder.append("====").append(NL).append(NL).append(NL);
//
//            List<String> content = new ArrayList<>();
//
//            content.addAll(Arrays.asList(builder.toString().split("\n")));
//
//            parseContent(parent, content);
//        } catch (Exception ex) {
//            logger.error("Error: {}", ex.getMessage(), ex);
//        }
//
//        return null;
//    }
//
//    private JSONObject getTicket(String ticket) throws Exception {
//        if (!configured) {
//            propertiesClient = new PropertiesClient();
//            jiraOAuthClient = new JiraOAuthClient(propertiesClient);
//        }
//
//        Map<String, String> properties = propertiesClient.getPropertiesOrDefaults();
//        String tmpToken = properties.get(ACCESS_TOKEN);
//        String secret = properties.get(SECRET);
//
//        String jiraHome = properties.get(JIRA_HOME);
//        String url = jiraHome + "/rest/api/2/issue/" + ticket;
//
//        logger.error(ticket);
//
//        try {
//            OAuthParameters parameters = jiraOAuthClient.getParameters(tmpToken, secret, properties.get(CONSUMER_KEY), properties.get(PRIVATE_KEY));
//            HttpResponse response = getResponseFromUrl(parameters, new GenericUrl(url));
//            return parseResponse(response);
//        } catch (Exception e) {
//            logger.error("error [{}] : {}", ticket, e.getMessage());
//            throw e;
//        }
//    }
//
//    private JSONObject getEpic(String ticket) throws Exception {
//        if (!configured) {
//            propertiesClient = new PropertiesClient();
//            jiraOAuthClient = new JiraOAuthClient(propertiesClient);
//        }
//
//        Map<String, String> properties = propertiesClient.getPropertiesOrDefaults();
//        String tmpToken = properties.get(ACCESS_TOKEN);
//        String secret = properties.get(SECRET);
//
//        String jiraHome = properties.get(JIRA_HOME);
//        String url = jiraHome + "/rest/agile/1.0/epic/" + ticket + "/issue";
//
//        try {
//            OAuthParameters parameters = jiraOAuthClient.getParameters(tmpToken, secret, properties.get(CONSUMER_KEY), properties.get(PRIVATE_KEY));
//            HttpResponse response = getResponseFromUrl(parameters, new GenericUrl(url));
//            return parseResponse(response);
//        } catch (Exception e) {
//            logger.error("error", e);
//            throw e;
//        }
//    }
//
//    private JSONObject parseResponse(HttpResponse response) throws Exception {
//        Scanner s = new Scanner(response.getContent()).useDelimiter("\\A");
//        String result = s.hasNext() ? s.next() : "";
//
//        try {
//            JSONObject jsonObj = new JSONObject(result);
//            return jsonObj;
//        } catch (Exception e) {
//            logger.error("error", e);
//            throw e;
//        }
//    }
//
//    private static HttpResponse getResponseFromUrl(OAuthParameters parameters, GenericUrl jiraUrl) throws IOException {
//        HttpRequestFactory requestFactory = new NetHttpTransport().createRequestFactory(parameters);
//        HttpRequest request = requestFactory.buildGetRequest(jiraUrl);
//        return request.execute();
//    }

}
