/* Copyright lottomart */
package example.lm.dataobjects.anon;

import gusl.core.annotations.DocField;

import java.util.HashMap;
import java.util.Map;

/**
 * @author grant
 */
public class AnonPropertiesDO {

    @DocField(description = "Map of properties")
    private Map<String, Object> properties = new HashMap<>();

    public Map<String, Object> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, Object> properties) {
        this.properties = properties;
    }

}
