package example.ll;

public enum LLPlayerBonusStatus {

    PENDING,
    ACTIVE,
    CANCELLED,
    NOT_ACHIEVED,
    REDEEMED,
    AUTO_CONV_REDEEMED,
    EXPIRED,
    WITHDRAW_FREEZE,
    WITHDRAW_CONFIRMED,
    PERMANENT_WITHDRAW_FREEZE;

}
