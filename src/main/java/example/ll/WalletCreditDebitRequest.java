package example.ll;

import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;

@DocClass(description = "Request to apply debit or credit to player's wallet")
public class WalletCreditDebitRequest {

    @DocField(description = "Unique playerId")
    private ObjectId playerId;

    @DocField(description = "Adjustment amount")
    private Money adjustmentAmount;

}
