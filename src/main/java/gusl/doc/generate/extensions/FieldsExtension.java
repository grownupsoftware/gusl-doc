/*
 * Grownup Software Limited.
 */
package gusl.doc.generate.extensions;

import gusl.core.annotations.DocClass;
import org.asciidoctor.ast.StructuralNode;
import org.asciidoctor.extension.Reader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static gusl.doc.BaseDocumentor.*;

/**
 *
 * @author grant
 */
public class FieldsExtension extends AbstractMacro {

    public FieldsExtension(String name, Map<String, Object> config) {
        super(name, config);
    }

    @Override
    public Object process(StructuralNode parent, Reader reader, Map<String, Object> attributes) {
        logger.debug("field attributes: {}", attributes);

        String className = (String) attributes.get("class");

        List<String> content = new ArrayList<>();
        try {
            Class<?> clazz = Class.forName(className);

            StringBuilder builder = new StringBuilder();

            // add anchor
            String anchorName = clazz.getCanonicalName();//.replace(".", "-");
            
            builder.append(BEGIN_ANCHOR).append(anchorName).append(END_ANCHOR).append(NL);
//            parseContent(parent, Arrays.asList(builder.toString().split("\n")));
//            builder = new StringBuilder();

            builder.append("[source,python,role=\"do-class\"]").append(NL).append("====").append(NL);
            builder.append(BEGIN_ANCHOR).append(anchorName).append(END_ANCHOR).append(NL).append(NL);

            builder.append(NL).append(BOLD).append("Class:").append(BOLD).append(SPACE)
                    .append(BOLD).append(clazz.getCanonicalName()).append(BOLD)
                    .append(NL);

            //parseContent(parent, Arrays.asList(builder.toString().split("\n")));
            DocClass docClass = clazz.getAnnotation(DocClass.class);
            if (docClass != null) {
                //builder = new StringBuilder();
                builder.append(NL).append("Description: ").append(docClass.description()).append(NL);
                //parseContent(parent, Arrays.asList(builder.toString().split("\n")));
            }

            StringBuilder aDoc = documentClass(clazz, true, null);
            aDoc.append(NL);

            aDoc.append("====").append(NL).append(NL).append(NL);

            builder.append(aDoc);

            //content.addAll(Arrays.asList(aDoc.toString().split("\n")));
            content.addAll(Arrays.asList(builder.toString().split("\n")));

//            logger.info("\n\n{}",builder.toString());
//            logger.info("\n\n{}",content);
            parseContent(parent, content);

        } catch (ClassNotFoundException ex) {
            logger.error("Failed to find class: {}", className);
            return createBlock(parent, "pass", Arrays.asList(new String[]{"Failed to find class " + className + " - must be on class path\n"}), attributes);
        }

        return null;
    }

}
