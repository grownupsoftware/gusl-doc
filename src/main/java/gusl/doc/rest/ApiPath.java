/* Copyright lottomart */
package gusl.doc.rest;

/**
 *
 * @author grant
 */
public class ApiPath implements Comparable<ApiPath> {

    private final String path;
    private final StringBuilder stringBuilder;
    private final String httpMethod;

    public ApiPath(String path, StringBuilder stringBuilder, String httpMethod) {
        this.path = path;
        this.stringBuilder = stringBuilder;
        this.httpMethod = httpMethod;
    }

    public String getPath() {
        return path;
    }

    public StringBuilder getStringBuilder() {
        return stringBuilder;
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    @Override
    public String toString() {
        return "ApiPath{" + "path=" + path + ", stringBuilder=" + stringBuilder + ", httpMethod=" + httpMethod + '}';
    }

    @Override
    public int compareTo(ApiPath o2) {
        return getPath().compareTo(o2.getPath());
    }
}
