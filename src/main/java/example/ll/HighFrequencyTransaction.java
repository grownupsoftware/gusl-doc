package example.ll;

import gusl.core.annotations.DocField;

import java.util.Date;
import java.util.Map;

public class HighFrequencyTransaction {
    @DocField(description = "Mongo Id")
    private ObjectId id;

    @DocField(description = "")
    private long transactionId;

    @DocField(description = "Not withdrawable balance")
    private Money withdrawableBalance;

    @DocField(description = "Not withdrawable balance")
    private Money notWithdrawableBalance;

    @DocField(description = "Unique playerId")
    private ObjectId playerId;

    @DocField(description = "withdrawable amount")
    private Money withdrawableAmount;

    @DocField(description = "Not withdrawable amount")
    private Money notWithdrawableAmount;

    @DocField(description = "External reference")
    private String externalReference;

    @DocField(description = "Transaction Type")
    private TransactionType transactionType;

    @DocField(description = "Grouping Id")
    private String groupingId;

    @DocField(description = "Product Group")
    private String productGroup;

    @DocField(description = "Product Id")
    private String productId;

    @DocField(description = "Timestamp")
    private Date timestamp;

    @DocField(description = "Additional data")
    private Map<String, String> additionalData;

}
