== Wallet

Having spending some time with the technical team around the Wallet (HFA sub system) we have come up with the following observations.

HFA uses HighFrequencyTransaction object, which is basically the last player transaction.
This stores all of the transactional information, as well as the current wallet balances.
These are cached on each node for fast lookups.

Although unconventional, (normally wallet amounts are stored separately), it does seem to be working for you, and does solve the problem of locking across two entities.

Performance.

When talking about performance, it can only really be measured in the context in the life cycle of when the HFA sub system will be used.
For example, lets take a game stake for example, we would need to measure the from the point of hitting the OMNI endpoint to the return to the game operator to understand the life of the transaction, and then measure the amount of time spent in HFA. Maybe this is something that we can do, when we integrate the existing lotto games into OMNI.

Loyalty Scheme.

One of the things that we were ask to look at was the possibility of adding a loyalty scheme to the wallet.
The main persistent object HighFrequencyTransaction, is easily extensible to add a couple of extra fields to enable a player loyalty scheme.
The extra fields required would be :-

|===
|Field | Description
|loyaltyPoints | Current number of loyalty points the player has.
|lifetimeLoyaltyPoints | The accumulative number of loyalty points
|===

It is envisaged that the new BBE sub system would calculate the number of loyalty points accrued per transaction as it would a bonus.
Having loyalty tiers could unlock differing player benefits, or change the rate at what the rate that the loyalty points are accrued.
When thinking about loyalty schemes, redemption is a major factor.
For example, can players purchase items with them?
Do they convert into scratch cards?
The tiers and contribution levels would need to be configured, but again, could be configured like a bonus.
The player entity would also need to be extended to store the currently loyalty tier.
