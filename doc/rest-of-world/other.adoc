
=== Diamonds

==== Current

[width=100%",options="header"]
|===
| UK | CA
| image:../img/lm/other/diamond_group_uk.png[width="200px] |   image:../img/lm/other/diamond_group_ca.png[width="200px]
|===

Diamonds have an *economy* whereby the value of *rewards* and *redeem* value is _linked_ to the underpinning currency.

An assumption at design stage was that the value of a diamond in GBP would not be the same value as one in USD.

==== RoW Challenges

I see no value in changing this relationship.

Although the 'exchange rate' (redeem rate) is the same for all cohorts, and likely to remain the case for proposed new currencies (USD,EUR) I see no value in simplifying this.

===== Solution

No additional work required.

=== Fund Options

==== Current

[width=100%",options="header"]
|===
| Options | Topup bucket
| image:../img/lm/other/fund_options.png[width="200px] | image:../img/lm/other/topup_bucket.png[width="200px]
|===

==== RoW Challenges

What fund options fails to do is to take into account 'perceived value' within countries. One country may view USD$50 as a small amount while another may view it as a lot of money.

However, at this stage I do not think this warrants country overrides.

===== Solution

No additional work required.

=== Terms

==== Current

[width=100%",options="header"]
|===
| UK
| image:../img/lm/other/terms.png[width="200px]
|===

Currently, our terms are 3 separate documents:

* Terms and conditions

* Privacy policy

* Funds Protection Policy

These are links to zendesk articles. (Note: we use 'en-ca' from an organisation point of view, the player never sees 'en-ca' so the path structure is immaterial and not related to any value in the cohort table. )

We update all 3 docs as one change.

==== RoW Challenges

I am expecting one broad 'Rest of World' terms applicable for all countries and any specific country elements would be included.

If we want specific terms for a country not included in the standard then this wil have to be designed.

===== Solution

No additional work required.

