package gusl.doc.yaml.dataobjects;

/**
 *
 * @author grant
 */
public class UserStoryDO {

    private String description;
    private String url;
    private boolean saveResponse;

    public UserStoryDO(String description, String url) {
        this.description = description;
        this.url = url;
    }

    
    public UserStoryDO(String description, String url,boolean saveResponse) {
        this.description = description;
        this.saveResponse = saveResponse;
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isSaveResponse() {
        return saveResponse;
    }

    public void setSaveResponse(boolean saveResponse) {
        this.saveResponse = saveResponse;
    }

}
