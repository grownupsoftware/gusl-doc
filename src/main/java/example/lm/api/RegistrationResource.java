package example.lm.api;

import example.lm.dataobjects.registration.RegistrationQueryDTO;
import gusl.core.annotations.DocApi;
import gusl.core.annotations.DocClass;
import gusl.core.exceptions.GUSLErrorException;

import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;

@Singleton
@Path(value = "auth")
@DocClass(description = "REST resource for player authentication")
public class RegistrationResource {

    @GET
    @Path(value = "info/v2")
    @DocApi(description = "Get Registration Info")
    public RegistrationQueryDTO getRegistrationInfoV2(
            @Context final HttpServletRequest servletRequest
    ) throws GUSLErrorException {
        return null;
    }

}
