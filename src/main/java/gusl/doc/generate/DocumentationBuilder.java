package gusl.doc.generate;

import gusl.core.json.ObjectMapperFactory;
import gusl.core.lambda.MutableBoolean;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.core.utils.Utils;
import gusl.doc.exceptions.DocException;
import gusl.doc.generate.builders.AbstractDocBuilder;
import gusl.doc.generate.builders.DocumentationType;
import gusl.doc.generate.extensions.AngularAllMacro;
import gusl.doc.generate.extensions.AngularClassMacro;
import gusl.doc.generate.extensions.AngularLocalisedMacro;
import gusl.doc.rest.ApiPath;
import org.apache.commons.cli.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;

import javax.ws.rs.Path;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import static gusl.doc.BaseDocumentor.NL;
import static gusl.doc.BaseDocumentor.SUB_HEADER;
import static gusl.doc.rest.RestDocumentor.*;

/**
 * @author grant
 */
public class DocumentationBuilder extends AbstractDocBuilder {

    public static final GUSLLogger logger = GUSLLogManager.getLogger(DocumentationBuilder.class);

    private final String theOutputDir;
    private final String theUIHome;

    private static final String OUTPUT_DIR_ARG = "output_dir";
    private static final String UI_HOME_ARG = "ui_home";
    private static final String DEV_ARG = "dev";
    private static final String SYSTEM_ARG = "system";
    private static final String UI_ARG = "ui";
    private static final String IWG_ARG = "iwg";
    private static final String ITEM_ARG = "item";
    private static final String EXAMPLE_ARG = "example";
    private static final String OVERVIEW_ARG = "overview";
    private static final String FUNDS_ARG = "funds";
    private static final String KENO_ARG = "keno";
    private static final String DM_ARG = "dm";

    public DocumentationBuilder(String outputDir, String uiHome) {
        theOutputDir = outputDir;
        theUIHome = uiHome;
    }

    private void createDocumentation(CommandLine commandLine) {

        AngularClassMacro.setUiHome(theUIHome);
        AngularAllMacro.setUiHome(theUIHome);
        AngularLocalisedMacro.setUiHome(theUIHome);

//
        if (commandLine.hasOption(IWG_ARG)) {
            String[] sourceFolders = new String[]{
                    "../lotto-gateway/src/main/java/lm/gateway/iwg"
            };

            documentCode("./doc/iwg/from_code.adoc", sourceFolders, commandLine.hasOption(EXAMPLE_ARG));
            document("iwg", "IWG Integration", DocumentationType.PDF);
            document("iwg", "IWG Integration", DocumentationType.HTML);
        }

        if (commandLine.hasOption(DEV_ARG)) {
            //        // extracts asciidoc from code - picked up in developer manual
            String[] sourceFolders = new String[]{
                    "../lotto-core/src/main/java",
                    "../lotto-edge/src/main/java",
                    "../lotto-admin/src/main/java",
                    "../lotto-dataservice/src/main/java",};
            documentCode("./doc/developer_manual/from_code.adoc", sourceFolders, commandLine.hasOption(EXAMPLE_ARG));
            document("developer_manual", "Lottomart Developer Manual", DocumentationType.PDF, "developer_manual", theOutputDir);
            document("developer_manual", "Lottomart Developer Manual", DocumentationType.HTML, "developer_manual", theOutputDir);
        }
        if (commandLine.hasOption(SYSTEM_ARG)) {
            document("system_overview", "Lottomart System Overview", DocumentationType.PDF, "system_overview", theOutputDir);
            document("system_overview", "Lottomart System Overview", DocumentationType.HTML, "system_overview", theOutputDir);
        }

        if (commandLine.hasOption(UI_ARG)) {
            document("ui_manual", "Lottomart Player", DocumentationType.HTML, "ui_manual", theOutputDir);
        }

        if (commandLine.hasOption(DM_ARG)) {
            document("draw_management", "Lotto Draws", DocumentationType.HTML, "draw_management", theOutputDir);
            document("draw_management", "Lotto Draws", DocumentationType.PDF, "draw_management", theOutputDir);
        }

        if (commandLine.hasOption(KENO_ARG)) {
            document("keno", "Keno MVP", DocumentationType.HTML, "keno", theOutputDir);
            document("keno", "Keno MVP", DocumentationType.PDF, "keno", theOutputDir);
        }

        if (commandLine.hasOption(FUNDS_ARG)) {
            document("topup-funds", "Wallet funds topup", DocumentationType.HTML, "topup-funds", theOutputDir);
            document("topup-funds", "Admin", DocumentationType.PDF, "topup-funds", theOutputDir);
        }

        if (commandLine.hasOption(ITEM_ARG)) {

            convertFile(DOC_SRC_FOLDER + File.separator + "ui_manual/cart" + File.separator + "index.adoc", "Cart", new File(theOutputDir + File.separator + "ui_manual/items"), DocumentationType.HTML, "cart");
            //  convertFile(DOC_SRC_FOLDER + File.separator + "ui_manual/cart" + File.separator + "summary_idx.adoc", "Cart", new File(theOutputDir + File.separator + "ui_manual/items"), DocumentationType.PDF, "cart_draft");
//            
//            convertFile(DOC_SRC_FOLDER + File.separator + "ui_manual/diamonds" + File.separator + "summary_idx.adoc", "Diamonds", new File(theOutputDir + File.separator + "ui_manual/items"), DocumentationType.HTML, "diamonds");
//            convertFile(DOC_SRC_FOLDER + File.separator + "ui_manual/diamonds" + File.separator + "summary_idx.adoc", "Diamonds", new File(theOutputDir + File.separator + "ui_manual/items"), DocumentationType.PDF, "diamonds_draft");
//            
//            convertFile(DOC_SRC_FOLDER + File.separator + "ui_manual/menu" + File.separator + "summary_reg.adoc", "Authentication", new File(theOutputDir + File.separator + "ui_manual/items"), DocumentationType.HTML, "authentication");
//            convertFile(DOC_SRC_FOLDER + File.separator + "ui_manual/menu" + File.separator + "summary_reg.adoc", "Authentication", new File(theOutputDir + File.separator + "ui_manual/items"), DocumentationType.PDF, "authentication_draft");
//
//            convertFile(DOC_SRC_FOLDER + File.separator + "ui_manual/menu" + File.separator + "summary_settings.adoc", "Account Settings", new File(theOutputDir + File.separator + "ui_manual/items"), DocumentationType.HTML, "settings");
//            convertFile(DOC_SRC_FOLDER + File.separator + "ui_manual/menu" + File.separator + "summary_settings.adoc", "Account Settings", new File(theOutputDir + File.separator + "ui_manual/items"), DocumentationType.PDF, "settings_draft");
//            
//            convertFile(DOC_SRC_FOLDER + File.separator + "ui_manual/lotto" + File.separator + "summary_idx.adoc", "Lotto", new File(theOutputDir + File.separator + "ui_manual/items"), DocumentationType.HTML, "lotto");
//            convertFile(DOC_SRC_FOLDER + File.separator + "ui_manual/lotto" + File.separator + "summary_idx.adoc", "Lotto", new File(theOutputDir + File.separator + "ui_manual/items"), DocumentationType.PDF, "lotto_draft");

//             convertFile(DOC_SRC_FOLDER + File.separator + "ui_manual/dev_status" + File.separator + "dev_status.adoc", "Dev Status", new File(theOutputDir + File.separator + "ui_manual/items"), DocumentationType.HTML, "dev_status");
//             convertFile(DOC_SRC_FOLDER + File.separator + "ui_manual/dev_status" + File.separator + "dev_status.adoc", "Dev Status", new File(theOutputDir + File.separator + "ui_manual/items"), DocumentationType.PDF, "dev_status");
//            convertFile(DOC_SRC_FOLDER + File.separator + "ui_manual/epic" + File.separator + "summary_idx.adoc", "Lotto", new File(theOutputDir + File.separator + "ui_manual/items"), DocumentationType.PDF, "epic_draft");
            // convertFile(DOC_SRC_FOLDER + File.separator + "ui_manual/all_images" + File.separator + "all_images.adoc", "Images", new File(theOutputDir + File.separator + "ui_manual/items"), DocumentationType.HTML, "images");
            //  convertFile(DOC_SRC_FOLDER + File.separator + "ui_manual/menu" + File.separator + "summary_other.adoc", "Menu", new File(theOutputDir + File.separator + "ui_manual/items"), DocumentationType.HTML, "menu");
            // convertFile(DOC_SRC_FOLDER + File.separator + "ui_manual/promotions" + File.separator + "summary_idx.adoc", "Promotions", new File(theOutputDir + File.separator + "ui_manual/items"), DocumentationType.HTML, "promotions");
        }

        if (commandLine.hasOption(OVERVIEW_ARG)) {
            // document("ui_manual", "Lottomart Player Overview", DocumentationType.PDF, "summary.adoc", "overview");
            document("ui_manual", "Lottomart Player Overview", DocumentationType.HTML, "summary.adoc", "overview");
        }

//        convertFile(DOC_SRC_FOLDER + File.separator + "ui_manual/menu/registration" + File.separator + "index.adoc", "Registration", new File(theOutputDir + File.separator + "ui_manual"), DocumentationType.HTML);
//
//
//        convertFile(DOC_SRC_FOLDER + File.separator + "ui_manual/promotions" + File.separator + "index.adoc", "Promotions", new File(theOutputDir + File.separator + "promotions"), DocumentationType.HTML);
        // convertFile(DOC_SRC_FOLDER + File.separator + "ui_manual/player" + File.separator + "account-states.adoc", "Account States", new File(theOutputDir + File.separator + "ui_manual"), DocumentationType.PDF);
//        document("pdf_manual", DocumentationType.PDF);
//
//        document("documentation", DocumentationType.HTML);
//        document("contribute", DocumentationType.HTML);
//        document("download", DocumentationType.HTML);
//
//        document("blog", DocumentationType.HTML);
//        document("blog", DocumentationType.PDF);
//
//        documentPresentations("/documentation/presentations", "presentations", DocumentationType.HTML_DECK);
//        documentExternal("module");
//        GenerateModuleDocumentation moduleDocumentation = new GenerateModuleDocumentation();
//        convertContent(
//                moduleDocumentation.generate(),
//                "Modules",
//                new File(theOutputDir + File.separator + "modules"),
//                DocumentationType.HTML, DocumentationType.PDF);
//        logger.info("Start webiste server ./gradle blast-website:site");
//        File home = new File(theOutputDir + File.separator + "index.html");
//        logger.info("or, open file://{}", home.getAbsoluteFile());
    }

    private void documentExternal(String category) {
        ExternalDocumentation externalDocumentation = new ExternalDocumentation();
        StringBuilder builder = externalDocumentation.generate(theOutputDir, category);

        convertContent(
                builder,
                category,
                new File(theOutputDir + File.separator + category.toLowerCase()),
                theOutputDir,
                DocumentationType.HTML, DocumentationType.PDF);

    }

    //    private void locateAndMoveImages(StringBuilder content) {
//        int fromIndex = 0;
//        while (fromIndex > 0) {
//            int startIndex = content.indexOf("image::", fromIndex);
//            if (startIndex > 0) {
//                int endIndex = content.indexOf("[]", startIndex);
//                String imageName = content.substring(startIndex + 7, endIndex);
//                logger.info("imageName: {}",imageName);
//            } else {
//                fromIndex = -1;
//            }
//        }
//    }
    @SuppressWarnings("deprecation")
    private void documentCode(String destFile, String[] sourceFolders, boolean addExample) {
        StringBuilder builder = new StringBuilder(1000);

        Set<Class<?>> dtoClasses = new HashSet<>();
        Map<String, List<ApiPath>> mapPath = new HashMap<>();

        logger.info("sourceFolders: {}", Arrays.asList(sourceFolders));

        Utils.safeStream(sourceFolders).forEach(dir -> {
            logger.info("dir: {}", dir);

            File directory = new File(dir);
            logger.info("dir exists: {}", directory.exists());
            if (directory.exists()) {
                Collection<File> files = FileUtils.listFiles(
                        directory,
                        null, // new RegexFileFilter("^(.*?)"),
                        DirectoryFileFilter.DIRECTORY
                );

                Utils.safeStream(files).forEach(file -> {
                    //logger.info("file: {}", file);
                    if (!file.getName().startsWith(".")) {
                        try {
                            List<String> lines = Files.readAllLines(Paths.get(file.toURI()), StandardCharsets.UTF_8);

                            if (file.getName().endsWith(".adoc")) {
                                Utils.safeStream(lines).forEach(line -> {
                                    builder.append(line).append(NL);
                                });
                            } else if (file.getName().endsWith(".java")) {

                                MutableBoolean docOn = new MutableBoolean();
                                Utils.safeStream(lines).forEach(line -> {
                                    //logger.info("line: {}", line);
                                    if (line.contains("@Asciidoc")) {
                                        // toggle
                                        if (docOn.get()) {
                                            docOn.set(false);
                                        } else {
                                            docOn.set(true);
                                        }
                                    } else if (line.contains("/*")) {
                                        docOn.set(false);
                                    } else if (line.contains("@author")) {
                                        docOn.set(false);
                                    }

                                    if (docOn.get()) {
                                        if (!line.contains("@Asciidoc")) {
                                            //logger.info("doc: {}", line);
                                            if (line.startsWith(" * ")) {
                                                builder.append(line.substring(3)).append(NL);
                                            } else {
                                                builder.append(line).append(NL);
                                            }
                                        }
                                    }
                                });
                                String className = null;
                                try {
                                    className = file.getCanonicalPath().replace(".java", "").replace("/", ".");
                                    int idx = className.indexOf("src.main.java");
                                    if (idx > 0) {
                                        className = className.substring(idx + "src.main.java".length() + 1);
                                    }

                                    //Class<?> clazz = Class.forName(file.getCanonicalPath().replace(".java", ""));
                                    Class<?> clazz = Class.forName(className);
                                    Path[] pathAnnotations = clazz.getAnnotationsByType(Path.class);
                                    if (pathAnnotations != null && pathAnnotations.length > 0) {
                                        // Its a REST file
                                        builder.append(documentRestClass(true, SUB_HEADER, clazz, dtoClasses, mapPath));
                                        builder.append(NL).append(NL);
                                    }
                                    //logger.info("=> {}", builder.toString());
                                } catch (ClassNotFoundException ex) {
                                    logger.error("Class not found: {}", className);
                                }
                            }
                        } catch (IOException ex) {
                            logger.error("failed", ex);
                        }
                    }
                });
            }

        });

        accumulateAllDTOS(dtoClasses);

        try {
            // generate the associated DO documentation
            builder.append(generateDTODocumentation(dtoClasses, ObjectMapperFactory.createDefaultObjectMapper(), addExample));
        } catch (Exception ex) {
            logger.error("error generating DO data ", ex);
        }

        try {
            FileUtils.writeStringToFile(new File(destFile), builder.toString());
        } catch (IOException ex) {
            logger.error("error writing file", ex);
        }
    }

    private void documentPresentations(String srcFolder, String destFolder, DocumentationType documentationType) {
        File file = new File(DOC_SRC_FOLDER + File.separator + srcFolder);
        String[] directories = file.list((File current, String name) -> new File(current, name).isDirectory());

        for (String directory : directories) {
            File dir = new File(DOC_SRC_FOLDER + File.separator + srcFolder + File.separator + directory);
            File destFile = new File(theOutputDir + File.separator + destFolder + File.separator + dir.getName());

            convertFile(dir + File.separator + "presentation.adoc", dir.getName(), destFile, documentationType, theOutputDir);
        }
    }

    public static void main(String... args) throws DocException {

        Options options = new Options();
        options.addOption(OUTPUT_DIR_ARG, true, "Destination Directory");
        options.addOption(UI_HOME_ARG, true, "Home of Player UI");
        options.addOption(UI_ARG, "Generate UI documentation");
        options.addOption(FUNDS_ARG, "Add wallet funds desc");
        options.addOption(KENO_ARG, "Keno documentation");
        options.addOption(DM_ARG, "Draw Management documentation");

        options.addOption(IWG_ARG, "Generate IWG Documentation");
        options.addOption(ITEM_ARG, "Generate Bespoke Documentation");
        options.addOption(SYSTEM_ARG, "Player Documentation");
        options.addOption(DEV_ARG, "Generate Dev Documentation");
        options.addOption(SYSTEM_ARG, "Generate System Documentation");
        options.addOption(EXAMPLE_ARG, "Include examples for DTOs");
        options.addOption(OVERVIEW_ARG, "Overview documentation");

        String outputDir;
        String uiHome;
        try {
            CommandLineParser parser = new DefaultParser();
            CommandLine commandLine = parser.parse(options, args);
            outputDir = commandLine.getOptionValue("output_dir");
            uiHome = commandLine.getOptionValue("ui_home");

            if (outputDir == null || outputDir.isEmpty()) {
                logger.error("No root project directory specified");
                throw new DocException("No root project directory specified");
            }

            DocumentationBuilder builder = new DocumentationBuilder(outputDir, uiHome);
            builder.createDocumentation(commandLine);

            logger.info("Output directory: {}", outputDir);
        } catch (ParseException ex) {
            throw new DocException("Error parsing options");
        }

    }

}
