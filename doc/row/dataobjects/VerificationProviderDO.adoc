[yuml,type="class",direction="LR",width=600,height=200,filename="cust-orders"]
----
[Customer{bg:red}]<>-orders.forname*>[Order]++-0..*>[LineItem],
[Order]-[note: Aggregate Root ala DDD{bg:wheat}],

[Customer|
+forname: string;
+surname: string;
-password: string
| login(user,pass)],

[Order|
+forname: string;
+surname: string;
-password: string
|login(user,pass)]
----

    @DocField(description = "Unique ID of provider")
    private Long id;

    @DocField(description = "Name of providers")
    private String name;

    @DocField(description = "Provider specific properties")
    private VerificationProviderPropertyDO properties;

    @DocField(description = "Status")
    private VerificationProviderStatus status;

