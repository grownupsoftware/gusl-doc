package example.ll;

import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;

import java.util.Date;

@DocClass(description = "Player instance of a bonus")
public class LLPlayerBonus {

    @DocField(description = "Unique instance id")
    private ObjectId id;

    @DocField(description = "Parent parachute bonus id")
    private ObjectId bonusId;

    @DocField(description = "Unique player id")
    private ObjectId playerId;

    @DocField(description = "Date instance issued")
    private Date dateIssued;

    @DocField(description = "Bonus amount to be redeemed on successful completion")
    private Money bonusAmount;

    @DocField(description = "This is dateIssued + validity (number days valid). Can be null for no limit. Bonus must be 'played out' or 'redeemed' before end-date")
    private Date endDate;

    @DocField(description = "Target for 'redemption'. Bonus amount * wagering")
    private Money accumulationTarget;

    @DocField(description = "Current progress. if currentAccumulation > accumulationTarget bonus amount can be redeemed. Note: units need to be able to handle decimals. E.g: if using GBP, which is 2 decimal places, accumulation must be 4 decimal places")
    private Money currentAccumulation;

    @DocField(description = "State within instance lifecycle")
    private LLPlayerBonusStatus status;


}
