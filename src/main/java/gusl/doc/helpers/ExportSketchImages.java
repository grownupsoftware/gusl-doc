package gusl.doc.helpers;

import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.core.utils.Utils;
import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * (1) open sketch in chrome, scroll to bottom of page , copy HTML from
 * developer tools [elements] to sketch.html in ./doc (2) run this
 *
 * @author grant
 */
public class ExportSketchImages {

    public static final GUSLLogger logger = GUSLLogManager.getLogger(ExportSketchImages.class);

    // private static final String IMAGE_DIR = "src/main/resources/webapp/doc/img/sketch/";
    private static final String IMAGE_DIR = "src/main/resources/webapp/doc/img/flow/";
    private static final String TMP_IMAGE_DIR = "/tmp/";

    private static final String SERVER = "https://sketch.cloud/s/r991A";
    private static StringBuilder names = new StringBuilder();
    public static String NL = "\n";

    @SuppressWarnings("deprecation")
    private String saveImage(String imageName, String urlString) throws MalformedURLException, IOException {

        URL url = new URL(urlString);

        String origImageName = imageName;
        imageName = TMP_IMAGE_DIR + imageName;

        File newImage = new File(imageName);
        newImage.getParentFile().mkdirs();

        logger.debug("sketch saving to: {} from url: {}", imageName, URLEncoder.encode(urlString, "UTF-8"));

        OutputStream out;
        try (InputStream in = new BufferedInputStream(url.openStream())) {
            out = new BufferedOutputStream(new FileOutputStream(imageName));
            for (int i; (i = in.read()) != -1;) {
                out.write(i);
            }
        }
        out.close();

        BufferedImage image = null;

        logger.info("file: {}", newImage.getAbsolutePath());
        image = ImageIO.read(newImage);
        File fixedImage = new File(IMAGE_DIR + origImageName);
        fixedImage.getParentFile().mkdirs();
        ImageIO.write(image, "png", fixedImage);

        return imageName;
    }

    private void downloadImages() throws IOException {
        Document doc = Jsoup.parse(new File("doc/sketch.html"), "UTF-8");

        Elements aTags = doc.select("a[href]");
        for (Element tag : aTags) {

            String href = tag.attr("href");
            String splits[] = href.split("/");

            if (splits.length == 6) {
                Elements images = tag.select("img");
                for (Element image : images) {

                    String imageName = splits[4] + "/" + splits[5] + ".png";
                    logger.info("tag {} {} ", tag.attr("href"), image.attr("src"));
                    saveImage(imageName, image.attr("src"));

                    names.append("image::./sketch/").append(splits[4]).append("/").append(splits[5]).append(".png[").append("title=\"").append(splits[5].replace("-", " ")).append("\"").append(",width=\"200px\"").append("]").append("\n");
                }
            }
        }

    }

    @SuppressWarnings("deprecation")
    private void writeFile() throws IOException {
        String fileName = "./doc/image_list.txt";
        FileUtils.writeStringToFile(new File(fileName), names.toString());
    }

    @SuppressWarnings("deprecation")
    private void processImageNames() throws IOException {
        String fileName = "./doc/image_list.txt";
        Map<String, List<String>> map = Files.lines(Paths.get(fileName)).map(line -> {
            // image::./sketch/lobby/
            //String[] split = line.split("/");
            //logger.info("split: {}",split[2]);
            return line; //split[2];
        }).collect(Collectors.groupingBy(l -> {
            String[] split = l.split("/");
            return split[2];
        }));
        // logger.info("resilts: {}", list);

        StringBuilder builder = new StringBuilder();
        
        Utils.safeStream(map.entrySet())
                .filter(entry -> !entry.getKey().equals("symbols")
                && !entry.getKey().equals("edge")
                && !entry.getKey().equals("backup")
                )
                .forEach(entry -> {
                    builder.append("== ").append(entry.getKey()).append(NL).append(NL);

                    
                    
                    builder.append("[width=\"100%\",options=\"header\"]").append(NL);
                    builder.append("|====================").append(NL);
                    
                    for (int x = 0; x < entry.getValue().size(); x++) {
                        builder.append("| ");
                    }
                    builder.append(NL);
                    Utils.safeStream(entry.getValue()).forEach(img -> {
                        builder.append("|").append(img.replace("image::", "image:")).append(NL);
                    });

                    builder.append("|====================").append(NL).append(NL).append(NL);
                    
                     builder.append("=== Tickets");

                    builder.append("[width=\"100%\",options=\"header\"]").append(NL);
                    builder.append("|====================").append(NL);
                    builder.append("| Ticket| Description").append(NL);
                    builder.append("| | ").append(NL);
                    builder.append("| | ").append(NL);
                    builder.append("|====================").append(NL).append(NL).append(NL);

                });

        logger.info("builder: \n {}", builder.toString());

        String outFile = "./doc/ui_manual/dev_status/dev_status_conv.adoc";
        FileUtils.writeStringToFile(new File(outFile), builder.toString());

    }

    public static void main(String... args) throws IOException, InterruptedException {
        ExportSketchImages exporter = new ExportSketchImages();
        exporter.downloadImages();
        exporter.writeFile();
        exporter.processImageNames();
        logger.info("Images:\n{}", names.toString());
        Thread.sleep(2000);
    }

}
