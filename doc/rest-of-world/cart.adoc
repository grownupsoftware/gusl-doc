=== Cart overview

image:./epg/cart.png[width="200px"]


image::./epg/cart-model.png[title="Cart Components",width="200px"]

[yuml,type="class",dir=LR,width=600,height=200,filename="cart-model"]
----
[Player{bg:orange}]<>1-carts 1..*>[Cart{bg:yellow}],
[Cart]<>1-betSlips 1..*>[BetSlip{bg:red}],
[BetSlip]<>1-tickets 1..*>[Ticket{bg:steelblue}],
[Ticket]<>1-lottoBets 1..*>[LottoBet{bg:green}],
[Ticket]<>1-gameBets 1..*>[GameBet{bg:green}],
----

[width=100%",options="header"]
|====================
|Status|Description
|Player| One 'ACTIVE' cart per player
|Cart| Container for current items picked for purchase.
|BetSlip| Is a collection of tickets (and promotions)
|Ticket| Is a collection of bets
|Lotto Bets| A collection of lotto bets
|Game Bets| A collection of game bet
|====================
