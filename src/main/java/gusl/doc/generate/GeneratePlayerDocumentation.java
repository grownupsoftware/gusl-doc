/* Copyright lottomart */
package gusl.doc.generate;

import gusl.doc.exceptions.DocException;
import gusl.doc.generate.builders.AbstractBuilder;
import gusl.doc.generate.builders.DocumentationType;
import org.apache.commons.cli.*;

import java.io.IOException;

/**
 *
 * @author grant
 */
public class GeneratePlayerDocumentation extends AbstractBuilder {

    public static final String PLAYER_DIR = "player";
    public static final String PLAYER_FILE_NAME = "player";

    public GeneratePlayerDocumentation(String outputDir) {
        super(outputDir);
    }

    private void createDocumentation() {
        StringBuilder builder = generateContent();

        createDocument("Player App", PLAYER_DIR, PLAYER_FILE_NAME, builder, DocumentationType.HTML);
        createDocument("Player App", PLAYER_DIR, PLAYER_FILE_NAME, builder, DocumentationType.PDF);

    }

    private StringBuilder generateContent() {
        StringBuilder builder = new StringBuilder();
        return builder;
    }

    public static void main(String... args) throws DocException, IOException, InterruptedException {

        try {
            Options options = new Options();
            options.addOption("output_dir", true, "Destination Directory");

            String outputDir;
            try {
                CommandLineParser parser = new DefaultParser();
                CommandLine commandLine = parser.parse(options, args);
                outputDir = commandLine.getOptionValue("output_dir");
                if (outputDir == null || outputDir.isEmpty()) {
                    logger.error("No root project directory specified");
                    throw new DocException("No root project directory specified");
                }

                logger.info("Output directory: {}", outputDir);
            } catch (ParseException ex) {
                throw new DocException("Error parsing options");
            }

            GeneratePlayerDocumentation builder = new GeneratePlayerDocumentation(outputDir);
            builder.createDocumentation();

            logger.info("Finished");
        } catch (Exception ex) {
            logger.error("Error", ex);
        } finally {
            Thread.sleep(2000);
        }
    }

}
