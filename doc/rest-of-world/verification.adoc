=== Verification

==== Current

Presently Lottomart uses the following verification providers:

[width=100%",options="header"]
|===
| Provider | Role
| GBG | Verification
| Hooyu | Documentation
|===

These are presently _baked_ into the code.

In the future we will need these to be more configurable:

* Phase 1 - move current 'baked in' coding to use the new table / configuration settings

* Phase 2 - cascading configuration (out of scope)

==== Row Challenges

Need country specific settings.

===== Solution (Phase 1)

Make verification settings specific to a country while keeping _duplication_ to a minimum.

Achieved by creating a *Verification Profile* with each country linking to a profile. Changes can then be done at a profile level rather than at a country level while maintaining flexibility in providing country specific overrides (i.e. create new profile only used by that country)

[yuml,type="class",direction="LR",format="plain",width=600,height=200,filename="country-verification"]
----
[Country{bg:steelblue}]<>verificationProfileId *..1 id>[VerificationProfile{bg:yellow}],
[VerificationProfile]<>verificationProviderId *..1 id>[VerificationProvider{bg:red}],
[VerificationProfile]<>documentationProviderId *..1 id>[VerificationProvider{bg:red}],

[Country|
...;
verificationProfileId: Long;
...],

[VerificationProfile|
id: Long;
name: string;
verificationProviderId: Long;
documentationProviderId: Long;
properties: ProfilePropertiesDO],

[VerificationProvider|
id: string;
name: string;
properties: ProviderPropertiesDO;
status: enum]
----


*Example Data*

[width=100%",options="header",title="Country"]]
|===
| id | name  | verificationProfileId
| 1  | UK    | 1
| 2  | CA    | 2
| 3  | AU    | 3
| 4  | NZ    | 3
| 5  | ZW    | 4
|===

[width=100%",options="header",title="Profile",cols="5*a"]]
|===
| id | name  | verificationProviderId | documentationProviderId | properties
| 1  | UK          | 1 | 2 |

[source,json]
----
{
  "properties": {
    "profile": "XXXX",
    "identityVerifiedThreshold": "111",
    "min_age": "18",
    "verificationGracePeriod": "PT0S",
    "verificationGracePeriodAgeVerified": "PT0S",
    "ageVerifiedThreshold": "1000",
    "verificationType": "KYC_BEFORE_FTD"
  }
}
----

| 2  | CA          | 1 | 2 |

[source,json]
----
{
  "properties": {
    "profile": "XXXX", <1>
    "identityVerifiedThreshold": "1111",
    "min_age": "19",
    "verificationGracePeriod": "PT36H",
    "verificationGracePeriodAgeVerified": "PT672H",
    "ageVerifiedThreshold": "1000",
    "verificationType": "KYC_TIMED"
  }
}

----

| 3  | RestOfWorld | 1 | 2 | ...
| 4  | ZW override | 1 | 2 | ...

|===

<1> We currently have a 'profile' key by cohort in casanova.json


[IMPORTANT]
====
* Some of the data above is currently configured in casanova.json. This will have to be removed.
* Need to check if changing values does not change GBG response i.e every change becomes a bespoke coding change.

====

[NOTE]
====
Properties will be different depending upon providers.
The use of 'UiField.ignoreIfNotPresent' on each different property types makes complex properties easily readable and maintainable in the back office.

====

[width=100%",options="header",title="Provider",cols="4*a"]
|===
| id | name   | status | properties
| 1  | GBG    | ACTIVE |

[source,json]
----
{
  "properties": {
    "gbg": {
      "player": {
        "service-key": "XXXX-XXXX-XXXX-XXXX",
        "search-url": "https://services.postcodeanywhere.co.uk/CapturePlus/Interactive/Find/v2.10/json3ex.ws?",
        "get-url": "https://services.postcodeanywhere.co.uk/CapturePlus/Interactive/Retrieve/v2.10/json3ex.ws?",
        "max-results": "30"
      },
      "server": {
        "user-name": "xxxxx@lottomart.com",
        "password": "xxxxx-xxxx-xxxxx",
        "url": "https://pilot.id3global.com/ID3gWS/ID3global.svc?wsdl"
      }
    }
  }
}
----

| 2  | Hooyou | ACTIVE |
[source,json]
----
{
  "hooyu": {
    "client-id": "xxxxx-xxxx-xxxx-xxxx-xxxxxxx",
    "secret": "xxxxxxxxxxx",
    "callback-url": "https://demo.lottomart.app/gateway/rest/hooyu/callback",
    "env": "Sandbox"
  }
}
----

|===

