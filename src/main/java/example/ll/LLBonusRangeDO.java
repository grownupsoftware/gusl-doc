package example.ll;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class LLBonusRangeDO {

    private Money from;

    private Money to;

    private Double percentage;

    private Integer displayOrder;

}
