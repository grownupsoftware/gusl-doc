/*
 * Grownup Software Limited.
 */
package gusl.doc.generate;


import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.core.utils.Utils;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author grant
 */
public class ExternalDocumentation {

    protected final GUSLLogger logger = GUSLLogManager.getLogger(this.getClass());

    private static final String DOC_LOCATIONS[] = new String[]{"/..", "/../.."};

    private static final String IGNORE[] = new String[]{"lotto-doc"};

    public StringBuilder generate(String outputDir, String category) {
        List<String> paths = findPathsWithCategory(category);
        logger.info("Components: {}", paths);
        return buildDocumentation(outputDir, category, paths);
    }

    @SuppressWarnings("deprecation")
    private StringBuilder buildDocumentation(String outputDir, String category, List<String> modulePaths) {
        StringBuilder builder = new StringBuilder();
        try {
            builder.append(FileUtils.readFileToString(new File("./doc/external/" + category + ".adoc")));
        } catch (IOException ex) {
            logger.error("Failed to find external start doc: {}", "./doc/external/" + category + ".adoc");
        }
//        builder.append("= Blast Modules").append(NL);
//        builder.append("Author: Grown up Software").append(NL).append(NL);

        Utils.safeStream(modulePaths).forEach(indexDoc -> {
            builder.append("include::").append(indexDoc).append("[]");
            moveImages(indexDoc, outputDir);
        });

        logger.info("builder: {}", builder.toString());
        return builder;
    }

    /**
     * find projects (modules) that have a doc folder with index.adoc
     *
     * @return list of paths to index.adoc
     */
    private List<String> findPathsWithCategory(String category) {
        List<String> modulePaths = new ArrayList<>();

        Path currPath = Paths.get("");

        Utils.safeStream(DOC_LOCATIONS).forEach(locDir -> {
            Path newPath = Paths.get(currPath.toAbsolutePath().toString() + locDir);

            File docPath = newPath.normalize().toFile();

            docPath.listFiles((File dir, String name) -> {

                File file = new File(dir.getAbsoluteFile() + "/" + name + "/doc/" + category + ".adoc");
                logger.debug("looking for: {}", file.getAbsolutePath());
                if (file.exists()) {
                    if (!Utils.safeStream(IGNORE).anyMatch(ignoreName -> file.getName().equals(ignoreName))) {
                        modulePaths.add(file.getAbsolutePath());
                    }
                }
                return false;
            });

        });

        return modulePaths;
    }

    void moveImages(String docName, String outputDir) {
        File doc = new File(docName);
//        logger.info("copy from :{}  to  {}", doc.getParent(), outputDir + "/images");
        try {
            FileUtils.copyDirectory(new File(doc.getParent() + "/images"), new File(outputDir + "/images"));
        } catch (IOException ex) {
            logger.error("failed to copy images from: {} to {}", doc.getParent() + "/images", outputDir + "/images");
        }
    }
}
