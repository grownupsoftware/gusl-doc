/* Copyright lottomart */
package example.lm.dataobjects.country;

import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@DocClass(description = "Cohort details for a country")
public class CountryCohortDO {

    @DocField(description = "Unique ID of cohort")
    private Long cohortId;


}
