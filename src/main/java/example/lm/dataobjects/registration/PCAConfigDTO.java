/* Copyright lottomart */
package example.lm.dataobjects.registration;

import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@DocClass(description = "PCA configuration")
public class PCAConfigDTO {

    @DocField(description = "PCA Predict service key")
    private String serviceKey;

    @DocField(description = "Url for searching PCA")
    private String searchUrl;

    @DocField(description = "Url for retrieving address using PCA id")
    private String getUrl;

    @DocField(description = "maximum number of results returned per search")
    private Integer maxResults;

    @DocField(description = "List of PCA field names for manual PCA address entry")
    private List<String> formFields;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
