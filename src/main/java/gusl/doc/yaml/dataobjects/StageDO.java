package gusl.doc.yaml.dataobjects;

/**
 *
 * @author grant
 */
public class StageDO {

    private String name;
    private String className;
    private String methodName;
    private String description;
    private String url;

    private RequestDO request;

    private ResponseDO response;

    public StageDO() {
    }

    public StageDO(String name, RequestDO request, ResponseDO response) {
        this.name = name;
        this.request = request;
        this.response = response;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RequestDO getRequest() {
        return request;
    }

    public void setRequest(RequestDO request) {
        this.request = request;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ResponseDO getResponse() {
        return response;
    }

    public void setResponse(ResponseDO response) {
        this.response = response;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        // return "StageDO{" + "name=" + name + ", className=" + className + ", methodName=" + methodName + ", description=" + description + ", url=" + url + ", request=" + request + ", response=" + response + '}';
        return "StageDO{" + "name=" + name  + ", methodName=" + methodName +"}";
    }
    

}
