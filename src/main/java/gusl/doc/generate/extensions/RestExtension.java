/* Copyright lottomart */
package gusl.doc.generate.extensions;

import gusl.core.utils.Utils;
import gusl.doc.rest.ApiPath;
import org.asciidoctor.ast.StructuralNode;
import org.asciidoctor.extension.Reader;

import java.util.*;

import static gusl.doc.rest.RestDocumentor.documentRestClass;

/**
 *
 * @author grant
 */
public class RestExtension extends AbstractMacro {

    public RestExtension(String name, Map<String, Object> config) {
        super(name, config);
    }

    @Override
    public Object process(StructuralNode parent, Reader reader, Map<String, Object> attributes) {
        logger.debug("field attributes: {}", attributes);

        String className = (String) attributes.get("class");
        String pathName = (String) attributes.get("path");
        String methodName = (String) attributes.get("method");

        List<String> content = new ArrayList<>();
        try {
            Class<?> clazz = Class.forName(className);

            StringBuilder builder = new StringBuilder();

            Set<Class<?>> dtoClasses = new HashSet<>();

            Map<String, List<ApiPath>> mapPath = new HashMap<>();

            builder.append("[source,java,role=\"rest\"]").append(NL).append("====").append(NL);
            //builder.append("[source").append(",").append("java]").append(NL).append("====").append(NL);

            StringBuilder restBuilder = documentRestClass(true, null, clazz, dtoClasses, mapPath, false);

            if (pathName == null) {
                builder.append(restBuilder);
            } else {
                Utils.safeStream(mapPath.values()).forEach(apiLst -> {
                    Utils.safeStream(apiLst).forEach(api -> {
                        if (api.getPath() != null && api.getPath().equals(pathName)) {
                            if (methodName != null && api.getHttpMethod().toLowerCase().equals(methodName.toLowerCase())) {
                            //logger.info("method: {} http method: {}",methodName.toLowerCase(),api.getHttpMethod().toLowerCase());
                                builder.append(api.getStringBuilder());
                            } else if (methodName == null) {
                                builder.append(api.getStringBuilder());
                            }
                        }
                    });
                });
            }

            // close source
            builder.append("====").append(NL).append(NL).append(NL);

            //logger.info("builder: {}", builder.toString());
            content.addAll(Arrays.asList(builder.toString().split("\n")));

            parseContent(parent, content);

        } catch (ClassNotFoundException ex) {
            logger.error("Failed to find class: {}", className);
            return createBlock(parent, "pass", Arrays.asList(new String[]{"Failed to find class " + className + " - must be on class path\n"}), attributes);
        }

        return null;
    }

}
