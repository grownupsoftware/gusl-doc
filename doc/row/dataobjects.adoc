=== Data Objects

Summary of final data objects






==== AnonymousTokenDO

An anonymous token:
[field_doc,class="example.lm.dataobjects.anon.AnonymousTokenDO"]
----
----

==== RegistrationInfoDTO

[field_doc,class="example.lm.dataobjects.registration.RegistrationInfoDTO"]
----
----

==== RegistrationRegexDTO

[field_doc,class="example.lm.dataobjects.registration.PCAConfigDTO"]
----
----

==== RegistrationRegexDTO

[field_doc,class="example.lm.dataobjects.registration.RegistrationRegexDTO"]
----
----

==== PaymentTypeRegistrationItem

[field_doc,class="example.lm.dataobjects.registration.PaymentTypeRegistrationItem"]
----
----

==== CountryDO

[yuml,type="class",direction="LR",width=600,height=200]
----
[Country{bg:blue}]<>verificationProfileId *..1>[VerificationProfile{bg:yellow}],
[Country]<>pcaProfileId *..1>[PcaProfile{bg:red}],
----

[field_doc,class="example.lm.dataobjects.country.CountryDO"]
----
----

[width=100%",options="header",title="Summary of changes"]
|===
| Field | Comment
| mobileRegex | Country specific mobile regex validation
| pcaProfileId | PCA profile Id
|===

==== VerificationProfileDO

[field_doc,class="example.lm.dataobjects.profile.VerificationProfileDO"]
----
----

==== VerificationProviderDO

[field_doc,class="example.lm.dataobjects.profile.VerificationProviderDO"]
----
----


==== PCAProfileDO


[field_doc,class="example.lm.dataobjects.country.PCAProfileDO"]
----
----

==== ManualFieldDO

[field_doc,class="example.lm.dataobjects.country.ManualFieldDO"]
----
----
