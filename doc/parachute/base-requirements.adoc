== Base Requirements

=== MVP

==== Funds: Logic & Order of Bonus & Cash Use
. End-user *deposits and gets bonus* (With wagering requirements).
. With this type of bonus (cash used first) the *deposited amount* leaves the general cash balance to *sit inside bonus (must be separated out to make it clear to the player).*
. This *cash deposit* is used first for play INSTEAD of bonus.
. If the end-user makes multiple deposits the *specific deposit is tied to the specific bonus.*
. If bonus expires, deposited cash tied to bonus becomes general cash balance. *Bonus balance disappears*
. If bonus is cancelled, deposited cash becomes general cash

[NOTE]
====
*Compliance Questions*
Is it ok for the deposit to visually sit in bonus funds?
E.g. Player has deposited £5 to get £5 bonus- with this parachute bonus the deposited funds are sitting in the total bonus.

*Answer:*
We need to find a way to show the player their bonus and separate cash tied to bonus. This is also ideal for UX. See comments in #8.
====

*More Info*

There is a segregation of different types of funds, based on how they can be used.

. General Cash Balance: Can be used anywhere on the site. Can be withdrawn.
. Locked Deposit (only usable on casino games): Must have option to be withdrawable in specific markets (UK) but will cancel bonus. Can be used on ALL games. Doesn’t have to be shown separately e.g. could be achieved with math and sit in bonus area. *Consider terminology player-facing.
. Bonus Balance: Can be used on qualifying games only. If end-user wins we separate winnings into bonus and cash. End-user only starts using this when the Casino Cash Balance is empty. Contributes to wagering requirements.

Comments:

* NOT all cash is tied to bonus so end-user DOESN’T needs to use all cash before getting bonus INSTEAD just the cash (deposit + winnings) tied to the bonus (see pockets of money below)
* Main difference with Parachute vs post wager (stake) is that with parachute bonus the player needs to lose £100 deposit to get bonus vs stake where they can just stake £100 and if they win £95 they still get bonus.

*Example*

[blue]#E.g. #1 Player deposits £100 to get £100 match bonus#
[code]
====
. General cash balance: £1.5
. Deposited balance (sits in bonus): £100
. Bonus balance: £100
Total balance: £201.50

====



==== Staking Behaviour
. Staking cash and bonus in one stake: possible unless the attempted stake exceeds the max bet limit set. (see max bet epic)
. Mixed stakes are possible e.g. mix of bonus, deposited cash and general cash but limited when ‘max bet/stake' is in use.
. MISSING REQUIREMENT: related to handling max bet

[NOTE]
====
*General Questions/Comments:* What happens when there is a max bet set e.g. can the player still place a bet using bonus and cash for a higher amount?

A: see Max Bet epic.
====

*Example*

[blue]#E.g. #1 Mixed Stake#
[code]
====

A Player could stake 31.5€ at once of the below UNLESS they are playing with a bonus that has a maximum stake limit.

. General balance: 11.5€
. Deposited balance: 10€
. Bonus balance: 10€

image:./parachute/image-stake-eg-1.png[title="Add fund from menu",width="400px"]

====

==== Releasing Bonus For Use
. Deposit should be spent first
. End-user only *starts using bonus* when deposited amount has been lost/spent (deposited balance = 0)
. *Note any winnings that have ‘topped’ up the deposited balance will need to be lost/spent
. Deposit can *ONLY be used on casino games* to release bonus.
. End-user can stake on *any game in casino vertical*


[blue]#Releasing funds#
[code]
====

[blue]#E.g #1. Player deposits £100 to get £100 match bonus#

. General cash balance: £1.5
. Deposited balance (sits in bonus): £100
. Bonus balance: £100
Total balance: £201.50

---THEN
-Stakes £20 → taken from deposited balance (no winnings)
-Stakes £100 → 80 from deposited balance, and 20 from bonus balance

[blue]#E.g. #2  situation when end-user has access to bonus funds#
image:./parachute/image-wagering.png[title="Add fund from menu",width="400px"]


[blue]#E.g #3#

* End-user stakes first £50 from deposited balance (£100-50)
* Player stakes other £50 from deposited balance (50-50)
* Next stake would start using bonus, but if players wins £500 like previous example, then player would continue using deposited balance, till is 0


====


==== Winnings Logic
. *Winnings under initial deposit*: Any winnings from deposited cash balance would top up deposited cash balance till reaching the initial amount of deposit. *If end-user wins they *must lose those winnings or they cannot start using the bonus.*
. *Winnings over the initial deposit amount* go to general cash balance (immediately withdrawable).
. *Winnings* from bonus go to bonus balance (wagering needs to be cleared to turn into cash)
. *Winnings* are split according to make up of stake (if 50 deposited cash and 50 bonus used then split 50/50)

[blue]#Example#
[code]
====

[blue]#E.g.#1#

if £10 cash + £10 bonus are staked then winnings are split 50/50 into cash vs bonus.

Wagering must be met to withdraw bonus winnings unless bonus money has not been used.

e.g. they win on cash they don’t need to complete wagering but early withdrawal forfeits bonus.

[blue]#E.g. #2#

Player deposits £100 to get £100 match bonus (£0 cash balance)

THEN Player stakes £100 and wins £500

. General cash balance: £400
. Deposited balance: £100
. Bonus balance: £100
====

==== Optional: Can add any feature when creating a bonus
. When admin is creating bonus they can pick and choose what elements apply e.g. cash first, bonus release threshold etc.
. Feature flag to enable/disable feature and elements of the above (TBA)

[blue]#Example#
[code]
====
Any feature above can be added by admin when creating any bonus so we have one bonus creation admin and NOT separate tools missing functionality e.g. accept/decline in Monolith.

Consider option for markets like Germany to have a different mechanic e.g. all winnings and cash when a bonus is in play are not withdrawable and are locked in bonus funds. As above, if each feature is optional we can create a custom configuration per market.

====

==== Multiple Bonus Logic & Warnings
. End-users can ONLY have 1 bonus of this type active at 1 time (cash first)
. Deposited balance tied to bonus should be 0 before getting the bonus and bonus should be 0 to consider ‘consumed’
. Bonus expiring first is used first
. If end-user has an active parachute bonus the 2nd parachute bonus will be visible but in a ‘locked’ state (offered not granted)
. ‘Locked’ parachute bonus could have message ‘LOCKED- only 1 bonus of this type can be active at the same time’
. If end-user wants the 2nd parachute bonus instead, they can
.. press cancel to cancel the 1st parachute bonus
.. [.line-through]#withdraw and cancel the 1st bonus or#
.. consume the parachute bonus before depositing to get the 2nd one
. If player has another active wagering bonus in their account, this will be used first (as per current bonus use logic or ideally by expiring first used first)
. Warning message if user tries to deposit to alert them of the offer they are depositing for (see alternative in more info)
. Consider choice in ‘add funds’ page on what deposit contributes to

[NOTE]
====
General Questions/Comments:

. If we go with the option that the 2nd parachute is locked then we wouldn’t need to cancel the first and warn the user
. Can we create logic for order/cancellation of ‘offers’ that are not yet granted to user account?
. Need to consider problem of player going to ‘add funds’ page without clicking on the specific offer they want to deposit against

====

[DANGER]
====
[red]#*Please Note:* after a discussion with compliance we’ll go with ‘cancel bonus' as the only way the player can then withdraw the deposited funds instead of going straight to withdrawal page and being able to enter the amount.#
====

[blue]#More Info#
[code]
====
Current Stacking Wagering Bonus:

Bonus usage rules

(expiring in next 3 days)

-most specific goes first ( 1st Product(specific game or list of games), 2nd ProductType(game. lottery, SC), 3 rd bonus that apply to All product verticals)

-date

(expiring after next 3 days)

-most specific goes first ( 1st Product(specific game or list of games), 2nd ProductType(game. lottery, SC), 3 rd bonus that apply to All product verticals)

-date

Alternative: Another option is to allow the user to deposit and display a warning message and then cancel the other bonus E.g. Message appears saying something like:"By completing a deposit with this promotion, you will forfeit your currently active bonus: {INSERT BONUS NAME HERE}

====

[blue]#Example#
[code]
====

[blue]#E.g. #1#

User wants a 2nd Parachute Bonus during Active 1st Parachute Bonus

End-user has an active parachute bonus with deposited balance:

* General balance: £3
* Deposited balance: £10
* Bonus balance: £100

End-user cannot add another similar (cash first) bonus until the deposited balance £10 + £100 bonus balance are used. Unless they try to withdraw >£3 or press ‘cancel’.

[blue]#E.g. #2 Multiple Bonus Use#

End-user has the following cash/bonuses:
. Wagering bonus for use on Starburst £10 (expires 2 days)
. Wagering bonus for use on slots £20 (expires 2 days)
. Deposits £30 for £25 Parachute bonus (expires in 1 day)
. £5 General Cash

*Ideal scenario:*
End-user opens up Starburst and stakes £90 (assuming there is no max bet limit)

. Deposit cash used first - £30
. Parachute bonus used next (or missed if Starburst is NOT applicable game) -£25
. Wagering bonus for use on Starburst -£10
. Wagering bonus for use on slots -£20
. General cash taken -£5

image:./parachute/image-multi-bonus.png[title="Add fund from menu",width="400px"]

====


==== Bonus Release Threshold
. If after doing a stake the remaining bonus value is below the threshold, it becomes real money.
. The admin user can set a different bonus release threshold for each bundle/offer where if the player has under a certain value in bonus funds then they are converted to general cash.
. This must be optional and configurable in admin.

[NOTE]
====
*General Questions/Comments:*
Is this different/variable for each different offer?
A: Need to configure by offer and game type. Need to consider multiple currencies.

[red]#Q: Is the bonus release threshold needed if we allow mixed stake bets? e.g. if player has only a few cents left they can use general cash or deposit with the remaining bonus to keep playing.#

====

==== Cancel Bonus
. Have an option for the user to ‘cancel’ their bonus so money returns to general cash instead of them having to withdraw. E.g. this might be a button in the ‘My Bonus Funds’ view that allows them to cancel the bonus.
. Depending on market it would go to withdrawable or nonwithdrawable balance, (configurable)
. If player wants to cancel bonus half way through E.g. if free spins are claimed they are not cancelled, if bonus funds are given they can be removed from the account and any money from original deposit is moved to real cash balance.
. What if the player is still completing wagering requirements? they can cancel and the bonus winnings are forfeited (warning message?)
. This option should be configurable for every bonus type so admin user can have this display on any bonus if required
. Should be warning message ‘Are you sure?’
. Is it possible for this also to be mirrored on admin side e.g. admin can go in and cancel bundles/offers on mass

[NOTE]
====
General Questions/Comments:

* *Q:* Do we need to spend extra effort to show actual number to dissuade them from cancelling?
** *A:*  not at this stage
* *Q:* Is there a way that this epic could replace ‘withdraw and forfeit’?
** *A:*  yes this is fine
* *Q:* Could this also help in the interim where there isn’t a bonus release threshold?
** *A:*  not really
* *Q:* Is there value to be able to cancel ANY bonus?
** *A:*  could show us they are not interested and gives us back the money.
* *Q:* What happens if the player withdraws but then cancels withdrawal?
** *A:*  Not actually possible the user needs to contact CS. Need to find out how often this happens. Maybe need to manually add back bonus.

====

==== Specific Payment Methods/ Providers

. Bonus only applicable to specific payment methods/providers
. Marketing admin can select relevant payment methods/providers AND/OR exclude specific payment methods/providers (NOT using template of providers)
. Warning message needed to user that bonus won’t be granted for the wrong payment method

[NOTE]
====
*General Questions/Comments:*

What about when we do multiple languages/currencies- we’ll need to factor in different payment methods for different markets?

====

==== Max Spin and Bet Value

. Can set a max spin value or value of bet.
. Need to consider different values for VIP.
. Max bet on bonus funds only
. Needs warning in game to say that max bet on bonus funds is X
. Need to make it clear to player when they are in-game what what funds they are using e.g. bonus or cash
. Wagering requirements are only on bonus
. Winnings split according to portion of funds used (see winnings epic)
. Set wagering requirements: ideally 30x- should be configurable
. *(Need to confirm) Recommended Option:* if player is playing with ‘casino cash or cash tied to bonus’ they can stake the entire casino cash amount + max bet set for bonus funds. However if they start with bonus funds (no casino cash left) they will be limited to the max bet amount.

[NOTE]
====

*General Questions/Comments:*

Q: Needs warning in game to say that max bet on bonus funds is X ?
A: Yes and the type of funds cash/bonus being used should be clear when player is in provider screen playing the game

*General Questions/Comments:*

Need to confirm what happens when there is a max bet set e.g. can the player still place a bet using bonus and cash for a higher amount?

A: Recommended Option: Still deciding between Option 1 and Option 3.

====

==== Block Duplicate & Risky Accounts
. Ability to block this bonus type from duplicate accounts (1 player doesn’t get both bonuses).
. Consider blacklisting of specific players so they do not receive bonuses

[NOTE]
====
*General Questions/Comments:*

Need to discuss further with Core team
Consider 3rd party provider like SEON

====

==== Max Winnings Cap
. OPTIONAL Setup bonus so maximum cash released by bonus is X.

==== Wagering Weighted Contribution
. Weighted contribution for wagering requirements. E.g. certain games/categories contribute different amounts to wagering requirements.

[NOTE]
====
*General Questions/Comments:*

. Consider include/exclude games/verticals/genres, dynamically updating new casino games? Could games team fix this?
. Clarify if wagering would change offer to offer? Easier if this doesn't change.
====

==== Release Cash, Withdraw & Forfeit Bonus + Warning
. If bonus expires, deposited cash tied to bonus becomes general cash balance. Bonus balance disappears (mentioned in main epic)
. If bonus is cancelled, deposited cash becomes general cash
. The only way a user can withdraw their cash tied to the bonus is to press ‘cancel’ on the bonus and the funds are then moved to general cash and able to be withdrawn
. (OPTIONAL) If end-user seeks to withdraw deposited cash- this is moved to general cash if selected by admin user when creating bonus
. In ALL cases where the deposited cash is withdrawn, the bonus is cancelled
. Withdrawing winnings over the initial deposited cash amount do not forfeit bonus (see winnings)
. Warning Message: if end-user tries to withdraw deposited cash they see a generic message e.g. “You'll forfeit bonus by withdrawing your cash deposit. Ok to proceed?”

[NOTE]
====
*Compliance Questions*

* *Q:* How does the end-user know they can withdraw the deposited cash in the UK? Is it ok for the deposit to visually sit in bonus funds?
** *A:*  Need to show user separation between actual bonus and deposited cash in bonus
* *Q:* What happens if the player withdraws but then cancels withdrawal?
** *A:*  Not actually possible the user needs to contact CS. Need to find out how often this happens. Maybe need to manually add back bonus.

* *Q:* Is this epic needed? Seems to be covered in the other epics- maybe move this to Phase 2 should we decide to allow the user to withdraw directly and not via ‘cancelling’ the bonus first.
** *A:* 
* *Q:* Instead can we make it that the user needs to cancel the bonus to get those funds back and therefore no need for withdrawal warning?
** *A:*  compliance ok for user to press ‘cancel’ button on bonus in order to withdraw.
* *Q:* What is happening with withdrawable balance from a compliance perspective?
** *A:*  in UK deposit needs to be immediately withdrawable (not turn over 1x)

====

==== UX Considerations
. We will need to make it easy for the player to understand what they are getting (T&Cs with example)
. Need to consider how the pockets of money will be show (if at all) and the wording used
. Display for end-user
. E.g. what the player sees in the persistent display at top of site
. When launching a casino game- what the player can see and what can be used?
. Do we need some way to see what is in each pocket and with a name/explanation easy to understand for end users

==== Wallet considerations
. Other wallet considerations for bonus cost attribution:
. Need to separate different pockets of money specifically bonus funds, stakes, bonus stakes, winnings, bonus winnings, real money, withdrawable, not withdrawable etc.

=== Existing Functionality

==== Cap on Number Users
. OPTIONAL E.g. only first 100 players can get this offer.

==== Unique Bundle Page
. Create a landing page for each bundle on unique URL. Will allow any user (logged in or not) to access information and serve compliance requirements with T&Cs expanded.

[NOTE]
====
*General Questions/Comments:*

* How to make this unique bundle page 'sexy' i.e.  background image?
* We still need to solve the problem- what happens if the user clicks out of the 'add funds' page e.g. with shopping cart it shows them visually there is something still in there. How do we remind the player that they still have something to do regardless of whether they are registered?
* Rename the step 'invalid link' to the 'user is not eligible for offer' and flesh out more examples
* Deposit or non-deposit state rather than 'new'
* Ensuring the unique bundle page can take the user directly to play the game
* Logic on what offers and are shown first e.g. if user clicks on two acquisition offers and then deposit
* As a player if I hate the offer I clicked on, how do I remove it and ensure my deposit doesn't count towards it?
====

==== Open Promotions
. *What*: Offer system should be able to create offers that can be claimed by any user registered. We need a new assignation method, so that admin users can create offers that are open to everyone and can be claimed by users from emails, in-site banners or promotions, etc.
Ideally option when creating offer e.g. 1. Open to all (existing players) 2. Upload CSV

==== Bonus funds use
. *What*: the restrictions on the reward offered to an end-user: bonus funds.
We LIMIT the bonus fund use to a specific vertical/game
Bonus funds may not be used on games that are not qualifying games

==== Wagering Requirements
. *What*:  The number of times players must wager must stake their bonus before it becomes cash. A £100 bonus with 10x Wagering Requirements means that £1,000 must be staked before the Bonus Balance becomes cash
Wagering requirements are on the bonus only (when player is playing with cash this DOESN’T contribute to wagering requirements.)
ONLY bonus contributes to Wagering requirement
When wagering requirements are complete the Bonus Balance becomes general cash.
if the Bonus expires without the user completing wagering requirements then the remaining casino cash balance becomes general cash (same as when bonus is forfeited on withdrawal as the deposit has now become cash)

==== Restrict Bonus to selected list of players
OPTIONAL choice to upload list of players eligible for an offer or ‘offers enabled for anyone’ via link
Can we exclude list of players? no, there is no segmentation on the platform.
Segmentation in platform: target certain affiliate IDs and first deposit. *Need to look at BI flag solution








