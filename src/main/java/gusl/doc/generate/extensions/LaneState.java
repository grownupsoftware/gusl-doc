package gusl.doc.generate.extensions;

/**
 *
 * @author grant
 */
public enum LaneState {

    PASSTHROUGH,
    JUNCTION,
    EMPTY,
    TERMINAL,
}
