package example.lm.dataobjects.registration;

import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@DocClass(description = "Supported payment types")
public class PaymentTypeRegistrationItem {
    @DocField(description = "Logo for payment method")
    private String logoUrl;

    @DocField(description = "Mini logo for url")
    private String miniLogoUrl;

    @DocField(description = "Display order of logos")
    private Integer displayOrder;
}
