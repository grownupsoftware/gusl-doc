/* Copyright lottomart */
package gusl.doc.generate.extensions;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import gusl.core.json.ObjectMapperFactory;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import org.asciidoctor.ast.StructuralNode;
import org.asciidoctor.extension.Reader;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author grant
 */
public class AngularLocalisedMacro extends AbstractMacro {

    protected final GUSLLogger logger = GUSLLogManager.getLogger(this.getClass());

    private static String UI_HOME;

    private static boolean loadedFile = false;

    private static HashMap<String, Object> theTranslationMap = null;
    private static ObjectMapper theObjectMapper;

    private static final TypeReference<HashMap<String, Object>> typeRef
            = new TypeReference<HashMap<String, Object>>() {
    };

    public AngularLocalisedMacro(String name, Map<String, Object> config) {
        super(name, config);
    }

    public static void setUiHome(String uiHome) {
        UI_HOME = new File(uiHome).getAbsolutePath() + "/src/assets/i18n/";
    }

    @Override
    public Object process(StructuralNode parent, Reader reader, Map<String, Object> attributes) {
        try {

//            if (!loadedFile) {
//                loadEn();
//                loadedFile = true;
//            }
//
//            String fromJsonPath = (String) attributes.get("fromPath");
//            if (fromJsonPath == null) {
//                logger.error("No fromJsonPath passed - ignoring");
//                return null;
//            }
//
////            Object jsonObject = theTranslationMap.get(fromJsonPath);
////            builder.append(theObjectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonObject));
//            StringBuilder builder = new StringBuilder();
//            List<String> content = new ArrayList<>();
//
//            builder.append("[source,java,role=\"localised\"]").append(NL).append("====").append(NL);
//            // builder.append(BOLD).append("LOCALISATION").append(BOLD).append(NL).append(NL);
//            // builder.append("Path: ").append(fromJsonPath).append(NL).append(NL);
//
//            content.addAll(Arrays.asList(builder.toString().split("\n")));
//            builder = new StringBuilder();
//            addJsonContent(builder, fromJsonPath);
//            content.addAll(Collections.singletonList(builder.toString()));
//
//            builder = new StringBuilder();
//            builder.append("====").append(NL).append(NL).append(NL);
//            content.addAll(Arrays.asList(builder.toString().split("\n")));
//
//            parseContent(parent, content);

        } catch (Exception ex) {
            logger.error("Error: {}", ex.getMessage(), ex);
        }

        return null;
    }

    private void addJsonContent(StringBuilder builder, String fromJsonPath) throws JsonProcessingException, IOException, IOException {

        HashMap<String, Object> jsonObject = null;

        if (fromJsonPath.indexOf(",") > 0 || fromJsonPath.indexOf(".") > 0) {

            if (fromJsonPath.indexOf(",") > 0) {
                String[] paths = fromJsonPath.split("\\,");
                for (String path : paths) {
                    // logger.info("=====> path: {}", path);
                    jsonObject = null;
                    String[] splits = path.trim().split("\\.");

                    boolean found = false;
                    for (String split : splits) {
                        if (jsonObject == null) {
                            if (!found) {
                                jsonObject = getData(theTranslationMap, split.trim());
                                found = true;
                            }
                        } else {
                            try {
                                jsonObject = getData(jsonObject, split.trim());
                            } catch (JsonMappingException e) {
                                builder.append("\"").append(path).append("\" : ").append("\"").append(jsonObject.get(split.trim())).append("\"").append(NL).append(NL);
                                jsonObject = null;
                            }
                        }

                    }
                    if (jsonObject != null) {
                        builder.append("{ \"").append(path).append("\" : ").append(theObjectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonObject)).append("}").append(NL).append(NL);
                    }

                }

            } else {

                String[] splits = fromJsonPath.split("\\.");

                boolean found = false;
                for (String split : splits) {
                    if (jsonObject == null) {
                        if (!found) {
                            jsonObject = getData(theTranslationMap, split);
                            found = true;
                        }
                    } else {
                        jsonObject = getData(jsonObject, split);
                    }

                }
                if (jsonObject != null) {
                    builder.append(theObjectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonObject));
                }
            }
        } else {
            builder.append(theObjectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(getData(theTranslationMap, fromJsonPath)));
        }

    }

    private void loadEn() throws IOException {
        File file = new File(UI_HOME + "/en.json");

        theObjectMapper = ObjectMapperFactory.getDefaultObjectMapper();
        theTranslationMap = theObjectMapper.readValue(file, typeRef);
    }

    private HashMap<String, Object> getData(HashMap<String, Object> data, String key) throws JsonProcessingException, IOException {
        return theObjectMapper.readValue(theObjectMapper.writeValueAsString(data.get(key)), typeRef);
    }
}
