/*
 * Grownup Software Limited.
 */
package gusl.doc.generate.extensions;

import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import org.asciidoctor.extension.BlockProcessor;

import java.util.Arrays;
import java.util.Map;

/**
 * @author dhudson - Apr 20, 2017 - 9:16:50 AM
 */
public abstract class AbstractMacro extends BlockProcessor {

    protected final GUSLLogger logger = GUSLLogManager.getLogger(this.getClass());

    static final String NL = "\n";

    public AbstractMacro(String name, Map<String, Object> config) {
        super(name, appendConfig(config));
    }

    private static Map<String, Object> appendConfig(Map<String, Object> config) {
        config.put("contexts", Arrays.asList(":listing"));
        return config;
    }

}
