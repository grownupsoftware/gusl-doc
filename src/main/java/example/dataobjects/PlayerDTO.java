package example.dataobjects;

import gusl.annotations.form.FieldType;
import gusl.annotations.form.UiField;
import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;

@DocClass(description = "Player")
public class PlayerDTO {
    @DocField(description = "Player's email address")
    @UiField(type = FieldType.text, label = "Email", displayInTable = true, filterInTable = true)
    private String email;

    @DocField(description = "Player's gender")
    @UiField(type = FieldType.text, label = "Gender", displayInTable = true, filterInTable = true)
    private String gender;

}
