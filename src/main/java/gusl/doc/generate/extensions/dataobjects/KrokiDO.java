package gusl.doc.generate.extensions.dataobjects;

import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class KrokiDO {
    private String diagramSource;
    private String diagramType;
    private String outputFormat;
}
