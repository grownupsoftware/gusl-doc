package example.lm.api;

import example.lm.dataobjects.registration.CountryRegistrationInfoDTO;
import gusl.core.annotations.DocApi;
import gusl.core.exceptions.GUSLErrorException;
import org.glassfish.jersey.server.ManagedAsync;

import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import java.util.List;

@Singleton
@Path(value = "country")
public class CountryResource {

    @GET
    @DocApi(description = "Get list of countries", returnType = CountryRegistrationInfoDTO.class)
    public List<CountryRegistrationInfoDTO> getCountries() throws GUSLErrorException {
        return null;
    }
}
