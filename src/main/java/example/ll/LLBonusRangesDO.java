package example.ll;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class LLBonusRangesDO {

    private List<LLBonusRangeDO> range;

}
