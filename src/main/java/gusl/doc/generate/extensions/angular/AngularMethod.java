/* Copyright lottomart */
package gusl.doc.generate.extensions.angular;

import gusl.core.tostring.ToString;

import java.util.List;

/**
 * @author grant
 */
public class AngularMethod {

    private String name;

    private String description;

    private List<AngularParam> params;

    private AngularParam returnType;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<AngularParam> getParams() {
        return params;
    }

    public void setParams(List<AngularParam> params) {
        this.params = params;
    }

    public AngularParam getReturnType() {
        return returnType;
    }

    public void setReturnType(AngularParam returnType) {
        this.returnType = returnType;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
