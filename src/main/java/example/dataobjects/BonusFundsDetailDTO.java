package example.dataobjects;

import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;

@DocClass(description = "Bonus funds")
public class BonusFundsDetailDTO {
    @DocField(description = "Unique Id")
    private Long id;

}
