package gusl.doc.generate.extensions;

import org.asciidoctor.ast.StructuralNode;
import org.asciidoctor.extension.Reader;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static gusl.doc.generate.extensions.Macro.IMAGE_DIR;

/**
 * @author grant
 */
public class WebSequenceDiagramsMacro extends AbstractMacro {

    private static final String SERVER = "https://www.websequencediagrams.com/";

    private static int imageCounter = 1;

    private final String randomPrefix;

    public WebSequenceDiagramsMacro(String name, Map<String, Object> config) {
        super(name, config);
        randomPrefix = UUID.randomUUID().toString().substring(0, 4);
        // turnOffSecurity();
    }

    // available styles
    // default
    // earth
    // magazine
    // modern-blue
    // mscgen
    // napkin
    // omegapple
    // patent
    // qsd
    // rose
    // roundgreen
    @Override
    public Object process(StructuralNode parent, Reader reader, Map<String, Object> attributes) {

        try {

            String style = "napkin";
            String format = "png";

            String tmpStr = (String) attributes.get("style");
            if (tmpStr != null && !tmpStr.isEmpty()) {
                style = tmpStr;
            }

            tmpStr = (String) attributes.get("format");
            if (tmpStr != null && !tmpStr.isEmpty()) {
                format = tmpStr;
            }

            StringBuilder messageBuilder = new StringBuilder();
            reader.readLines().stream().forEach((line) -> {
                if (!line.isEmpty()) {
                    messageBuilder.append(line).append(NL);//.replace("\n", ","));
                }
            });

            createSequenceImage(style, format, messageBuilder.toString());

            // String imageName = saveImage(urlBuilder.toString());
            //List<Map<String, Object>> params = new ArrayList<>();
            Map<String, Object> paramValues = new HashMap<>();

            paramValues.put("target", "generated/" + "sequence_" + randomPrefix + "_" + imageCounter + ".png");

            return createBlock(parent, "image", "", paramValues);
        } catch (Throwable ex) {
            logger.error("Failed to create web sequence images", ex);
            return createBlock(parent, "pass", Arrays.asList(new String[]{"Failed to create web sequence diagram image\n"}), attributes);
        }

    }

    public String createSequenceImage(String style, String format, String content) throws IOException {

        // Step 1 - post params for image generation
        //Build parameter string
        String data = "style=" + style + "&message="
                + URLEncoder.encode(content, "UTF-8")
                + "&format=" + format
                + "&apiVersion=1";

        // Send the request
        URL url = new URL(SERVER);
        URLConnection conn = url.openConnection();
        conn.setDoOutput(true);
        OutputStreamWriter writer = new OutputStreamWriter(
                conn.getOutputStream());

        //write parameters
        writer.write(data);
        writer.flush();

        // Get the response
        StringBuilder answer = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(
                conn.getInputStream()));
        String line;
        while ((line = reader.readLine()) != null) {
            answer.append(line);
        }
        writer.close();
        reader.close();

        String json = answer.toString();

        int start = json.indexOf("?" + format + "=");
        int end = json.indexOf("\"", start);
        String imgUrl = json.substring(start, end);

        String imageName = saveImage(imgUrl);

        logger.debug("Image name: {}", imageName);

        return imageName;
    }

    private String saveImage(String urlString) throws MalformedURLException, IOException {

        URL url = new URL(SERVER + urlString);

        String imageName = IMAGE_DIR + "sequence_" + randomPrefix + "_" + ++imageCounter + ".png";

        logger.debug("sequence saving to: {} from url: {}", imageName, SERVER + URLEncoder.encode(urlString, "UTF-8"));

        OutputStream out;
        try (InputStream in = new BufferedInputStream(url.openStream())) {
            out = new BufferedOutputStream(new FileOutputStream(imageName));
            for (int i; (i = in.read()) != -1; ) {
                out.write(i);
            }
        }
        out.close();

        final File createdFile = new File(imageName);
        logger.debug("Imaged saved: {} {}", createdFile.getAbsolutePath(), createdFile.exists());

        return imageName;
    }

    /**
     * using https to go to yuml server - don't want cert validation
     */
    public static void turnOffSecurity() {
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            @Override
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }

            @Override
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }};

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
        }
    }

}
