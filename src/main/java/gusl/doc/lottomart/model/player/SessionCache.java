/* Copyright lottomart */
package gusl.doc.lottomart.model.player;

import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;

/**
 *
 * @author grant
 */
@DocClass(description = "Player Session Cache")
public class SessionCache {

    @DocField(description = "Unique player id")
    private String playerId;

    @DocField(description = "Unique player token")
    private String playerToken;

    @DocField(description = "Unique session token")
    private String sessionToken;

    @DocField(description = "unique annonmous id")
    private String anonId;

    @DocField(description = "Unique annoymous token")
    private String anonToken;

    @DocField(description = "Unique analytics Id")
    private String analyticsId;

    @DocField(description = "Unique analytics token")
    private String analyticsToken;

    @DocField(description = "Player's email")
    private String email;

    @DocField(description = "Player's firstname")
    private String firstName;

    @DocField(description = "Player's country code")
    private String countryCode;

    @DocField(description = "Player's currency code")
    private String currencyCode;

    @DocField(description = "Player's currency symbol")
    private String currencySymbol;

    @DocField(description = "Id of the avatar")
    private String avatarId;

    @DocField(description = "Unique device UUID")
    private String uuid;

    @DocField(description = "Status of the player")
    private String playerStatus;

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public String getPlayerToken() {
        return playerToken;
    }

    public void setPlayerToken(String playerToken) {
        this.playerToken = playerToken;
    }

    public String getSessionToken() {
        return sessionToken;
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }

    public String getAnonId() {
        return anonId;
    }

    public void setAnonId(String anonId) {
        this.anonId = anonId;
    }

    public String getAnonToken() {
        return anonToken;
    }

    public void setAnonToken(String anonToken) {
        this.anonToken = anonToken;
    }

    public String getAnalyticsId() {
        return analyticsId;
    }

    public void setAnalyticsId(String analyticsId) {
        this.analyticsId = analyticsId;
    }

    public String getAnalyticsToken() {
        return analyticsToken;
    }

    public void setAnalyticsToken(String analyticsToken) {
        this.analyticsToken = analyticsToken;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    public String getAvatarId() {
        return avatarId;
    }

    public void setAvatarId(String avatarId) {
        this.avatarId = avatarId;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getPlayerStatus() {
        return playerStatus;
    }

    public void setPlayerStatus(String playerStatus) {
        this.playerStatus = playerStatus;
    }

}
