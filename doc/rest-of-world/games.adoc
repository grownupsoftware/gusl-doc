=== Games

include::./game-images.adoc[]



===== Game Images
The assumption is an *image* for a game can contain both language and currency information.

All game images are retrieved from 'Google storage' using a path that includes the cohort's *game path* e.g. '/gb/gbp' i.e. [language]/[currency].

[IMPORTANT]
====
The game path can contain any value, it is purely a prefix to the game image. It could be blank.
====

One observation ... only our scratch game preview images have currency information.

[width=100%",options="header"]
|===
| UK | CA
| image:../img/lm/games/cash_factor_pounds.png[width="200px] | image:../img/lm/games/cash_factor_dollars.png[width="200px]
|===

And a quick look at a foreign casino site, shows that standard game images are shown with no localisation.

image:../img/lm/games/games_foreign.png[width="200px]

So with games ... the use of *game path* is currently forcing a duplication of casino game images.




I recommend splitting *game path* into scratch and casino paths,as below, this would allow casino games to use the same path.

The value for *casinoPath* will be the same for all cohorts, unless we want localisation where the folder will have to be copied and localised images applied.


[yuml,type="class",direction="LR",format="plain",width=600,height=200,filename="games_cohort_paths"]
----
[Cohort|
+...;
-gamePath: String;
+sratchPath: String;
+casinoPath: String;
+...],

----


==== Cohort Game

[width=100%",options="header"]
|===
| Field | UK | CA
| cohort_id  | 1          | 2
| id   | 187  | 188
| auto_play  | true       | true
| bonus_cap  | 15         | 15
| categories  | {"categories":["SLOTS"]}  | {"categories":["SLOTS"]}
| code  | RP_HTML5_AliceInTheWild_Mobile  | RP_HTML5_AliceInTheWild_Mobile
| currency_id  | 10001  | 10005
| diamond_group_id  |   5  | 105
| display_order  | 0  | 0
| game_id  | 454  | 454
| game_labels  | {}  | {}
| gibraltar_tax_cap  | 0  | 0
| hit_rate  | 2.2  | 2.2
| icon_url  | ./assets/images/games/alice-in-the-wild-rubyplay/gb/gbp/game_icon.png  | ./assets/images/games/alice-in-the-wild-rubyplay/ca/cad/game_icon.png
| language_code  | en  | en
| link_color  | NULL  | NULL
| load_timeout  | 0  | 0
| lobby_url  | NULL  | NULL
| logo_no_brand_url  | NULL  | NULL
| logo_url  | ./assets/images/games/alice-in-the-wild-rubyplay/gb/gbp/game_icon_var.png  | ./assets/images/games/alice-in-the-wild-rubyplay/ca/cad/game_icon_var.png
| max_progressive_jackpot  | {"currency-id":10001,"value":6400000}  | {"currency-id":10005,"value":6400000}
| max_stake  | {"currency-id":10001,"value":4000}  | {"currency-id":10005,"value":4000}
| min_stake  | {"currency-id":10001,"value":20}  | {"currency-id":10005,"value":20}
| name  | #Alice in the Wild  | #Alice in the Wild
| new_game_categories  | {1571230083278, 1571230083287, 1571230083288, 1571230083297, 13110556731460, 18102585885889}  | {12107942935622, 15107942885021, 15107942937247}
| orientation  | {"portrait":true,"landscape":true}  | {"portrait":true,"landscape":true}
| overlay_url  | NULL  | NULL
| overwritten_diamond_group_id  |   | NULL
| overwritten_status  | NULL  | NULL
| popular  | true  | true
| preview_line  | #Alice in the Wild  | #Alice in the Wild
| preview_url  | ./assets/images/games/alice-in-the-wild-rubyplay/gb/gbp/preview.jpg  | ./assets/images/games/alice-in-the-wild-rubyplay/ca/cad/preview.jpg
| prizes_article  | NULL  | NULL
| progressive  | false  | false
| progressive_admin_fee  | 0  | 0
| progressive_contribution  | 0  | 0
| provider  | PARI_PLAY  | PARI_PLAY
| provider_reference  |   |
| purchase_options  | {}  | NULL
| reference  | NULL  | NULL
| return_to_player  | 96.4  | 96.4
| royalty  | 12.5  | 12.5
| rules_article  |   | NULL
| safe_preview_url  | NULL  | NULL
| safe_tab_url  | ./assets/images/games/alice-in-the-wild-rubyplay/gb/gbp/tile_large.jpg  | ./assets/images/games/alice-in-the-wild-rubyplay/ca/cad/tile_large.jpg
| selected_url  | NULL  | NULL
| settled_bin  | 13110556731455  | 13110556731461
| since_build_id  | 0  | 0
| status  | ACTIVE  | ACTIVE
| strap_line  | Join Alice, the rabbit and the Cheshire cat on an amazing adventure!  | Join Alice, the rabbit and the Cheshire cat on an amazing adventure!
| supports_free_spins  | false  | false
| tab_url  | ./assets/images/games/alice-in-the-wild-rubyplay/gb/gbp/tile_large_alt.jpg  | ./assets/images/games/alice-in-the-wild-rubyplay/ca/cad/tile_large_alt.jpg
| tags  | ['{"id":"paylines","title":"Paylines ..."}']  | ['{"id":"paylines","title":"Paylines..."}']
| tax_cap  | 21  | 21
| technical_description  | #Alice in the Wild is a 5-reel, 4-row slot with 50 fixed paylines.  | #Alice in the Wild is a 5-reel, 4-row slot with 50 fixed paylines.
| text_color  | #9f2bf6  | #9F2BF6
| ticket_price  | NULL  | NULL
| top_prize  | {"currency-id":10001,"value":6400000}  | {"currency-id":10005,"value":6400000}
| topup_options  | {}  | NULL
| type  | CASINO  | CASINO
| unsettled_bin  | 0  | 0
| video_url  | https://storage.googleapis.com/kickthe/assets/images/games/alice-in-the-wild-rubyplay/gb/gbp/preview.mp4  | https://storage.googleapis.com/kickthe/assets/images/games/alice-in-the-wild-rubyplay/ca/cad/preview.mp4
| volatility  | 2  | 2
|===



* Entities with all fields*

[yuml,type="class",direction="LR",format="plain",width=600,height=200,filename="game"]
----
[Game{bg:wheat}]<>id 1..* gameId>[CohortGame{bg:steelblue}],

[Game|
id: Long;
studio: GameStudioDO;
name: String;
type: GameType;
status: GameStatus;
ukGcType: UkGcType;
code: String;
overwrittenStatus: String;
provider: GameProvider;
categories: GameCategoriesDO;
volatility: Integer;
orientation: GameOrientationDO;
progressives: Boolean;
progressiveContribution: Float;
progressiveAdminFee: Float;
royalty: Float;
bonusCap: Float;
taxCap: Float;
gibraltarTaxCap: Float;
supportsFreeSpins:  ;
studioName: String;
providerReference: String;
returnToPlayer: Float;
hitRate: Float;
autoPlay: Boolean;
tags: List;
dateReleased: Date;
gameLabels: Set],


[CohortGame|
id: Long;
cohortId: Long;
gameId: Long;
currencyId: Long;
status: String;
popular: Boolean;
overlayUrl: String;
logoUrl: String;
lobbyUrl: String;
tabUrl: String;
selectedUrl: String;
name: String;
purchaseOptions: GamePacksDO;
topupOptions: GamePacksDO;
ticketPrice: MoneyDO;
unsettledBin: Long;
settledBin: Long;
backgroundColor: String;
linkColor: String;
textColor: String;
logoNoBrandUrl: String;
previewUrl: String;
iconUrl: String;
rulesArticle: String;
prizesArticle: String;
strapLine: String;
previewLine: String;
safeTabUrl: String;
safePreviewUrl: String;
videoUrl: String;
loadTimeout: String;
sinceBuildId: String;
languageCode: String;
minStake: MoneyDO;
maxStake: MoneyDO;
topPrize: MoneyDO;
progressive: Boolean;
supportsFreeSpins: Boolean;
progressiveContribution: Float;
progressiveAdminFee: Float;
royalty: Float;
bonusCap: Float;
taxCap: Float;
gibraltarTaxCap: Float;
gameLabels: Set;
newGameCategories: Set;
diamondGroupId: Long;
maxProgressiveJackpot: MoneyDO;
type: GameType;
code: String;
provider: GameProvider;
categories: GameCategoriesDO;
volatility: Integer;
orientation: GameOrientationDO;
providerReference: String;
returnToPlayer: Float;
hitRate: Float;
autoPlay: Boolean;
tags: List;
dateReleased: String;
dateCreated: Date;
dateUpdated: Date;
technicalDescription: String;
displayOrder: Integer;
reference: String;
overwrittenStatus: String]


----



