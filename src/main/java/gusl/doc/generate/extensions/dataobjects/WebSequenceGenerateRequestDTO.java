/* Copyright lottomart */
package gusl.doc.generate.extensions.dataobjects;

/**
 *
 * @author grant
 */
public class WebSequenceGenerateRequestDTO {

    // available styles
    //default
    //earth
    //magazine
    //modern-blue
    //mscgen
    //napkin
    //omegapple
    //patent
    //qsd
    //rose
    //roundgreen    
    private String style;

    // actual content
    private String message;

    private String apiVersion = "1";

    // "png", "pdf", or "svg"    
    private String format;

    public WebSequenceGenerateRequestDTO() {
    }

    public WebSequenceGenerateRequestDTO(String style, String format, String message) {
        this.style = style;
        this.message = message;
        this.format = format;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    @Override
    public String toString() {
        return "WebSequenceGenerateRequestDTO{" + "style=" + style + ", message=" + message + ", apiVersion=" + apiVersion + ", format=" + format + '}';
    }

}
