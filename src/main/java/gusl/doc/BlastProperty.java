/*
 * Grownup Software Limited.
 */
package gusl.doc;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Property Annotation.
 *
 * Use this annotation for properties, which is then picked up by the
 * documentation service.
 *
 * @author dhudson
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface BlastProperty {

    /**
     * Description of the field value
     *
     * @return description
     */
    String description();

    /**
     * All blast properties should have a sensible default value
     *
     * @return what the default value is.
     */
    String defaultValue() default "";
}
