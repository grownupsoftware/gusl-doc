package gusl.doc.yaml;

import gusl.doc.yaml.dataobjects.UserStoriesDO;
import gusl.doc.yaml.dataobjects.UserStoryDO;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author grant
 */
public class StoryBuilder {

    public static void main(String[] args) throws Exception {
        StoryBuilder storyBuilder = new StoryBuilder();
        storyBuilder.build();

        try {
        } finally {
            Thread.sleep(2000);
        }

    }

    public List<UserStoriesDO> build() {

        List<UserStoriesDO> stories = new ArrayList<>();

        //stories.addAll(settings());
        stories.addAll(adyen());

        return stories;
    }

//POST /adyen/addCard
//POST /adyen/removeCard
//POST /adyen/session
    private List<UserStoriesDO> adyen() {
        List<UserStoriesDO> stories = new ArrayList<>();

        UserStoriesDO story1 = new UserStoriesDO("Get Credit Cards");
        story1.getStories().add(new UserStoryDO("Get Cards", "adyen/getCards"));
        stories.add(story1);

        story1 = new UserStoriesDO("Add Credit Card");
        story1.getStories().add(new UserStoryDO("Add Card", "adyen/addCard"));
        stories.add(story1);

        story1 = new UserStoriesDO("Remove Credit Card");
        story1.getStories().add(new UserStoryDO("Remove Card", "adyen/removeCard"));
        stories.add(story1);

        story1 = new UserStoriesDO("Withdraw to Credit Card");
        story1.getStories().add(new UserStoryDO("Withdraw", "adyen/withdraw"));
        stories.add(story1);

        story1 = new UserStoriesDO("Payment from Credit Card");
        story1.getStories().add(new UserStoryDO("Payment", "adyen/payment"));
        stories.add(story1);

        return stories;
    }

//POST /player/settings/changestatus
//POST /player/settings/email
//POST /player/settings/password
//POST /player/settings/requirepassword
//POST /player/settings/secretoffers
//POST /player/settings/selfexclusion
//POST /player/settings/sound
//POST /player/settings/player
//POST /player/settings/timeout
//POST /player/settings/validpassword
    private List<UserStoriesDO> settings() {
        List<UserStoriesDO> stories = new ArrayList<>();

        UserStoriesDO story1 = new UserStoriesDO("Change email");
        story1.getStories().add(new UserStoryDO("Validate password", "player/settings/validpassword"));
        story1.getStories().add(new UserStoryDO("Change email", "player/settings/email"));
        stories.add(story1);

        story1 = new UserStoriesDO("Change password");
        story1.getStories().add(new UserStoryDO("Validate password", "player/settings/validpassword"));
        story1.getStories().add(new UserStoryDO("Change password", "player/settings/password"));
        stories.add(story1);

        story1 = new UserStoriesDO("Require password");
        story1.getStories().add(new UserStoryDO("Never", "player/settings/requirepassword"));
        story1.getStories().add(new UserStoryDO("15 minutes", "player/settings/requirepassword"));
        story1.getStories().add(new UserStoryDO("1 day", "player/settings/requirepassword"));
        story1.getStories().add(new UserStoryDO("1 week", "player/settings/requirepassword"));
        stories.add(story1);

        story1 = new UserStoriesDO("Close Account");
        story1.getStories().add(new UserStoryDO("Close account", "player/settings/changestatus"));
        stories.add(story1);

        story1 = new UserStoriesDO("Sound");
        story1.getStories().add(new UserStoryDO("Sound On", "player/settings/sound", false));
        story1.getStories().add(new UserStoryDO("Sound Off", "player/settings/sound", false));
        stories.add(story1);

        story1 = new UserStoriesDO("Secret Offers");
        story1.getStories().add(new UserStoryDO("Offers On", "player/settings/secretoffers"));
        story1.getStories().add(new UserStoryDO("Offers Off", "player/settings/secretoffers"));
        stories.add(story1);

        story1 = new UserStoriesDO("Set Spending Limit");
        story1.getStories().add(new UserStoryDO("Exclude", "player/settings/player"));
        stories.add(story1);

        story1 = new UserStoriesDO("Timeout");
        story1.getStories().add(new UserStoryDO("None", "player/settings/timeout"));
        story1.getStories().add(new UserStoryDO("1 Day", "player/settings/timeout"));
        story1.getStories().add(new UserStoryDO("2 Days", "player/settings/timeout"));
        story1.getStories().add(new UserStoryDO("1 Week", "player/settings/timeout"));
        story1.getStories().add(new UserStoryDO("1 Month", "player/settings/timeout"));
        stories.add(story1);

        return stories;
    }

}
//Edge
//POST /adyen/addCard
//POST /adyen/removeCard
//POST /adyen/session
//POST /auth/analyticstoken
//POST /auth/anontoken
//POST /auth/checkemail
//POST /auth/createSession
//GET /auth/info
//POST /auth/login
//POST /auth/logout
//POST /auth/pwreset
//POST /auth/pwtoken
//POST /auth/register
//POST /auth/terms
//POST /basket/add/anonBetSlip
//POST /basket/add/anonQuickpick
//POST /basket/add/playerBetSlip
//POST /basket/add/playerQuickpick
//POST /basket/payment
//POST /basket/paymentoptions
//POST /basket/remove/anonBetSlip
//POST /basket/remove/playerBetSlip
//POST /basket/update/anonBetSlip
//POST /basket/update/playerBetSlip
//POST /diamonds/optin
//POST /games/launch
//GET /hc
//POST /logs/analytic_logs
//POST /logs/anon_logs
//POST /logs/player_logs
//POST /logs_old/anon
//POST /logs_old/player
//POST /mybuys/all
//POST /player/anondata
//POST /player/maindata
//POST /player/settings/changestatus
//POST /player/settings/email
//POST /player/settings/password
//POST /player/settings/requirepassword
//POST /player/settings/secretoffers
//POST /player/settings/selfexclusion
//POST /player/settings/sound
//POST /player/settings/player
//POST /player/settings/timeout
//POST /player/settings/validpassword
//POST /postcode/lookup
//GET /settings/registration
//POST /transaction/all
