
== Deliverables


=== Business Scope and deliverables

. Review requirements (commercial, competitive and compliance)
+
* Reviewed <<lottoland-requirements,Lottoland requirements>>
* Performed <<competitive-analysis,Competitor analysis>>
* Grownup Software <<gs-bonus-recommendations,recommendations>>
+

. View on prioritisation of features (based on expected value and complexity)
+
* Competitor analysis indicates to be competitive all current requirements need to be delivered.
* Some future thought should be given to:
** Cash back
** Bonus based player journeys
** Realtime 'bad-experience' bonus. (i.e. just lost £1k here is x free spins)
+

. Wallet considerations to support future bonus types and features
* First phase was reviewing a wallet _implementation_ of a Loyalty bonus type. <<gs-wallet-recommendations,See observations.>>
* Plan is to review additional bonus types.

. Bonus cost and tracking
+
jj
+
. Other bonus/discount types e.g. shopping cart discounts
+
jj
+
. Multi-market, currency, languages and localisation considerations
+
jj
+


. Business Report & Presentation*
+
* Analysis and assessment of requirements and suggest prioritisation (linked to flow charts)
+



=== Technical Scope and deliverables

. Reviewing (find pain points) of technical architecture
+
* Lottoland identified major game providers do not use OMNI. <<gs-omni-recommendations,see recommendation>>
+

. Technical break down of high-level requirements
+
In the next couple of weeks the plan is to flush out all elements of parachute bonus. <<gs-bonus-mkt-next-steps,see epics>>
+

. Planning implementation and defining test cases
+
jj
+
. Assess technical implications of Risk and fraud considerations and mitigation strategies/ features
+
jj
+

. SWOT analysis of the architecture / platform
+
jj
+
. Review implementation (OMNI bonuses, parachute bonus, wallet and TBD)
+
In progress, meetings with OMNI and wallet teams.
+

. Flow chart for how bonus money flows
+
Outstanding, will be completed as part of epics review and update.
+

. Wallet considerations to support future bonus types and features • Bonus cost and tracking
+
jj
+

. Other bonus/discount types
+
jj
+

. Multi-market, currency, languages and localisation considerations
+
jj
+

. Technical Report & Presentation*

. SWOT analysis of architecture, flow chart for how bonus money flows, recommendations for technical breakdown and assessment of risk/fraud

. Review & Support: planning and implementation


