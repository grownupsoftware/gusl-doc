package example.lm.dataobjects.profile;

public enum VerificationProviderStatus {
    ACTIVE,
    IN_ACTIVE
}
