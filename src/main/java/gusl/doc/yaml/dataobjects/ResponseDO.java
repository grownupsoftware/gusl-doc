package gusl.doc.yaml.dataobjects;

/**
 *
 * @author grant
 */
public class ResponseDO {

    private int statusCode;
    private String className;

    public ResponseDO() {
    }

    public ResponseDO(int statusCode) {
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

}
