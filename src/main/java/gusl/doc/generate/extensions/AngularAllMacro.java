/* Copyright lottomart */
package gusl.doc.generate.extensions;

import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import org.asciidoctor.ast.StructuralNode;
import org.asciidoctor.extension.Reader;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static gusl.doc.BaseDocumentor.*;

/**
 * @author grant
 */
public class AngularAllMacro extends AbstractAngular {

    protected final GUSLLogger logger = GUSLLogManager.getLogger(this.getClass());

    private static String UI_HOME;

    public AngularAllMacro(String name, Map<String, Object> config) {
        super(name, config);
    }

    public static void setUiHome(String uiHome) {
        UI_HOME = new File(uiHome).getAbsolutePath();
    }

    @Override
    public Object process(StructuralNode parent, Reader reader, Map<String, Object> attributes) {

        String dir = (String) attributes.get("dir");

        List<SourceCode> files = new ArrayList<>();

        File parentDir = new File(UI_HOME + File.separator + dir);

        buildDirectoryContents(parentDir, files);

        files.sort((a, b) -> a.name.compareTo(b.name));

        List<String> content = new ArrayList<>();

        for (SourceCode sourceCode : files) {
            StringBuilder builder = new StringBuilder();

            String[] lines = readFile(sourceCode.file.getAbsolutePath());

//            content.add(SUB_HEADER_2 + " " + sourceCode.name.replace(".ts", ""));
//            content.add(NL);
//            content.add(NL);
            builder.append(NL).append(NL).append(HEADER).append(sourceCode.name.replace(".ts", name)).append(NL).append(NL);

            builder.append(BOLD).append(sourceCode.file.getName().replace(".ts", "")).append(BOLD).append(NL).append(NL);
            builder.append("Source: ").append((getPath(parentDir.getAbsolutePath(), sourceCode.file.getAbsolutePath()))).append(NL).append(NL);

            boolean header = true;
            boolean method = false;
            builder.append(OPEN_TABLE_NO_HEADER).append(NL);

//        builder
//                .append(HDR_COL).append("Source")
//                .append(HDR_COL).append(sourceCode)
//                .append(NL);
            for (int x = 0; x < lines.length; x++) {
                if (lines[x].indexOf("@type") > 0) {
                    doType(builder, lines, x);
                }
                if (lines[x].indexOf("@name") > 0) {
                    doName(builder, lines, x);
                }
                if (lines[x].indexOf("@description") > 0) {
                    doDescription(builder, header, lines, x);
                }
                if (lines[x].indexOf("@method") > 0) {
                    if (header) {
                        header = false;
                        builder.append(CLOSE_TABLE).append(NL);
                        builder.append(OPEN_TABLE).append(NL);

                        builder
                                .append(HDR_COL).append("Method")
                                .append(HDR_COL).append("Description")
                                .append(NL);

                    }
                    doMethod(builder, lines, x);
                    method = true;
                }
            }

            if (method || header) {
                builder.append(CLOSE_TABLE).append(NL);
            }

// * @type Page
// * @name LobbyPage
// * @description Main lobby page
//        
            builder.append(NL);
            content.addAll(Arrays.asList(builder.toString().split("\n")));
        }

        parseContent(parent, content);

        return null;
    }

    private String[] readFile(String filename) {
        try {
            Path filePath = new File(filename).toPath();
            Charset charset = Charset.defaultCharset();
            List<String> stringList = Files.readAllLines(filePath, charset);
            String[] stringArray = stringList.toArray(new String[]{});
            return stringArray;

        } catch (IOException ex) {
            return new String[0];
        }
    }

    private void buildDirectoryContents(File dir, List<SourceCode> treeLines) {
        if (dir.exists()) {
//            try {
            File[] files = dir.listFiles();
            for (File file : files) {
                if (file.isDirectory()) {
                    buildDirectoryContents(file, treeLines);
                } else {
                    if (file.getName().endsWith(".ts")) {
                        treeLines.add(new SourceCode(file.getName(), file));
                    }
                }
            }
//            } catch (IOException ex) {
//                logger.error("error", ex);
//            }
        }
    }

    private String getPath(String basePath, String fileName) {
        return fileName.substring(basePath.length() + 1);
    }

    private class SourceCode {

        public String name;
        public File file;

        public SourceCode(String name, File file) {
            this.name = name;
            this.file = file;
        }

    }

}
