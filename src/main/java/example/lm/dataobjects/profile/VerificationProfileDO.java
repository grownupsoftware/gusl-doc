package example.lm.dataobjects.profile;

import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@DocClass(description = "Verification settings")
public class VerificationProfileDO {

    @DocField(description = "Unique ID of profile")
    private Long id;

    @DocField(description = "Id of verification provider for initial verfication e.g. GBG")
    private Long verificationProviderId;

    @DocField(description = "Id of verification provider for documentation e.g. Hooyu")
    private Long documentationProviderId;

    @DocField(description = "Verification type")
    private VerificationType verificationType;

    @DocField(description = "if zero it means no grace period and user needs to be verified to purchase/play how long we gently remind player about verification")
    private String verificationGracePeriod;

    @DocField(description = "if zero it means no grace period and user with verified age needs to be verified to purchase/play. how long we gently remind player with verified age about verification")
    private String verificationGracePeriodAgeVerified;

    @DocField(description = "if gbg check on this threshold is exceeded then we set identityVerified=true on player")
    private Integer identityVerifiedThreshold;

    @DocField(description = "if gbg check on this threshold is exceeded then we set ageVerified=true on player")
    private Integer ageVerifiedThreshold;

    @DocField(description = "Minimum valid age")
    private Integer minAge;

}
