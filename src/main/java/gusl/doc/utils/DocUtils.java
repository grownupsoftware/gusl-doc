/*
 * @author dhudson -
 * Created 22 May 2012 : 06:11:28
 */
package gusl.doc.utils;


import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Set;

/**
 * Class of static methods that are general utils
 *
 * @author dhudson - created 25 Jun 2012
 * @since 1.0
 */
public final class DocUtils {

    /**
     * Constructor.
     *
     * @since 1.0
     */
    private DocUtils() {
    }

    /**
     * Scans package for annotated classes with specified annotation type.
     *
     * @param packageName package name
     * @param annotatedClass Annotation class
     * @return a set of annotated classes
     * @since 1.0
     */
    public static Set<Class<?>> getAnnotatedClasses(String packageName, Class<? extends Annotation> annotatedClass) {
        ConfigurationBuilder builder = new ConfigurationBuilder();
        builder.addUrls(ClasspathHelper.forPackage(packageName));
        builder.addScanners(new TypeAnnotationsScanner());
        Reflections reflections = new Reflections(builder);

        return reflections.getTypesAnnotatedWith((Class<? extends Annotation>) annotatedClass);
    }

    /**
     * Scan the given package for methods with the given annotation.
     *
     * @param packageName to search
     * @param annotation to look for
     * @return a set of methods with the annotation
     * @since 1.0
     */
    public static Set<Method> getAnnotatedMethods(String packageName, Class<? extends Annotation> annotation) {
        ConfigurationBuilder builder = new ConfigurationBuilder();
        builder.addUrls(ClasspathHelper.forPackage(packageName));
        builder.addScanners(new MethodAnnotationsScanner());
        Reflections reflections = new Reflections(builder);

        return reflections.getMethodsAnnotatedWith(annotation);
    }

}
