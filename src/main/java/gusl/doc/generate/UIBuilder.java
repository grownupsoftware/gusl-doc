/* Copyright lottomart */
package gusl.doc.generate;

import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.doc.BaseDocumentor;
import gusl.doc.exceptions.DocException;
import gusl.doc.generate.builders.ChangeAttributeValuePreprocessor;
import gusl.doc.generate.builders.DocumentationType;
import gusl.doc.generate.extensions.*;
import org.apache.commons.cli.*;
import org.apache.commons.io.FileUtils;
import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Attributes;
import org.asciidoctor.Placement;
import org.asciidoctor.SafeMode;
import org.asciidoctor.extension.JavaExtensionRegistry;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import static org.asciidoctor.Asciidoctor.Factory.create;
import static org.asciidoctor.OptionsBuilder.options;

/**
 * @author grant
 */
public class UIBuilder extends BaseDocumentor {

    private final String theOutputDir;

    public static final GUSLLogger logger = GUSLLogManager.getLogger(UIBuilder.class);

    private static final String DOC_LOCATIONS[] = new String[]{"/..", "/../.."};

    private static final String IGNORE[] = new String[]{"lotto-doc"};

    public UIBuilder(String outputDir) {
        theOutputDir = outputDir;
    }

    private void createDocumentation() {
        StringBuilder builder = buildDocumentation();
        writeHtmlFile("ui_man", builder);
    }

    @SuppressWarnings("deprecation")
    private StringBuilder buildDocumentation() {
        StringBuilder builder = new StringBuilder();
        try {
            builder.append(FileUtils.readFileToString(new File("./doc/ui_manual/pages.adoc")));

        } catch (IOException ex) {
            logger.error("Failed to find external start doc: {}", "./doc/ui_manual/pages.adoc");
        }
        return builder;
    }

    private void writeHtmlFile(String name, StringBuilder content) {
        try {

            Asciidoctor asciidoctor = createDoctor();
            DocumentationType documentationType = DocumentationType.HTML;
            // DocumentationType documentationType = DocumentationType.PDF;
            Map<String, Object> options = createOptions("UI", new File("src/main/resources/webapp/player_ui"), documentationType);

            switch (documentationType) {
                case PDF:
                    options.put("to_file", "player_ui.pdf");
                    break;
                case HTML:
                case HTML_DECK:
                    options.put("to_file", name + ".html");
                    break;
                default:
                    throw new AssertionError(documentationType.name());
            }

            asciidoctor.convert(content.toString(), options);

        } catch (Exception e) {
            logger.error("There was an error", e);
        }

    }

    void moveImages(String docName, String outputDir) {
        File doc = new File(docName);
//        logger.info("copy from :{}  to  {}", doc.getParent(), outputDir + "/images");
        try {
            FileUtils.copyDirectory(new File(doc.getParent() + "/images"), new File(outputDir + "/images"));
        } catch (IOException ex) {
            logger.error("failed to copy images from: {} to {}", doc.getParent() + "/images", outputDir + "/images");
        }
    }

    private Asciidoctor createDoctor() {
        Asciidoctor asciidoctor = create();

        JavaExtensionRegistry extensionRegistry = asciidoctor.javaExtensionRegistry();

        extensionRegistry.preprocessor(ChangeAttributeValuePreprocessor.class);

        extensionRegistry.block("tree", TreeMacro.class);
        extensionRegistry.block("dir_listing", NewDirListing.class);
        extensionRegistry.block("yuml", YumlMacro.class);
        extensionRegistry.block("field_doc", FieldsExtension.class);
        extensionRegistry.block("rest_doc", RestExtension.class);
        extensionRegistry.block("event_doc", EventMacro.class);
        extensionRegistry.block("example_doc", ExamplesExtension.class);

        return asciidoctor;
    }

    private String calcRootPrefix(String outputDir, File destFile) {

        File outDir = new File(outputDir);
        String dest = destFile.getAbsolutePath().replace(outDir.getAbsolutePath(), "");

        StringTokenizer stOR = new StringTokenizer(dest, "/");

        StringBuilder builder = new StringBuilder();
        for (int x = 0; x < stOR.countTokens(); x++) {
            builder.append("../");
        }
        return builder.toString();
    }

    private Map<String, Object> createOptions(String title, File destFile, DocumentationType documentationType) {
        String rootPrefix = calcRootPrefix(theOutputDir, destFile);

        Attributes attributes = new Attributes();
        if (documentationType != DocumentationType.HTML_DECK) {
            attributes.setTableOfContents(true);
            attributes.setTableOfContents2(Placement.LEFT);
            attributes.setLinkCss(true);
        }
        attributes.setTitle(title);

        Map<String, Object> attributeMap = attributes.map();
        if (documentationType == DocumentationType.HTML_DECK) {
            attributeMap.put("deckjsdir", rootPrefix + "js/deck.js");
        }

        attributeMap.put("icons", "font");
        attributeMap.put("stylesdir", rootPrefix + "css");
        attributeMap.put("stylesheet", "lottomart-asciidoctor.css");
        attributeMap.put("source-highlighter", "coderay");
        attributeMap.put("toc-title", "Lottomart");
        attributeMap.put("toclevels", "3");
        attributeMap.put(":docinfo:", "shared");

        Map<String, Object> options = options()
                .safe(SafeMode.UNSAFE)
                .toDir(destFile)
                .mkDirs(true)
                .attributes(attributes)
                .attributes(attributeMap)
                .option("backend", documentationType.getType())
                .asMap();

        if (documentationType == DocumentationType.HTML_DECK) {
            List<String> templateDirs = new ArrayList<>();
            templateDirs.add("./doc/templates/haml");
            options.put("template_dirs", templateDirs);
        }

        return options;
    }

    public static void main(String... args) throws DocException, IOException, InterruptedException {

        try {
            Options options = new Options();
            options.addOption("output_dir", true, "Destination Directory");

            String outputDir;
            try {
                CommandLineParser parser = new DefaultParser();
                CommandLine commandLine = parser.parse(options, args);
                outputDir = commandLine.getOptionValue("output_dir");
                if (outputDir == null || outputDir.isEmpty()) {
//                    logger.error("No root project directory specified");
//                    throw new DocException("No root project directory specified");
                    outputDir = "/tmp";
                }

                logger.info("Output directory: {}", outputDir);
            } catch (ParseException ex) {
                throw new DocException("Error parsing options");
            }

            UIBuilder builder = new UIBuilder(outputDir);
            builder.createDocumentation();

            logger.info("Finished");
        } catch (Exception ex) {
            logger.error("Error", ex);
        } finally {
            Thread.sleep(2000);
        }
    }

}
