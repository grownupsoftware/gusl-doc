/* Copyright lottomart */
package example.lm.dataobjects.country;

import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@DocClass(description = "Contains all the information for a country")
public class CountryDO  {

    @DocField(description = "Unique ID for country")
    private Long id;

    @DocField(description = "Country Name")
    private String name;

    @DocField(description = "ISO3166-1-Alpha-2")
    private String alpha2;

    @DocField(description = "ISO3166-1-Alpha-3")
    private String alpha3;

    @DocField(description = "Dial Code")
    private String dialCode;


    @DocField(description = "ID of the Currency for this country")
    private Long currencyId;


    @DocField(description = "Is it a 'top' country - *Deprecated RoW*")
    private Boolean topCountry;

    @DocField(description = "Do not show country in registration - Deprecated RoW")
    private Boolean hideInRegistration;

    @DocField(description = "Block country in registration - Deprecated RoW")
    private Boolean blockInRegistration;

    @DocField(description = "Block country access - Deprecated RoW")
    private Boolean blockAccess;


    @DocField(description = "Country supports post code, otherwise manual entry - Deprecated RoW")
    private Boolean supportPostCode;

    @DocField(description = "Ordering for top countries - Deprecated RoW")
    private Integer topOrder;

    @DocField(description = "Use country in mobile lookup - Deprecated RoW")
    private Boolean mobileLookup;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
