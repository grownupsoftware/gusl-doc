package gusl.doc.generate;

import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.core.utils.StringUtils;
import gusl.doc.exceptions.DocException;
import gusl.doc.generate.builders.DocumentationType;
import gusl.doc.generate.runners.AbstractDocumentationRunner;
import org.apache.commons.cli.*;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static gusl.core.utils.Utils.safeStream;

/**
 * @author grant
 */
public class GradleTaskDocumentationBuilder extends AbstractDocumentationRunner {

    public static final GUSLLogger logger = GUSLLogManager.getLogger(GradleTaskDocumentationBuilder.class);

    private String theSourceFilename;
    private String theTitle;
    List<DocumentationType> theTypes;

    private static final String SOURCE_FILENAME = "source";
    private static final String TITLE = "title";
    private static final String TYPES = "type";

    private static final String DOC_FOLDER = "/Users/grantwallace/dev/azure/gusl-parent/gusl-doc/doc/";

    private static final String OUTPUT_FOLDER = DOC_FOLDER + "../src/main/resources/webapp/";

    public GradleTaskDocumentationBuilder(
            String sourceFile,
            String title,
            List<DocumentationType> types
    ) {
        theSourceFilename = sourceFile;
        theTitle = title;
        theTypes = types;
    }

    private void createDocumentation(CommandLine commandLine) {

        // document("bonus.adoc");
        documentFile(theSourceFilename, theTitle, theTypes);

    }

    private void documentFile(
            String sourceFile,
            String title,
            List<DocumentationType> documentationTypes
    ) {

        safeStream(documentationTypes)
                .forEach(type -> {
                    documentFile(
                            "doc",
                            title,
                            DOC_FOLDER + sourceFile,
                            OUTPUT_FOLDER,
                            // theOutputDir + destinationFile,
                            type);
                });

    }

    public static void main(String... args) throws DocException {

        Options options = new Options();
        options.addOption(SOURCE_FILENAME, true, "Source filename");
        options.addOption(TITLE, true, "Title");
        options.addOption(TYPES, true, "output types");

        String srcFilename;
        String title;
        String types;
        try {
            CommandLineParser parser = new DefaultParser();
            CommandLine commandLine = parser.parse(options, args);
            srcFilename = commandLine.getOptionValue(SOURCE_FILENAME);
            title = commandLine.getOptionValue(TITLE);
            types = commandLine.getOptionValue(TYPES);

            if (StringUtils.isBlank(srcFilename)) {
                logger.error("No source file specified");
                throw new DocException("No source file specified");
            }
            if (StringUtils.isBlank(title)) {
                logger.error("No title");
                throw new DocException("No title specified");
            }
            if (StringUtils.isBlank(types)) {
                logger.error("No types specified ... must have html o rpdf or html,pdf");
                throw new DocException("No types specified");
            }

            GradleTaskDocumentationBuilder builder = new GradleTaskDocumentationBuilder(
                    srcFilename,
                    title,
                    getTypes(types));
            builder.createDocumentation(commandLine);

            // logger.info("Output directory: {}", outputDir);
        } catch (ParseException ex) {
            throw new DocException("Error parsing options");
        }

    }

    private static List<DocumentationType> getTypes(String types) {
        final String[] splits = types.split(",");
        return safeStream(splits)
                .filter(Objects::nonNull)
                .map(split -> DocumentationType.valueOf(split.toUpperCase()))
                .collect(Collectors.toList());
    }

}
