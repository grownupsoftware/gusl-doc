/*
 * Grownup Software Limited.
 */
package gusl.doc;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Description for event producers.
 *
 * @author dhudson
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface BlastEvent {

    public enum EventSource {
        SERVER,
        CLIENT,
        APPLICATION
    }

    String description();

    EventSource source();
}
