package example.lm.dataobjects.anon;


import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Builder;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AnonPlayerDO {

    @DocField(description = "Unique ID")
    private Long id;

    @DocField(description = "")
    private Date createTime;

    @DocField(description = "Player ID - only set after registration - login")
    private Long playerId;

    @DocField(description = "")
    private AnonPropertiesDO properties;

    @DocField(description = "Set when user logins in or registers")
    private Date registeredTime;

    @DocField(description = "")
    private AnonPlayerStatus status;

    @DocField(description = "Unique country id")
    private Long countryId;

    @DocField(description = "Unique currency id")
    private Long currencyId;


    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
