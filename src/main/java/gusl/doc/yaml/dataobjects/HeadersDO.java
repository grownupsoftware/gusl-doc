package gusl.doc.yaml.dataobjects;

/**
 *
 * @author grant
 */
public class HeadersDO {

    private String contentType;

    public HeadersDO() {
    }

    public HeadersDO(String contentType) {
        this.contentType = contentType;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

}
