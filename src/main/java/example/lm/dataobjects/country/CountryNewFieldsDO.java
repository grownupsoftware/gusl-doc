/* Copyright lottomart */
package example.lm.dataobjects.country;

import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@DocClass(description = "New fields to be added to country")
public class CountryNewFieldsDO {

    @DocField(description = "Country specific mobile regex")
    private String mobileRegex;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
