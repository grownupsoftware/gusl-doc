package gusl.doc.exceptions;

import gusl.core.utils.Platform;

import java.util.ArrayList;
import java.util.List;

/**
 * @author grant
 */
public class DocException extends Exception {

    /**
     * Constant {@value}
     */
    private static final long serialVersionUID = 6359601744856420338L;

    /**
     * Creates a new instance of <code>DocfactoException</code> with the given
     * error message and the exception.
     *
     * @param message for the exception
     * @param ex      what caused the exception
     * @since 2.0
     */
    public DocException(String message, Throwable ex) {
        super(message, ex);
    }

    /**
     * Creates a new instance of <code>DocfactoException</code> with an error
     * message.
     *
     * @param message for the exception
     * @since 2.0
     */
    public DocException(String message) {
        super(message);
    }

    /**
     * Returns a full nested exception message for a Exception.
     *
     * @param exception  to textlocalGet
     * @param diagnostic indicates whether a stack trace of the innermost
     *                   exception should be appended to the exception message and exception
     *                   location details added
     * @return the fully expanded exception message
     * @since 2.0
     */
    public static String getFullExceptionMessage(
            Throwable exception,
            boolean diagnostic) {

        // Set up the message buffer
        final StringBuilder outputStringBuilder = new StringBuilder(50);

        String localizedMessage;

        int indent = 0;
        int subIndent = 0;

        Throwable currentException = null;

        for (final Throwable currentCause : getCauseChain(exception)) {

            currentException = currentCause;

            if (indent > 0) {
                // output the pretty printing
                outputStringBuilder.append(Platform.LINE_SEPARATOR);
                for (int count = 0; count < indent; count++) {
                    outputStringBuilder.append(' ');
                }
                outputStringBuilder.append(" \\--");
            }

            if (diagnostic) {
                outputStringBuilder.append(
                        currentException.getClass().getName()).append(": ");
            }
            localizedMessage = currentException.getLocalizedMessage();
            if (localizedMessage == null) {
                localizedMessage = currentException.getClass().getName();
            }
            outputStringBuilder.append(localizedMessage);

            // location details
            if (diagnostic) {
                if (indent > 0) {
                    subIndent = indent + 4;
                }
                appendTopOfStackTrace(
                        currentException, outputStringBuilder, subIndent);
            }

            indent = indent + 2;
        }

        // Stack Trace
        if (diagnostic) {
            appendRemainingStackTrace(
                    exception, currentException, outputStringBuilder, subIndent);
        }

        return outputStringBuilder.toString();
    }

    /**
     * Appends the location details of the top element of an exception's stack
     * trace to a given buffer. This is used for all exceptions in a nested
     * exception print when diagnostic information is required.
     *
     * @param exception to search
     * @param buffer    the buffer to append a line to
     * @param indent    the line indentation
     * @since 2.0
     */
    private static void appendTopOfStackTrace(
            Throwable exception,
            StringBuilder buffer,
            int indent) {

        final StackTraceElement[] elements = exception.getStackTrace();
        if (elements.length > 0) {
            appendStackTraceElement(elements[0], buffer, indent);
        }

    }

    /**
     * This is used to append the remains of an exception's stack trace (other
     * than the top entry which we assume has been already appended) to a buffer
     *
     * @param outerException the top level exception that we are reporting on
     * @param innerException the innermost exception that we require the stack
     *                       trace from
     * @param buffer         the buffer to append all lines of the stack trace other
     *                       than the first to
     * @param indent         line indentation
     * @since 2.0
     */
    private static void appendRemainingStackTrace(
            Throwable outerException,
            Throwable innerException,
            StringBuilder buffer,
            int indent) {

        int stackDepth = -1;
        if (stackDepth < 0) {
            stackDepth = Integer.MAX_VALUE;
        }
        final StackTraceElement[] elements = innerException.getStackTrace();
        for (int count = 1; count < elements.length; count++) {
            appendStackTraceElement(elements[count], buffer, indent);
            if (count >= stackDepth) {
                buffer.append(
                        " ("
                                + Integer.toString(elements.length - count)
                                + " lines suppressed)");
                return;
            }
        }

    }

    /**
     * Appends details of a given stack trace element to a buffer as a single
     * line. This will be in the form className.method(line)
     *
     * @param element the stack trace element
     * @param buffer  the buffer to write a line to
     * @param indent  the line indentation
     * @since 2.0
     */
    private static void appendStackTraceElement(
            StackTraceElement element,
            StringBuilder buffer,
            int indent) {

        if (element == null) {
            return;
        }
        buffer.append(Platform.LINE_SEPARATOR);
        for (int count = 0; count < indent; count++) {
            buffer.append(' ');
        }
        buffer.append("at ");
        buffer.append(element.getClassName());
        buffer.append(".");
        buffer.append(element.getMethodName());
        buffer.append("(");
        buffer.append(Integer.toString(element.getLineNumber()));
        buffer.append(")");
    }

    /**
     * Returns the cause chain for a specified exception with the given
     * exception as the first element and the chain of enclosed exceptions
     * following with the innermost last
     *
     * @param exception to textlocalGet
     * @return cause chain (outermost first)
     * @since 2.0
     */
    public static List<Throwable> getCauseChain(
            Throwable exception) {
        final ArrayList<Throwable> causeChain
                = new ArrayList<Throwable>();
        if (exception != null) {
            Throwable currentException = exception;
            while (currentException != null) {
                causeChain.add(currentException);
                currentException = currentException.getCause();
            }
        }
        return causeChain;
    }

    /**
     * Returns the root cause of an exception. Where the exception has a cause
     * chain this returns the innermost exception. Where there is no cause chain
     * this returns itself.
     *
     * @param exception to textlocalGet
     * @return the root cause - where there is no root cause this returns the
     * exception itself
     * @since 2.0
     */
    public static Throwable rootCause(Throwable exception) {
        final List<Throwable> causeChain = getCauseChain(exception);
        return causeChain.get(causeChain.size() - 1);
    }

    /**
     * Indicates whether an exception is an instance of or contains an instance
     * of a specified exception class. This will return true if any exception in
     * the cause chain is an instance of (or subclass of) the specified class.
     *
     * @param ex             the exception to check
     * @param exceptionClass the class of exception to check this exception for
     *                       or any enclosed exception
     * @return true if exception is an instance of or contains an exception that
     * is an instance of the specified class
     * @since 2.0
     */
    public static boolean containsInstanceOf(Throwable ex,
                                             Class<?> exceptionClass) {
        if (exceptionClass == null) {
            return false;
        }
        for (final Throwable cause : getCauseChain(ex)) {
            if (exceptionClass.isInstance(cause)) {
                return true;
            }
        }
        return false;
    }
}
