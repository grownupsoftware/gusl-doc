package gusl.doc.generate.runners;

import gusl.core.utils.Utils;
import gusl.doc.generate.builders.DocumentationType;

/**
 * @author grant
 */
public class WalletDocumentation extends AbstractDocumentationRunner {

    public static final String DOC_SRC_FOLDER = "doc";

    private static final String SRC_FOLDER = "../gusl-model/src/main/java";
    private static final String DOC_FOLDER = "/Users/grantwallace/dev/gusl/gusl-doc/doc/";

    private static final String SOURCE_FILE = "wallet/wallet.adoc";
    private static final String OUTPUT_FOLDER = DOC_FOLDER + "../src/main/resources/webapp/";

    private static final DocumentationType[] documentationTypes = new DocumentationType[]{
            DocumentationType.PDF,
            DocumentationType.HTML
    };

    public void generate() {


        Utils.safeStream(documentationTypes)
                .forEach(type -> {
                    documentFile("wallet", "Wallet", DOC_FOLDER + SOURCE_FILE, OUTPUT_FOLDER, type);
                });
    }

    public static void main(String... args) throws Exception {
        WalletDocumentation generator = new WalletDocumentation();
        generator.generate();
    }

}
