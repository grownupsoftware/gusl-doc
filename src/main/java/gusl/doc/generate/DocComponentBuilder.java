package gusl.doc.generate;

import gusl.doc.generate.builders.AbstractDocBuilder;
import gusl.doc.generate.builders.DocumentationType;

public class DocComponentBuilder extends AbstractDocBuilder {

    private static final String OUTPUT_DIR = "/tmp";

    public static void main(String[] args) {
        new DocComponentBuilder().build();
    }

    private void build() {
        document("wagering_profile.adoc");
        document("bonus.adoc");
    }

    private void document(String adoc) {
        document("back_office", adoc, "Wagering Profile", DocumentationType.HTML, adoc.replace(".adoc", ""), OUTPUT_DIR, false);
    }

}
