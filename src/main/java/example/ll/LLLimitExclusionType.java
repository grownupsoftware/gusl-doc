package example.ll;

public enum LLLimitExclusionType {
    USE_CASH_AND_BONUS_WITH_NO_CONTRIBUTION,
    USE_CASH_ONLY_WITH_NO_CONTRIBUTION,
    NO_BET_ALLOWED;

}
