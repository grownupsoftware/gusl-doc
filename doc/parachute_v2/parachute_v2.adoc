image::../img/grownup-logo.png[]

== Parachute Bonus

Author: Grownup Software

include::doc_version.adoc[]

:numbered:


NOTE: The term bonus in this document refers to bonus funds associated with casino play (i.e. parachute bonus)

include::status.adoc[]

include::requirements.adoc[]

include::competition.adoc[]

include::overview.adoc[]

include::usage.adoc[]

include::deliverables.adoc[]
include::recommendations.adoc[]

include::wagering-profile.adoc[]

//include::configuration.adoc[]
//include::promotion.adoc[]
//include::award.adoc[]
//include::usage.adoc[]
//include::withdraw.adoc[]
//include::redeem.adoc[]

//include::base-requirements.adoc[]
//include::integration.adoc[]
//include::use-case.adoc[]
