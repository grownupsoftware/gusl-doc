/* Copyright lottomart */
package gusl.doc.generate;

import com.google.common.collect.Lists;
import gusl.core.utils.Utils;
import gusl.doc.exceptions.DocException;
import gusl.doc.generate.builders.AbstractBuilder;
import gusl.doc.generate.builders.DocumentationType;
import org.apache.commons.cli.*;
import org.asciidoctor.Asciidoctor;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;
import org.reflections.vfs.Vfs;


import java.io.*;
import java.util.*;

/**
 *
 * @author grant
 */
public class BackOfficeBuilder extends AbstractBuilder {

    private static final String OUTPUT_FILE = "generated/backoffice.adoc";

    public BackOfficeBuilder(String outputDir) {
        super(outputDir);
    }

    private void createDocumentation() throws IOException {

        Map<HeaderDO, List<MenuDO>> menuByHeader = readLeftMenu();

        Map<String, ComponentDO> martComponents = readModuleImports("lottomart", "../../lotto-backoffice-ui/src/app/lottomart/lottomart.module.ts");
        Map<String, ComponentDO> botComponents = readModuleImports("lotto-bot", "../../lotto-backoffice-ui/src/app/lotto-bot/lotto-bot.module.ts");

        Map<String, StateDO> martStates = readStates("lottomart", "../../lotto-backoffice-ui/src/app/lottomart/lottomart.module.ts");
        Map<String, StateDO> botStates = readStates("lotto-bot", "../../lotto-backoffice-ui/src/app/lotto-bot/lotto-bot.module.ts");

        Map<String, ComponentDO> allComponents = new HashMap<>();
        allComponents.putAll(martComponents);
        allComponents.putAll(botComponents);

        Map<String, StateDO> allStates = new HashMap<>();
        allStates.putAll(martStates);
        allStates.putAll(botStates);

        createDocumentation(OUTPUT_FILE, menuByHeader, allComponents, allStates);

    }

    private StringBuilder generateBackOfficeDocumentation(Map<HeaderDO, List<MenuDO>> menuByHeader, Map<String, ComponentDO> allComponents, Map<String, StateDO> allStates) {

        StringBuilder indexBuilder = new StringBuilder();

        Map<String, ClassDO> classMap = getAllModelClasses();

        Utils.safeStream(menuByHeader.entrySet()).sorted((Map.Entry<HeaderDO, List<MenuDO>> o1, Map.Entry<HeaderDO, List<MenuDO>> o2) -> o1.getKey().order.compareTo(o2.getKey().order)).forEach(entry -> {
            indexBuilder.append(NL).append(SECTION_HEADER).append(entry.getKey().name).append(NL).append(NL);

            Utils.safeStream(entry.getValue()).forEach(menu -> {
                StringBuilder builder = new StringBuilder();
                builder.append(NL).append(HEADER).append(menu.name).append(NL).append(NL);

                StateDO stateDo = allStates.get(menu.state);
                if (stateDo == null) {
                    logger.info("Failed to find state for {}", menu.state);
                } else {
                    ComponentDO componentDo = allComponents.get(stateDo.component);

                    if (componentDo != null) {
                        builder.append(OPEN_TABLE).append(NL);
//        builder.append(HDR_COL).append("Name")
//                .append(HDR_COL).append("Type")
//                .append(HDR_COL).append("Description")
//                .append(NL);

                        builder.append(COL).append("Component").append(COL).append(componentDo.name).append(NL);
                        builder.append(COL).append("Menu").append(COL).append(stateDo.state).append(NL);
                        builder.append(COL).append("Source").append(COL).append(componentDo.file).append(NL);

                        builder.append(CLOSE_TABLE).append(NL);

                        try {
                            documentMaintain(builder, classMap, componentDo);
                        } catch (IOException ex) {
                            logger.error("error", ex);
                        }
                        indexBuilder.append(builder).append(NL);
                        writeHtmlFile(componentDo.name, builder);
                    }
                }
            });

        });
        return indexBuilder;

    }

    private void documentMaintain(StringBuilder builder, Map<String, ClassDO> classMap, ComponentDO componentDo) throws FileNotFoundException, IOException {
        BufferedReader in = new BufferedReader(new FileReader("../../lotto-backoffice-ui/src/app/" + componentDo.module + "/" + componentDo.file + ".ts"));
        String str;

        Map<String, StateDO> stateMap = new HashMap<>();

        StringBuilder contentBuilder = new StringBuilder();

        while ((str = in.readLine()) != null) {
            contentBuilder.append(str.replace("\n", ""));
        }

        String content = contentBuilder.toString();
        //logger.info("content: {}", content);

        if (content.contains("FORM.MaintainConfigDO")) {
            int start = content.indexOf("FORM.MaintainConfigDO");
            int end = content.substring(start + 22).indexOf(">");
            String dto = content.substring(start + 22, start + 22 + end).replace("Model.", "");

            logger.info("dto: {}", dto);

            ClassDO classDo = classMap.get(dto);
            if (classDo != null) {
                logger.info("found: {}", dto);
            } else {
                classDo = classMap.get(dto.replace("DTO", "DO"));
                if (classDo == null) {
                    logger.info("NOT found: {}", dto);
                }

            }
        }
// FORM.MaintainConfigDO                    

    }

    private void writeHtmlFile(String name, StringBuilder content) {
        try {

            Asciidoctor asciidoctor = createDoctor();
            DocumentationType documentationType = DocumentationType.HTML;
            Map<String, Object> options = createOptions(theOutputDir, "Back Office", new File("src/main/resources/webapp/backoffice"), documentationType);

            switch (documentationType) {
                case PDF:
                    options.put("to_file", "backoffice.pdf");
                    break;
                case HTML:
                case HTML_DECK:
                    options.put("to_file", name + ".html");
                    break;
                default:
                    throw new AssertionError(documentationType.name());
            }

            asciidoctor.convert(content.toString(), options);

        } catch (Exception e) {
            logger.error("There was an error", e);
        }

    }

    private class ClassDO {

        public String simpleName;
        public Class<?> clazz;

        public ClassDO(String simpleName, Class<?> clazz) {
            this.simpleName = simpleName;
            this.clazz = clazz;
        }

    }

    private Map<String, ClassDO> getAllModelClasses() {

        Map<String, ClassDO> classMap = new HashMap<>();

        final List<Vfs.UrlType> urlTypes = Lists.newArrayList();

        // include a list of file extensions / filenames to be recognized
        urlTypes.add(new EmptyIfFileEndingsUrlType(".mar", ".jnilib", ".dylib"));

        urlTypes.addAll(Arrays.asList(Vfs.DefaultUrlTypes.values()));

        Vfs.setDefaultURLTypes(urlTypes);

        List<ClassLoader> classLoadersList = new LinkedList<>();
        classLoadersList.add(ClasspathHelper.contextClassLoader());
        classLoadersList.add(ClasspathHelper.staticClassLoader());

        Reflections reflections = new Reflections(new ConfigurationBuilder()
                .setScanners(new SubTypesScanner(false /* don't exclude Object.class */), new ResourcesScanner())
                .setUrls(ClasspathHelper.forClassLoader(classLoadersList.toArray(new ClassLoader[0])))
                .filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix("lm.model"))));

        Set<Class<?>> allClasses
                = reflections.getSubTypesOf(Object.class);

        Utils.safeStream(allClasses).forEach(clazz -> {
            classMap.put(clazz.getSimpleName(), new ClassDO(clazz.getSimpleName(), clazz));
        });
        return classMap;

    }

    private void createDocumentation(String outputFile, Map<HeaderDO, List<MenuDO>> menuByHeader, Map<String, ComponentDO> allComponents, Map<String, StateDO> allStates) {
        try {

            // add document header
            // StringBuilder headerBuilder = generateHeader();
            // generate the associated DO documentation
            StringBuilder builder = new StringBuilder(generateBackOfficeDocumentation(menuByHeader, allComponents, allStates));

            // final string builder - so we can rejig the final contents displayed
            StringBuilder finalBuilder = new StringBuilder();

            // I want system components before events
            // finalBuilder.append(headerBuilder).append(builder);
            //logger.info(finalBuilder.toString());
            // FileUtils.writeStringToFile(new File(outputFile), finalBuilder.toString());
            //logger.info(builder.toString());
            // FileUtils.writeStringToFile(new File(outputFile), builder.toString());
            createDocument("Back Office", "backoffice", "backoffice", builder, DocumentationType.HTML);

//            Asciidoctor asciidoctor = createDoctor();
//            DocumentationType documentationType = DocumentationType.HTML;
//            Map<String, Object> options = createOptions(theOutputDir, "Back Office", new File("src/main/resources/webapp/backoffice"), documentationType);
//
//            switch (documentationType) {
//                case PDF:
//                    options.put("to_file", "backoffice.pdf");
//                    break;
//                case HTML:
//                case HTML_DECK:
//                    options.put("to_file", "backoffice.html");
//                    break;
//                default:
//                    throw new AssertionError(documentationType.name());
//            }
//
//            asciidoctor.convert(builder.toString(), options);
        } catch (Exception e) {
            logger.error("There was an error", e);
        }

    }

    private StringBuilder generateHeader() {
        StringBuilder builder = new StringBuilder();

        builder.append("image::../doc/logo.png[]").append(NL);
        builder.append("= Lottomart Back Office").append(NL);

        builder.append("Back Office User Manual").append(NL);

        builder.append(NL).append(".Current Status").append(NL);
        builder.append("****").append(NL);
        builder.append("This document is autogenerated.").append(NL);
        builder.append("****").append(NL);

        return builder;

    }

    private class HeaderDO {

        public String name;
        public Integer order;

        public HeaderDO(String name, int order) {
            this.name = name;
            this.order = order;
        }

    }

    private class MenuDO {

        public String state;
        public String name;

        public MenuDO(String state, String name) {
            this.state = state;
            this.name = name;
        }

    }

    private class ComponentDO {

        public String name;
        public String file;
        public String module;

        public ComponentDO(String module, String name, String file) {
            this.name = name;
            this.file = file;
            this.module = module;
        }

    }

    private class StateDO {

        public String module;
        public String state;
        public String component;
        public String url;

        public StateDO(String module, String state, String component, String url) {
            this.state = state;
            this.module = module;
            this.component = component;
            this.url = url;
        }

    }

    private Map<String, StateDO> readStates(String moduleName, String moduleFile) throws FileNotFoundException, IOException {
        BufferedReader in = new BufferedReader(new FileReader(moduleFile));
        String str;

        Map<String, StateDO> stateMap = new HashMap<>();

        while ((str = in.readLine()) != null) {
            if (str.startsWith("export const ") && str.contains("{name:")) {
// export const loginState = {name: 'lottobot.login', url: '/login', component: LoginPlayerComponent};

                String state = null;
                String url = null;
                String component = null;
                int start = str.indexOf("{name:");
                if (start > 0) {
                    String sub = str.substring(start + 8);
                    int end = sub.indexOf("'");
                    state = sub.substring(0, end).trim();
                }
                start = str.indexOf("url:");
                if (start > 0) {
                    String sub = str.substring(start + 5);
                    int end = sub.indexOf("'");
                    url = sub.substring(0, end).trim();
                }
                start = str.indexOf("component:");
                if (start > 0) {
                    String sub = str.substring(start + 11);
                    int end = sub.indexOf("}");
                    component = sub.substring(0, end).replace(",", "").trim();
                }
                if (state != null) {
                    stateMap.put(state, new StateDO(moduleName, state, component, url));
                }
            }
        }

        return stateMap;
    }

    private Map<String, ComponentDO> readModuleImports(String moduleName, String moduleFile) throws FileNotFoundException, IOException {
        BufferedReader in = new BufferedReader(new FileReader(moduleFile));
        String str;

        Map<String, ComponentDO> componentMap = new HashMap<>();

        while ((str = in.readLine()) != null) {
            if (str.startsWith("import {") && !str.contains("@")) {
                int endIndex = str.indexOf("}");
                String componentName = str.substring(8, endIndex);

                int fromIndex = str.indexOf("from '");
                int toIndex = str.substring(fromIndex + 6).indexOf("'");

                String fileName = str.substring(fromIndex + 6, fromIndex + 6 + toIndex);

                // only want components - not services
                if (componentName.contains("Component")) {
                    // logger.info("componentName: {} {}", componentName, fileName);
                    componentMap.put(componentName, new ComponentDO(moduleName, componentName, fileName));
                }
            }
            // list.add(str);
        }

        return componentMap;
    }

    private Map<HeaderDO, List<MenuDO>> readLeftMenu() throws IOException {

        Document document = Jsoup.parse(new File("../../lotto-backoffice-ui/src/app/components/main-layout/left-menu/left-menu.component.html"), "UTF-8");

        Element firstChild = document.body().child(0);

        Element ul = firstChild.child(0).child(0).child(0);

        Elements liElements = ul.select("li");
        Iterator<Element> iterator = liElements.iterator();
        int count = 0;

        Map<HeaderDO, List<MenuDO>> menuMap = new HashMap<>();

        List<MenuDO> menus = new ArrayList<>();
        int order = 0;
        while (iterator.hasNext()) {
            Element liElement = iterator.next();
            Elements aElements = liElement.select("a");

            if (!aElements.isEmpty()) {

                Elements spanElements = liElement.select("span");
                if (!spanElements.isEmpty()) {
                    // header
                    // logger.info("header: {}", spanElements.get(0).ownText());
                    menus = new ArrayList<>();
                    menuMap.put(new HeaderDO(spanElements.get(0).ownText(), order), menus);
                    order++;
                } else {
                    // menu
                    for (Element aElement : aElements) {
                        // logger.info("menu: {}", aElement.ownText());
                        String state = aElement.attr("uiSref");
                        menus.add(new MenuDO(state, aElement.ownText()));
                    }
                }

            }

//            Element liElement = iterator.next();
//            Elements children = liElement.children();
//            for (Element element : children) {
//                logger.info("element: {}", element.toString());
//
//            }
            count++;
        }

        // prettyPrint(menuMap);
        return menuMap;

    }

    public static void main(String... args) throws DocException, IOException, InterruptedException {

        try {
            Options options = new Options();
            options.addOption("output_dir", true, "Destination Directory");

            String outputDir;
            try {
                CommandLineParser parser = new DefaultParser();
                CommandLine commandLine = parser.parse(options, args);
                outputDir = commandLine.getOptionValue("output_dir");
                if (outputDir == null || outputDir.isEmpty()) {
                    logger.error("No root project directory specified");
                    throw new DocException("No root project directory specified");
                }

                logger.info("Output directory: {}", outputDir);
            } catch (ParseException ex) {
                throw new DocException("Error parsing options");
            }

            BackOfficeBuilder builder = new BackOfficeBuilder(outputDir);
            builder.createDocumentation();

            logger.info("Finished");
        } catch (Exception ex) {
            logger.error("Error", ex);
        } finally {
            Thread.sleep(2000);
        }
    }

}
