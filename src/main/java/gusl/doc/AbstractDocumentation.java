package gusl.doc;

import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.doc.generate.builders.ChangeAttributeValuePreprocessor;
import gusl.doc.generate.builders.DocumentationType;
import gusl.doc.generate.extensions.*;
import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Attributes;
import org.asciidoctor.Placement;
import org.asciidoctor.SafeMode;
import org.asciidoctor.extension.JavaExtensionRegistry;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import static org.asciidoctor.Asciidoctor.Factory.create;
import static org.asciidoctor.OptionsBuilder.options;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author grant
 */
public abstract class AbstractDocumentation {

    public static final GUSLLogger logger = GUSLLogManager.getLogger(AbstractDocumentation.class);

    private final String theOutputFile;
    private final Generator theGenerator;

    public AbstractDocumentation(String outputFile, Generator generator) {
        theOutputFile = outputFile;
        theGenerator = generator;
    }

    public void generate() {
        try {
            logger.info("Generating documentation");

            cleanFile(theOutputFile);

            theGenerator.generate(theOutputFile);

            File generatedFile = new File(theOutputFile);

            logger.info("generated documentation to : {} ", theOutputFile);

            assertThat("generated file exists", generatedFile.exists());

            documentFile("rest", "Lottomart REST API", theOutputFile, "./src/main/resources/webapp/doc/", DocumentationType.PDF);
            documentFile("rest", "Lottomart REST API", theOutputFile, "./src/main/resources/webapp/doc/", DocumentationType.HTML);

        } catch (Exception ex) {
            logger.error("Failed to generate documentation", ex);
        }
    }

    protected void documentFile(String outputFileNamePrefix, String title, String sourceFileName, String outputDir, DocumentationType documentationType) {
        File destFolder = new File(outputDir + File.separator + outputFileNamePrefix);

        // note: parent adoc must match the name of the folder e.g. download equals download/download.adoc
        convertFile(sourceFileName, title, outputDir, destFolder, documentationType);
    }

    private void convertFile(String srcFile, String title, String outputDir, File destFile, DocumentationType documentationType) {
        Asciidoctor asciidoctor = createDoctor();

        Map<String, Object> options = createOptions(outputDir, title, destFile, documentationType);

        File aDoc = new File(srcFile);
        if (aDoc.exists()) {
            logger.info("-- converting: {}", aDoc.getName());
            asciidoctor.convertFile(aDoc, options);
        } else {
            logger.error("file does not exist: {}", aDoc.getAbsoluteFile());
        }
    }

    void cleanFile(String outputFile) {

        File file = new File(outputFile);
        if (file.exists()) {
            file.delete();
        }
    }

    private Map<String, Object> createOptions(String outputDir, String title, File destFile, DocumentationType documentationType) {
        String rootPrefix = calcRootPrefix(outputDir, destFile);

        Attributes attributes = new Attributes();
        if (documentationType != DocumentationType.HTML_DECK) {
            attributes.setTableOfContents(true);
            attributes.setTableOfContents2(Placement.LEFT);
            attributes.setLinkCss(true);
        }
        attributes.setTitle(title);

        Map<String, Object> attributeMap = attributes.map();
        if (documentationType == DocumentationType.HTML_DECK) {
            attributeMap.put("deckjsdir", rootPrefix + "js/deck.js");
        }
        attributeMap.put("icons", "font");
        attributeMap.put("stylesdir", rootPrefix + "css");
        attributeMap.put("stylesheet", "lottomart-asciidoctor.css");
        attributeMap.put("source-highlighter", "coderay");
        attributeMap.put("toc-title", "Table of Contents");
        attributeMap.put("toclevels", "2");
        attributeMap.put(":docinfo:", "shared");

        Map<String, Object> options = options()
                .safe(SafeMode.UNSAFE)
                .toDir(destFile)
                .mkDirs(true)
                .attributes(attributes)
                .attributes(attributeMap)
                .option("backend", documentationType.getType())
                .asMap();

        if (documentationType == DocumentationType.HTML_DECK) {
            List<String> templateDirs = new ArrayList<>();
            templateDirs.add("./doc/templates/haml");
            options.put("template_dirs", templateDirs);
        }

        return options;
    }

    private Asciidoctor createDoctor() {
        Asciidoctor asciidoctor = create();

        JavaExtensionRegistry extensionRegistry = asciidoctor.javaExtensionRegistry();

        extensionRegistry.preprocessor(ChangeAttributeValuePreprocessor.class);

        extensionRegistry.block("tree", TreeMacro.class);
        extensionRegistry.block("dir_listing", NewDirListing.class);
        extensionRegistry.block("yuml", YumlMacro.class);
        extensionRegistry.block("field_doc", FieldsExtension.class);
        extensionRegistry.block("event_doc", EventMacro.class);

        return asciidoctor;
    }

    private String calcRootPrefix(String outputDir, File destFile) {

        File outDir = new File(outputDir);
        String dest = destFile.getAbsolutePath().replace(outDir.getAbsolutePath(), "");

        StringTokenizer stOR = new StringTokenizer(dest, "/");

        StringBuilder builder = new StringBuilder();
        for (int x = 0; x < stOR.countTokens(); x++) {
            builder.append("../");
        }
        return builder.toString();
    }

}
