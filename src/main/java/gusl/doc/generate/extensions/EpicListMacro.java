/* Copyright lottomart */
package gusl.doc.generate.extensions;

/**
 *
 * @author grant
 */
public class EpicListMacro  { // extends AbstractMacro {

    /**
     * **** THIS USED DEPRECATED LIBS, not sure we use this functionality anymore - so commenting out rather than fixing
     */

//    public static final GUSLLogger logger = GUSLLogManager.getLogger(this.getClass());
//
//    private static PropertiesClient propertiesClient;
//    private static JiraOAuthClient jiraOAuthClient;
//    private static boolean configured = false;
//
//    public EpicListMacro(String name, Map<String, Object> config) {
//        super(name, config);
//    }
//
//    @Override
//    public Object process(StructuralNode parent, Reader reader, Map<String, Object> attributes) {
//        StringBuilder builder = new StringBuilder();
//
//        String project = (String) attributes.get("project");
//        if (project == null) {
//            logger.error("No project passed - ignoring");
//            return null;
//        }
//        String release = (String) attributes.get("release");
//
//        try {
//
//            for (int y = 0; y < 5; y++) {
//                int startAt = y * 50 + 1;
//                if (startAt < 50) {
//                    startAt = 0;
//                }
//                JSONObject epicList = getEpicList(project, startAt);
//                // logger.info("epicList : {}", epicList);
//
//                if (epicList.isNull("issues")) {
//                    break;
//                }
//                JSONArray issues = epicList.getJSONArray("issues");
//
//                Map<String, Integer> mapEpics = new HashMap<>();
//
//                // sort by epic name
//                for (int x = 0; x < issues.length(); x++) {
//                    JSONObject issue = issues.getJSONObject(x);
//                    JSONObject fields = issue.getJSONObject("fields");
//
//                    String epicName = (String) fields.get("summary");
//
//                    logger.info("epic name: {}, key: {} valid: {}", epicName, (String) issue.get("key"), epicName.contains(release));
//
//                    if (release == null || epicName.contains(release)) {
//                        mapEpics.put(epicName, x);
//                    }
//
//                }
//
//                Utils.safeStream(mapEpics.keySet()).sorted().forEach(name -> {
//                    Integer x = mapEpics.get(name);
//
//                    //for (int x = 0; x < issues.length(); x++) {
//                    JSONObject issue = issues.getJSONObject(x);
//                    JSONObject fields = issue.getJSONObject("fields");
//
//                    String epicName = (String) fields.get("summary");
//
//                    if (release == null || epicName.contains(release)) {
//                        //  builder.append("===").append((String) issue.get("key")).append(" ").append(epicName).append(NL).append(NL);
//                        StringBuilder result = processEpic((String) issue.get("key"));
//                        if (result != null) {
//                            builder.append(result);
//                        }
//                        builder.append(NL).append(NL);
//                    }
//
//                });
//            }
//
//            List<String> content = new ArrayList<>();
//
//            content.addAll(Arrays.asList(builder.toString().split("\n")));
//
//            parseContent(parent, content);
//        } catch (Exception ex) {
//            logger.error("Error: {}", ex.getMessage(), ex);
//        }
//
//        return null;
//
//    }
//
//    public StringBuilder processEpic(String jiraTicket) {
//
//        try {
//            JSONObject ticket = getTicket(jiraTicket);
//            JSONObject epic = getEpic(jiraTicket);
//
//            String epicSummary;
//            JSONObject ticketFields = ticket.getJSONObject("fields");
//            try {
//                epicSummary = (String) ticketFields.get("summary");
//            } catch (Exception e) {
//                epicSummary = "something wrong";
//            }
//
//            JSONObject epicStatus = ticketFields.getJSONObject("status");
//
//            JSONArray issues = epic.getJSONArray("issues");
//            StringBuilder builder = new StringBuilder();
//
//            String statusCss = (String) epicStatus.get("name");
//            statusCss = "story-" + statusCss.toLowerCase().replace(" ", "-");
//
//            // start - source block
//            builder.append("[source,python,,role=\"").append(statusCss).append("\"]").append(NL).append("====").append(NL);
//            builder.append(BOLD).append("EPIC").append(BOLD).append(NL).append(NL);
//
//            builder.append(BOLD).append("").append(jiraTicket).append(" ").append(epicSummary).append(BOLD).append(NL).append(NL);
//            builder.append(BOLD).append("Status: ").append((String) epicStatus.get("name")).append(BOLD).append(NL).append(NL);
//            builder.append("[options=\"header,footer\",cols=\"10%,65%,10%,15%\"]\n|===").append(NL);
//            builder
//                    .append(HDR_COL).append("Ticket")
//                    .append(HDR_COL).append("Description")
//                    .append(HDR_COL).append("Status")
//                    .append(HDR_COL).append("Remaining")
//                    .append(NL);
//
//            int epicTime = 0;
//            if (!ticketFields.isNull("timetracking")) {
//
//                JSONObject epictimeTracking = ticketFields.getJSONObject("timetracking");
//
//                if (!epictimeTracking.isNull("remainingEstimateSeconds")) {
//
//                    epicTime = (Integer) epictimeTracking.get("remainingEstimateSeconds");
//                }
//            }
//            int epicTimeLeft = epicTime;
//
//            for (int x = 0; x < issues.length(); x++) {
//
//                JSONObject issue = issues.getJSONObject(x);
//                JSONObject fields = issue.getJSONObject("fields");
//
//                JSONObject status = fields.getJSONObject("status");
//
//                int storyTime = 0;
//                if (!fields.isNull("timetracking")) {
//
//                    JSONObject storyTimeTracking = fields.getJSONObject("timetracking");
//
//                    if (!storyTimeTracking.isNull("remainingEstimateSeconds")) {
//
//                        storyTime = (Integer) storyTimeTracking.get("remainingEstimateSeconds");
//                    }
//                }
//                epicTimeLeft += storyTime;
//                int totalTimeLeft = storyTime;
//
//                if (!fields.isNull("subtasks")) {
//                    JSONArray subIssues = fields.getJSONArray("subtasks");
//
//                    for (int z = 0; z < subIssues.length(); z++) {
//                        JSONObject subIssue = subIssues.getJSONObject(z);
//
//                        JSONObject subTicket = getTicket((String) subIssue.get("key"));
//                        // logger.info("subTicket: {}", subTicket);
//
//                        JSONObject subTicketFields = subTicket.getJSONObject("fields");
//                        JSONObject timeTracking = subTicketFields.getJSONObject("timetracking");
//
//                        int timeLeft = (Integer) timeTracking.get("remainingEstimateSeconds");
//                        totalTimeLeft += timeLeft;
//                        epicTimeLeft += timeLeft;
//                    }
//
//                }
//
////                logger.info("issue: {}", issue);
////                logger.info("fields: {}", fields);
////                logger.info("status: {}", status);
//                String assigneeName;
//
//                if (fields.isNull("assignee")) {
//                    assigneeName = "unassigned";
//                } else {
//                    JSONObject assignee = fields.getJSONObject("assignee");
//                    assigneeName = (String) assignee.get("displayName");
//                }
//
//                String summary = fields.isNull("summary") ? "" : (String) fields.get("summary");
//                String description = fields.isNull("description") ? "" : (String) fields.get("description");
//                summary = "*" + summary + ".* " + description;
//
//                builder
//                        .append(HDR_COL).append((String) issue.get("key"))
//                        //                        .append(HDR_COL).append(fields.isNull("description") ? "" :(String)  fields.get("description"))
//                        .append(HDR_COL).append(summary)
//                        .append(HDR_COL).append(status.isNull("name") ? "" : (String) status.get("name"))
//                        //.append(HDR_COL).append(assigneeName)
//                        .append(HDR_COL).append(formatTime(totalTimeLeft))
//                        .append(NL);
//            }
//            builder
//                    .append(HDR_COL).append("")
//                    .append(HDR_COL).append("")
//                    .append(HDR_COL).append("Total")
//                    .append(HDR_COL).append(formatTotalTime(epicTimeLeft))
//                    .append(NL);
//
//            builder.append(CLOSE_TABLE).append(NL);
//
//            // end - source block
//            builder.append("====").append(NL).append(NL).append(NL);
//
//            return builder;
//        } catch (Exception ex) {
//            logger.error("Error: {}", ex.getMessage(), ex);
//        }
//
//        return null;
//    }
//
//    private String formatTotalTime(int time) {
//        if (time == 0) {
//            return String.format("%6.2f days", 0.0);
//        } else {
//            return String.format("%6.2f days", (time / (3600.0 * 8.0)));
//        }
//    }
//
//    private String formatTime(int time) {
//        if (time == 0) {
//            return String.format("%6.2f hrs", 0.0);
//        } else {
//            return String.format("%6.2f hrs", (time / 3600.0));
//        }
//    }
//
//    private JSONObject getEpicList(String project, int startAt) throws Exception {
//        if (!configured) {
//            propertiesClient = new PropertiesClient();
//            jiraOAuthClient = new JiraOAuthClient(propertiesClient);
//        }
//
//        Map<String, String> properties = propertiesClient.getPropertiesOrDefaults();
//        String tmpToken = properties.get(ACCESS_TOKEN);
//        String secret = properties.get(SECRET);
//
//        String jiraHome = properties.get(JIRA_HOME);
//        String url = jiraHome + "/rest/api/2/search/?startAt=" + startAt + "&jql=project%3D%22" + project + "%22%20AND%20issuetype%3D%22Epic%22";
//
//        try {
//            OAuthParameters parameters = jiraOAuthClient.getParameters(tmpToken, secret, properties.get(CONSUMER_KEY), properties.get(PRIVATE_KEY));
//            HttpResponse response = getResponseFromUrl(parameters, new GenericUrl(url));
//            return parseResponse(response);
//        } catch (Exception e) {
//            logger.error("error ticket [{}] {}", project, e.getMessage());
//            throw e;
//        }
//    }
//
//    private JSONObject getTicket(String ticket) throws Exception {
//        if (!configured) {
//            propertiesClient = new PropertiesClient();
//            jiraOAuthClient = new JiraOAuthClient(propertiesClient);
//        }
//
//        Map<String, String> properties = propertiesClient.getPropertiesOrDefaults();
//        String tmpToken = properties.get(ACCESS_TOKEN);
//        String secret = properties.get(SECRET);
//
//        String jiraHome = properties.get(JIRA_HOME);
//        String url = jiraHome + "/rest/api/2/issue/" + ticket;
//
//        try {
//            OAuthParameters parameters = jiraOAuthClient.getParameters(tmpToken, secret, properties.get(CONSUMER_KEY), properties.get(PRIVATE_KEY));
//            HttpResponse response = getResponseFromUrl(parameters, new GenericUrl(url));
//            return parseResponse(response);
//        } catch (Exception e) {
//            logger.error("error ticket [{}] {}", ticket, e.getMessage());
//            throw e;
//        }
//    }
//
//    private JSONObject getEpic(String ticket) throws Exception {
//        if (!configured) {
//            propertiesClient = new PropertiesClient();
//            jiraOAuthClient = new JiraOAuthClient(propertiesClient);
//        }
//
//        Map<String, String> properties = propertiesClient.getPropertiesOrDefaults();
//        String tmpToken = properties.get(ACCESS_TOKEN);
//        String secret = properties.get(SECRET);
//
//        String jiraHome = properties.get(JIRA_HOME);
//        String url = jiraHome + "/rest/agile/1.0/epic/" + ticket + "/issue";
//
//        try {
//            OAuthParameters parameters = jiraOAuthClient.getParameters(tmpToken, secret, properties.get(CONSUMER_KEY), properties.get(PRIVATE_KEY));
//            HttpResponse response = getResponseFromUrl(parameters, new GenericUrl(url));
//            return parseResponse(response);
//        } catch (Exception e) {
//            logger.error("error", e);
//            throw e;
//        }
//    }
//
//    private JSONObject parseResponse(HttpResponse response) throws Exception {
//        Scanner s = new Scanner(response.getContent()).useDelimiter("\\A");
//        String result = s.hasNext() ? s.next() : "";
//
//        try {
//            JSONObject jsonObj = new JSONObject(result);
//            return jsonObj;
//        } catch (Exception e) {
//            logger.error("error", e);
//            throw e;
//        }
//    }
//
//    private static HttpResponse getResponseFromUrl(OAuthParameters parameters, GenericUrl jiraUrl) throws IOException {
//        HttpRequestFactory requestFactory = new NetHttpTransport().createRequestFactory(parameters);
//        HttpRequest request = requestFactory.buildGetRequest(jiraUrl);
//        return request.execute();
//    }

}
