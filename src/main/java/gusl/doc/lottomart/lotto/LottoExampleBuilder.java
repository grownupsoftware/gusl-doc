/* Copyright lottomart */
package gusl.doc.lottomart.lotto;

import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;

/**
 *
 * @author grant
 */
public class LottoExampleBuilder {

    public static final GUSLLogger logger = GUSLLogManager.getLogger(LottoExampleBuilder.class);

    /*
    Map<Long, CurrencyDO> mapCurrencyById = new HashMap<>();
    Map<String, CurrencyDO> mapCurrencyByCode = new HashMap<>();

    Map<Long, CountryDO> mapCountryById = new HashMap<>();
    Map<String, CountryDO> mapCountryByCode = new HashMap<>();
    Map<Long, LottoConfigDO> mapLottoConfigById = new HashMap<>();

    Map<Long, LottoDO> mapLottoById = new HashMap<>();

    Map<String, StringBuilder> mapExamples = new HashMap<>();

    protected final ObjectMapper theMapper = ObjectMapperFactory.getDefaultObjectMapper();

    private long countryCounter = 1;
    private long currencyCounter = 1;
    private long lottoCounter = 1;
    private long lottoConfigCounter = 1;
    private long drawOptionCounter = 1;
    private long priceTableCounter = 1;

    public void buildExamples() {
        createCurrencies();
        createCountries();
        createLottos();

        createDTOs();
        addExamplesToMap();

//        Utils.safeStream(mapExamples.entrySet()).forEach(entry -> {
//            logger.info("Example -> name: {} content: {}", entry.getKey(), entry.getValue());
//        });
    }

    private void addExamplesToMap() {

        // Lotto - All
        StringBuilder lottoBuilder = new StringBuilder();
        Utils.safeStream(mapLottoById.values()).forEach(lotto -> {
            //logger.info("lotto: {}", lotto);
            lottoBuilder.append(documentExample(lotto.getName(), lotto));
        });
        mapExamples.put("lotto", lottoBuilder);

        // Country - One
        StringBuilder countryBuilder = new StringBuilder();
        CountryDO countryDo = mapCountryByCode.get("GB");
        countryBuilder.append(documentExample(countryDo.getName(), countryDo));
        mapExamples.put("country", countryBuilder);

        // Currency - One
        StringBuilder currencyBuilder = new StringBuilder();
        CurrencyDO currencyDo = mapCurrencyByCode.get("EUR");
        currencyBuilder.append(documentExample(currencyDo.getName(), currencyDo));
        mapExamples.put("currency", currencyBuilder);

    }

    public StringBuilder getExample(String exampleName) {
        return mapExamples.get(exampleName);
    }

    private List<DayOfWeek> getDays(String[] days) {
        return Utils.safeStream(days).map(day -> {
            return DayOfWeek.valueOf(day);
        }).collect(Collectors.toList());
    }

    private void createLottos() {

        InsurerDO defaultInsurerDo = createInsurer();

        // Euro Millions
        List<PriceTableDO> prices = new ArrayList<>();
        prices.add(createPriceTable("GB", 200, true));
        prices.add(createPriceTable("DE", 250, false));

        List<DrawOptionDO> drawOptions = new ArrayList<>();
        drawOptions.add(createDrawOption("Main", DrawOptionType.RANGE, true, 1, 50, "EUR", 250, false, 5, null, null, prices, "#CFCFCE", "#fff", "#CFCFCE", "#6987ca", "#6987ca", "#fff", "#CFCFCE", "#CFCFCE", DrawShapeType.CIRCLE));
        drawOptions.add(createDrawOption("Mega Ball", DrawOptionType.RANGE, false, 1, 12, null, null, false, 2, null, null, null, "#CFCFCE", "#fff", "#CFCFCE", "#fde07e", "#fde07e", "#fff", "#CFCFCE", "#CFCFCE", DrawShapeType.STAR));

        prices = new ArrayList<>();
        prices.add(createPriceTable("GB", 150, true));
        prices.add(createPriceTable("DE", 200, false));

        drawOptions.add(createDrawOption("Megaplier", DrawOptionType.MULTIPLIER, false, null, null, null, 200, true, null, 2, 5, prices, "#CFCFCE", "#fff", "#CFCFCE", "#6987ca", "#6987ca", "#fff", "#CFCFCE", "#CFCFCE", DrawShapeType.STAR));

        LottoConfigDO lottoConfigDo = createLottoConfig(getDays(new String[]{"FRIDAY"}), defaultInsurerDo, drawOptions, "#6987ca", "#4059aa", "#fff", "euromillions/logo.png", "euromillions/flag.png");
        createLotto("EU", "euromillions", "Euro Millions", lottoConfigDo);

        // Powerball USA
        prices = new ArrayList<>();
        prices.add(createPriceTable("GB", 400, true));
        prices.add(createPriceTable("DE", 500, false));

        drawOptions = new ArrayList<>();
        drawOptions.add(createDrawOption("Main", DrawOptionType.RANGE, true, 1, 69, "USD", 200, false, 5, null, null, prices, "#CFCFCE", "#fff", "#CFCFCE", "#8ba4db", "#8ba4db", "#fff", "#CFCFCE", "#CFCFCE", DrawShapeType.CIRCLE));
        drawOptions.add(createDrawOption("Powerball", DrawOptionType.RANGE, false, 1, 26, null, null, false, 1, null, null, null, "#CFCFCE", "#fff", "#CFCFCE", "#fde07e", "#fde07e", "#fff", "#CFCFCE", "#CFCFCE", DrawShapeType.STAR));

        prices = new ArrayList<>();
        prices.add(createPriceTable("GB", 500, true));
        prices.add(createPriceTable("DE", 800, false));

        drawOptions.add(createDrawOption("Power Play", DrawOptionType.ON_OFF, false, null, null, null, 300, true, null, 2, 10, prices, "#CFCFCE", "#fff", "#CFCFCE", "#8ba4db", "#8ba4db", "#fff", "#CFCFCE", "#CFCFCE", DrawShapeType.STAR));

        lottoConfigDo = createLottoConfig(getDays(new String[]{"WEDNESDAY", "SATURDAY"}), defaultInsurerDo, drawOptions, "#8ba4db", "#677dc6", "#fff", "megamillions/logo.png", "megamillions/flag.png");
        createLotto("US", "powerballusa", "US PowerBall", lottoConfigDo);

        // MegaMillions USA        
        drawOptions = new ArrayList<>();
        prices = new ArrayList<>();
        prices.add(createPriceTable("GB", 250, true));
        prices.add(createPriceTable("DE", 500, false));
        drawOptions.add(createDrawOption("Main", DrawOptionType.RANGE, true, 1, 75, "USD", 100, false, 5, null, null, prices, "#CFCFCE", "#fff", "#CFCFCE", "#6eb0e6", "#6eb0e6", "#fff", "#CFCFCE", "#CFCFCE", DrawShapeType.CIRCLE));
        drawOptions.add(createDrawOption("Mega Ball", DrawOptionType.RANGE, false, 1, 15, null, null, false, 1, null, null, prices, "#CFCFCE", "#fff", "#CFCFCE", "#fde07e", "#fde07e", "#fff", "#CFCFCE", "#CFCFCE", DrawShapeType.STAR));
        drawOptions.add(createDrawOption("Megaplier", DrawOptionType.MULTIPLIER, false, null, null, null, 200, true, null, 2, 5, prices, "#CFCFCE", "#fff", "#CFCFCE", "#6885C3", "#6885C3", "#fff", "#CFCFCE", "#CFCFCE", DrawShapeType.STAR));

        lottoConfigDo = createLottoConfig(getDays(new String[]{"TUESDAY", "FRIDAY"}), defaultInsurerDo, drawOptions, "#6eb0e6", "#4a89dc", "#fff", "megamillions/logo.png", "megamillions/flag.png");
        createLotto("US", "megamillionsusa", "Mega Millions", lottoConfigDo);

        // Super Enalotto
        drawOptions = new ArrayList<>();
        prices = new ArrayList<>();
        prices.add(createPriceTable("GB", 300, true));
        prices.add(createPriceTable("ES", 600, false));
        drawOptions.add(createDrawOption("Main", DrawOptionType.RANGE, true, 1, 90, "EUR", 100, false, 6, null, null, prices, "#CFCFCE", "#fff", "#CFCFCE", "#e16b77", "#e16b77", "#fff", "#CFCFCE", "#CFCFCE", DrawShapeType.CIRCLE));
        drawOptions.add(createDrawOption("Super Star", DrawOptionType.RANGE, false, 1, 90, "EUR", 150, true, 1, null, null, prices, "#CFCFCE", "#fff", "#CFCFCE", "#fde07e", "#fde07e", "#fff", "#CFCFCE", "#CFCFCE", DrawShapeType.STAR));
        drawOptions.add(createDrawOption("Instant", DrawOptionType.SE_INSTANT, false, 1, 90, null, null, true, 1, null, null, prices, "#CFCFCE", "#fff", "#CFCFCE", "#6885C3", "#6885C3", "#fff", "#CFCFCE", "#CFCFCE", DrawShapeType.STAR));

        lottoConfigDo = createLottoConfig(getDays(new String[]{"FRIDAY"}), defaultInsurerDo, drawOptions, "#e16b77", "#d34c59", "#fff", "superenalloto/logo.png", "superenalloto/flag.png");
        createLotto("ES", "superenalotto", "Super Enalotto", lottoConfigDo);

        // German Lotto(6aus49)         
        drawOptions = new ArrayList<>();
        prices = new ArrayList<>();
        prices.add(createPriceTable("GB", 500, true));
        prices.add(createPriceTable("DE", 800, false));
        drawOptions.add(createDrawOption("Main", DrawOptionType.RANGE, true, 1, 49, "EUR", 200, false, 6, null, null, prices, "#CFCFCE", "#fff", "#CFCFCE", "#585c60", "#585c60", "#fff", "#CFCFCE", "#CFCFCE", DrawShapeType.CIRCLE));
        drawOptions.add(createDrawOption("Super", DrawOptionType.RANGE, false, 0, 9, null, null, false, 1, null, null, prices, "#CFCFCE", "#fff", "#CFCFCE", "#fde07e", "#fde07e", "#fff", "#CFCFCE", "#CFCFCE", DrawShapeType.STAR));

        lottoConfigDo = createLottoConfig(getDays(new String[]{"WEDNESDAY", "SATURDAY"}), defaultInsurerDo, drawOptions, "#585c60", "#434a54", "#fff", "lottoaus/logo.png", "lottoaus/flag.png");
        createLotto("DE", "6aus49", "Lotto 6aus49", lottoConfigDo);

        // megasens         
        drawOptions = new ArrayList<>();
        prices = new ArrayList<>();
        prices.add(createPriceTable("GB", 500, true));
        prices.add(createPriceTable("DE", 800, false));

        drawOptions.add(createDrawOption("Main", DrawOptionType.RANGE, true, 1, 49, "EUR", 200, false, 6, null, null, prices, "#CFCFCE", "#fff", "#CFCFCE", "#78bc80", "#78bc80", "#fde07e", "#CFCFCE", "#CFCFCE", DrawShapeType.CIRCLE));
        drawOptions.add(createDrawOption("Super", DrawOptionType.RANGE, false, 0, 9, null, null, false, 1, null, null, prices, "#CFCFCE", "#fff", "#CFCFCE", "#fde07e", "#fde07e", "#78bc80", "#CFCFCE", "#CFCFCE", DrawShapeType.STAR));

        lottoConfigDo = createLottoConfig(getDays(new String[]{"WEDNESDAY", "SATURDAY"}), defaultInsurerDo, drawOptions, "#78bc80", "#52c162", "#fff", "megasena/logo.png", "megasena/flag.png");
        createLotto("DE", "megasena", "Mega Sena", lottoConfigDo);

    }

    private PriceTableDO createPriceTable(String countryCode, Integer price, boolean systemDefault) {

        CountryDO countryDo = mapCountryByCode.get(countryCode);

        if (countryDo == null) {
            logger.error("******* Country for {} not found", countryCode);
            return new PriceTableDO();
        }

        CurrencyDO currencyDo = mapCurrencyById.get(countryDo.getCurrencyId());

        if (currencyDo == null) {
            logger.error("******* Currency for {} not found", countryDo.getCurrencyId());
            return new PriceTableDO();
        }

        PriceTableDO priceTableDo = new PriceTableDO();
        priceTableDo.setId(priceTableCounter);
        priceTableDo.setCountryId(countryDo.getId());
        priceTableDo.setDrawOptionId(null);// set later
        priceTableDo.setSystemDefault(systemDefault);

        MoneyDO moneyDo = new MoneyDO();
        moneyDo.setCurrencyId(currencyDo.getId());
        moneyDo.setValue(price.longValue());

        priceTableDo.setPrice(moneyDo);

        priceTableCounter++;
        return priceTableDo;

    }

    private DrawOptionDO createDrawOption(String name,
            DrawOptionType optionType,
            boolean primary,
            Integer minRange,
            Integer maxRange,
            String currencyCode,
            Integer price,
            boolean optional,
            Integer numSelections,
            Integer minMultiplier,
            Integer maxMultiplier,
            List<PriceTableDO> prices,
            String nonSelectedStrokeColor,
            String nonSelectedFillColor,
            String nonSelectedTextColor,
            String selectedStrokeColor,
            String selectedFillColor,
            String selectedTextColor,
            String selectionPlaceholderFillColor,
            String selectionPlaceholderStrokeColor,
            DrawShapeType drawShapeType
    ) {

        CurrencyDO currencyDo = null;
        if (currencyCode != null) {
            currencyDo = mapCurrencyByCode.get(currencyCode);

            if (currencyDo == null) {
                logger.error("******* Currency for {} not found", currencyCode);
            }
        }

        DrawOptionDO drawOptionDo = new DrawOptionDO();
        drawOptionDo.setId(drawOptionCounter);
        drawOptionDo.setLottoConfigId(null);// done later
        drawOptionDo.setName(name);

        drawOptionDo.setMinRange(minRange);
        drawOptionDo.setMaxRange(maxRange);

        drawOptionDo.setMinRangeMultiplier(minMultiplier);
        drawOptionDo.setMaxRangeMultiplier(maxMultiplier);

        drawOptionDo.setOptionType(optionType);
        drawOptionDo.setOptional(optional);

        drawOptionDo.setNonSelectedStrokeColor(nonSelectedStrokeColor);
        drawOptionDo.setNonSelectedFillColor(nonSelectedFillColor);
        drawOptionDo.setNonSelectedTextColor(nonSelectedTextColor);
        drawOptionDo.setSelectedStrokeColor(selectedStrokeColor);
        drawOptionDo.setSelectedFillColor(selectedFillColor);
        drawOptionDo.setSelectedTextColor(selectedTextColor);
        drawOptionDo.setSelectionPlaceholderFillColor(selectionPlaceholderFillColor);
        drawOptionDo.setSelectionPlaceholderStrokeColor(selectionPlaceholderStrokeColor);

        drawOptionDo.setShapeType(drawShapeType);

        if (currencyCode != null) {
            MoneyDO moneyDo = new MoneyDO();
            moneyDo.setCurrencyId(currencyDo.getId());
            moneyDo.setValue(price.longValue());

            drawOptionDo.setPrice(moneyDo);
        }
        drawOptionDo.setPrimaryDraw(primary);

        // drawOptionDo.setPrices(prices);
        drawOptionCounter++;
        return drawOptionDo;
    }

    private LottoConfigDO createLottoConfig(List<DayOfWeek> weekDays, InsurerDO insurerDo, List<DrawOptionDO> drawOptions,
            String backgroundGradient1, String backgroundGradient2, String fontColor, String logo, String flag) {

        Utils.safeStream(drawOptions).forEach(option -> {
            option.setLottoConfigId(lottoConfigCounter);
        });

        LottoConfigDO lottoConfigDo = new LottoConfigDO();
        lottoConfigDo.setId(lottoConfigCounter);
        // lottoConfigDo.setDefaultInsurer(insurerDo);
        lottoConfigDo.setDrawOptions(drawOptions);

        // lottoConfigDo.setWeekdays(weekDays);
        lottoConfigDo.setLottoId(null); // set later
        lottoConfigDo.setDefaultAutoBatchSize(Integer.SIZE);
        lottoConfigDo.setDefaultAutoBatchTime(60);
        lottoConfigDo.setDefaultRestrictedQuickPicksAllowed(true);
        lottoConfigDo.setDefaultRestrictedQuickPicksPoolSize(2000);
        lottoConfigDo.setDefaultUnderwritingType(UnderwritingType.AUTO_LINE);
        lottoConfigDo.setBackgroundGradient1(backgroundGradient1);
        lottoConfigDo.setBackgroundGradient2(backgroundGradient2);
        lottoConfigDo.setFontColor(fontColor);

        try {
//            lottoConfigDo.setLogo(convertToBase64(logo));
//            lottoConfigDo.setFlag(convertToBase64(flag));
            lottoConfigDo.setLogoUrl(logo);
//            lottoConfigDo.setFlagUrl(flag);
        } catch (Exception ex) {
            logger.error("Failed to encode image", ex);
        }

        mapLottoConfigById.put(lottoConfigCounter, lottoConfigDo);

        lottoConfigCounter++;

        return lottoConfigDo;
    }

    private String convertToBase64(String filename) throws Exception {
        InputStream resourceAsStream = IOUtils.getResourceAsStream("images/" + filename, getClass().getClassLoader());
        byte[] bytes = ByteStreams.toByteArray(resourceAsStream);
        String encodedString = Base64.getEncoder().encodeToString(bytes);
        return "data:image/" + filename.substring(filename.length() - 3) + ";base64," + encodedString;
    }

    private InsurerDO createInsurer() {
        InsurerDO insurerDo = new InsurerDO();
        insurerDo.setId(lottoCounter);
        insurerDo.setCode("abc");
        insurerDo.setName("ABC Corp");
        insurerDo.setStatus(InsurerStatus.ACTIVE);

        return insurerDo;
    }

    private LottoDO createLotto(String countryCode, String code, String name, LottoConfigDO lottoConfigDo) {

        CountryDO countryDo = mapCountryByCode.get(countryCode);

        if (countryDo == null) {
            logger.error("Cannot find country: {}", countryCode);
            return null;
        }

        LottoDO lottoDo = new LottoDO();
        lottoDo.setId(lottoCounter);
        lottoDo.setCode(code);
        lottoDo.setName(name);
        lottoDo.setCurrencyId(countryDo.getId());
        lottoDo.setStatus(LottoStatus.ACTIVE);
        // lottoDo.setConfig(lottoConfigDo);

        lottoConfigDo.setLottoId(lottoCounter);

        mapLottoById.put(lottoCounter, lottoDo);
        lottoCounter++;

        return lottoDo;

    }

    private void createCurrencies() {

        createCurrency("EUR", "Euro", 2, "&#8364;", false);
        createCurrency("GBP", "Pound Sterling", 2, "&#163;", false);
        createCurrency("SEK", "Swedish Krona", 2, "&#107;&#114;", false);
        createCurrency("USD", "US Dollar", 2, "&#36;", false);
        createCurrency("AUD", "Australian Dollar", 2, "&#36;", false);
        createCurrency("CAD", "Canadian Dollar", 2, "&#36;", false);

    }

    private void createCountries() {
//        createCountry("GBP", "UK", "United Kingdom", true, true, "en");
//        createCountry("Euro", "EU", "Europe", false, false, "");
//        createCountry("Euro", "ES", "Spain", false, false, "");
//        createCountry("USD", "US", "United States", true, false, "");
//        createCountry("AUD", "AUS", "Australia", true, false, "");
//        createCountry("Euro", "DE", "Germany", true, false, "");
//        createCountry("Euro", "AT", "Austria", true, false, "");
//        createCountry("Euro", "BE", "Belgium", true, false, "");
//        createCountry("Euro", "IE", "Ireland", true, false, "");
//        createCountry("Euro", "IT", "Italy", true, false, "");
//        createCountry("ZLT", "PL", "Poland", true, false, "");

        createCountry("EUROPE", "EU", "EUR", true, false, false, "EUR");
        createCountry("United Kingdom", "GB", "GBR", false, false, true, "GBP");
        createCountry("Ireland", "IE", "IRL", false, false, true, "EUR");
        createCountry("Sweden", "SE", "SWE", false, false, true, "SEK");
        createCountry("Australia", "AU", "AUS", false, false, true, "AUD");
        createCountry("Canada", "CA", "CAN", false, false, true, "CAD");
        createCountry("United States of America", "US", "USA", false, true, true, "USD");
        createCountry("Germany", "DE", "DEU", true, false, false, "EUR");
        createCountry("Spain", "ES", "ESP", true, false, false, "EUR");

    }

    private CountryDO createCountry(String name, String alpha2, String alpha3, boolean hideInRegistration, boolean blockInRegistration, boolean topCountry, String currencyCode) {

        CurrencyDO currencyDO = mapCurrencyByCode.get(currencyCode);

        CountryDO countryDo = new CountryDO();
        countryDo.setId(countryCounter);
        countryDo.setName(name);
        countryDo.setAlpha2(alpha2);
        countryDo.setAlpha3(alpha3);
        countryDo.setBlockInRegistration(blockInRegistration);
        countryDo.setHideInRegistration(hideInRegistration);
        countryDo.setTopCountry(topCountry);
        countryDo.setCurrencyId(currencyDO.getId());

        mapCountryById.put(countryCounter, countryDo);
        mapCountryByCode.put(alpha2, countryDo);

        countryCounter++;
        return countryDo;
    }

    private CurrencyDO createCurrency(String code, String name, Integer decimalPlaces, String symbol, boolean hideInRegistration) {
        CurrencyDO currencyDo = new CurrencyDO();
        currencyDo.setId(currencyCounter);
        currencyDo.setCode(code);
        currencyDo.setDecimalPlaces(decimalPlaces);
        currencyDo.setName(name);
        currencyDo.setSymbol(symbol);
        currencyDo.setHideInRegistration(hideInRegistration);

        mapCurrencyById.put(currencyCounter, currencyDo);
        mapCurrencyByCode.put(code, currencyDo);

        currencyCounter++;
        return currencyDo;

    }
//    private StringBuilder documentTheClass(Class doClass) {
//        StringBuilder builder = new StringBuilder();
//
//        if (!doClass.isInterface() && !Modifier.isAbstract(doClass.getModifiers())) {
//
//            // add an anchor
//            builder.append(createAnchor(doClass.getCanonicalName())).append(NL);
//            // add bookmark using class canonical name
//            builder.append(HEADER).append(doClass.getSimpleName()).append(NL);
//            builder.append("Class: ").append(doClass.getCanonicalName()).append(NL);
//            // add the classes documentation
//
//            DocClass doAnnotation = (DocClass) doClass.getAnnotation(DocClass.class);
//            if (doAnnotation != null) {
//                builder.append(NL).append(BOLD).append("Overview").append(BOLD).append(NL);
//                builder.append(doAnnotation.description()).append(NL).append(NL);
//            }
//
//            //builder.append(includeTags(doClass));
//            builder.append(NL).append(BOLD).append("Contents").append(BOLD).append(NL);
//
//            builder.append(documentClass(doClass, true));
//
//        }
//
//        return builder;
//
//    }

    public StringBuilder documentExample(String name, Object object) {
        StringBuilder builder = new StringBuilder();

        String json;
        try {
            json = theMapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
        } catch (JsonProcessingException ex) {
            logger.error("Failed to create JSON example of object. Class: {} Error: {}", object.getClass().getCanonicalName(), ex.getMessage(), ex);
            return builder;
        }
        builder.append(NL).append(BOLD).append("Example: ").append(name).append(BOLD).append(NL);

        builder.append("[source,json]").append(NL);
        builder.append("----").append(NL);
        json = json.replaceAll("null", "\"\"");

        builder.append(json);
        builder.append(NL).append("----").append(NL).append(NL);

        return builder;

    }

    public static void main(String... args) throws Exception {
        LottoExampleBuilder lottoExample = new LottoExampleBuilder();

        lottoExample.buildExamples();

        Thread.sleep(1000);

    }

    private void createDTOs() {

        mapExamples.put("RegistrationSettingsDTO", documentExample("Registration Options", getRegistrationSettingsDTO()));
        mapExamples.put("RegisterRequestDTO", documentExample("Registration Request", getPlayerRegisterRequestDTO()));
        mapExamples.put("AdminLoginRequestDTO", documentExample("Admin User Login Request", getAdminLoginRequestDTO()));
        mapExamples.put("AdminLoginResponseDTO", documentExample("Admin User Login Response", getAdminLoginResponseDTO()));
        mapExamples.put("AdminLogoutRequestDTO", documentExample("Admin User Logout Request", getAdminLogoutRequestDTO()));
        mapExamples.put("AdminLogoutResponseDTO", documentExample("Admin User Logout Response", getAdminLogoutResponseDTO()));

    }

    private Map<String, String> getAdminLogoutRequestDTO() {
        Map<String, String> map = new HashMap<>();
        map.put("session-token", "xxx");

        return map;

    }

    private AdminLogoutResponseDTO getAdminLogoutResponseDTO() {
        AdminLogoutResponseDTO adminLogoutResponseDTO = new AdminLogoutResponseDTO();
        adminLogoutResponseDTO.setAccountId(lottoCounter);
        adminLogoutResponseDTO.setSessionId(lottoCounter);
        return adminLogoutResponseDTO;

    }

    private AdminLoginRequestDTO getAdminLoginRequestDTO() {
        return new AdminLoginRequestDTO();
    }

    private AdminLoginResponseDTO getAdminLoginResponseDTO() {
        AdminLoginResponseDTO adminLoginResponseDTO = new AdminLoginResponseDTO();

        String sessionToken = "xxx";

        AdminSessionDTO sessionDTO = new AdminSessionDTO();
        sessionDTO.setUsername("admin");
        sessionDTO.setId(lottoCounter);
        sessionDTO.setSessionToken(sessionToken);
        List<String> permissions = new ArrayList<>();
        permissions.add("CLOSE_DRAW");
        sessionDTO.setPermissions(permissions);

        adminLoginResponseDTO.setSessionToken(sessionToken);
        return adminLoginResponseDTO;
    }

    private RegistrationSettingsDTO getRegistrationSettingsDTO() {
        RegistrationSettingsDTO dto = new RegistrationSettingsDTO();

        List<CountryOptionDTO> countries = new ArrayList<>();
        countries.add(new CountryOptionDTO("UK", "United Kingdom"));
        countries.add(new CountryOptionDTO("IE", "Ireland"));

        dto.setCountries(countries);

        List<CurrencyOptionDTO> currencies = new ArrayList<>();
        currencies.add(new CurrencyOptionDTO("GBP", "£", "British Pound"));
        currencies.add(new CurrencyOptionDTO("Euro", "€", "Euro"));

        dto.setCurrencies(currencies);

        List<TitleOptionDTO> titles = new ArrayList<>();
        titles.add(new TitleOptionDTO("Mr"));
        titles.add(new TitleOptionDTO("Ms"));
        titles.add(new TitleOptionDTO("Mx"));
        dto.setTitles(titles);

        return dto;
    }

    private PlayerRegisterRequestDTO getPlayerRegisterRequestDTO() {
        PlayerRegisterRequestDTO dto = new PlayerRegisterRequestDTO();

        dto.setCountryCode("UK");
        dto.setDob("31/03/1985");
        dto.setEmail("joe.bloggs@gmail.com");
        dto.setFirstName("Joe");
        dto.setLastName("Bloggs");
        dto.setPassword("password");
        dto.setGender("MALE");
        return dto;
    }
*/
}
