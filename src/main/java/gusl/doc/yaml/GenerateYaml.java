package gusl.doc.yaml;

import com.google.common.base.CaseFormat;
import gusl.core.annotations.DocApi;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.core.utils.Utils;
import gusl.doc.rest.ApiPath;
import gusl.doc.yaml.dataobjects.*;
import gusl.doc.yaml.example.PlayerRegisterRequestDO;
import org.apache.commons.io.FileUtils;
import org.reflections.Reflections;

import javax.ws.rs.*;
import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.util.*;
import java.util.stream.Collectors;

import static gusl.doc.BaseDocumentor.getFieldsFor;

/**
 * @author grant
 */
public class GenerateYaml {

    protected final GUSLLogger logger = GUSLLogManager.getLogger(this.getClass());
    public static final String NL = "\n";

    private final Map<String, List<Class<?>>> mapResourcesByComponent = new HashMap<>();

    private static final String BASE_FOLDER = "lm.edge.api";

    private static final String OUT_FOLDER = "/tmp/adyen/";
    private static final String PREFIX = "adyen";

    private final Set<Class<?>> theDTOClasses = new HashSet<>();

    private final Map<String, List<ApiPath>> mapPath = new HashMap<>();

    private void addToResourcesMap(String componentName, Class<?> restClass) {
        List<Class<?>> list = mapResourcesByComponent.get(componentName);
        if (list == null) {
            list = new ArrayList<>();
            mapResourcesByComponent.put(componentName, list);
        }
        list.add(restClass);
    }

    public void generate() throws Exception {

        try {
            loadRestClasses();

            logger.info("mapPath -> {}", mapPath.toString());

//            logger.info("api -> {}", mapResourcesByComponent.toString());
//            Utils.safeStream(mapResourcesByComponent.entrySet()).forEach(entry -> {
//                //final String parentDi r= 
//                Utils.safeStream(entry.getValue()).forEach(clazz -> {
//                    
//                });
//                
//            });

        } catch (Exception e) {
            logger.error("There was an error", e);
        }
    }

    //    public void generate() throws Exception {
//
//        try {
//
//            loadRestClasses();
//
//            List<StageDO> stages = generateStages();
//
//            // option 1
//            //generateYaml(stages, true);
//            StoryBuilder storyBuilder = new StoryBuilder();
//            List<UserStoriesDO> stories = storyBuilder.build();
//
//            // option 2
//            generateYamlForStories(stages, stories);
//
//        } catch (Exception e) {
//            logger.error("There was an error", e);
//        }
//    }
    @SuppressWarnings("deprecation")
    private void generateYamlForStories(List<StageDO> stages, List<UserStoriesDO> stories) throws ClassNotFoundException, IOException {
        Map<String, StageDO> map
                = stages.stream().collect(Collectors.toMap(StageDO::getUrl, item -> item));

        for (UserStoriesDO storiesDo : stories) {
            StringBuilder builder = new StringBuilder();

            builder.append("test_name: ").append(storiesDo.getDescription()).append(NL);

            builder.append("includes:").append(NL);
            builder.append("  - !include ../config.yaml").append(NL);

            builder.append("stages:").append(NL);

            builder.append(addLogin()).append(NL);

            for (UserStoryDO userStoryDo : storiesDo.getStories()) {
                StageDO stageDo = map.get(userStoryDo.getUrl());
                logger.info("--> url: {} stageDo: {}", userStoryDo.getUrl(), stageDo);

                builder.append(generateYaml(userStoryDo.getDescription(), stageDo, userStoryDo.isSaveResponse())).append(NL);
            }

            FileUtils.writeStringToFile(new File(OUT_FOLDER + "test_" + PREFIX + "_" + getName(storiesDo.getDescription()) + ".yaml"), builder.toString());

        }
    }

    public static void main(String[] args) throws Exception {
        try {
            GenerateYaml generateYaml = new GenerateYaml();
            generateYaml.generate();

        } finally {
            Thread.sleep(2000);
        }

    }

    @SuppressWarnings("unchecked")
    private StageDO createStage() {
        RequestDO request = new RequestDO();

        request.setJson(new PlayerRegisterRequestDO());
        request.setHttpMethod("POST");
        request.setUrl("/myurl");

        HeadersDO headers = new HeadersDO("application/json");
        request.setHeaders(headers);

        ResponseDO response = new ResponseDO(200);

        StageDO stageDO = new StageDO("test", request, response);

        return stageDO;
    }

    public void loadRestClasses() throws Exception {
        Reflections reflections = new Reflections(BASE_FOLDER);

        Set<Class<?>> resourceClassSet = reflections.getTypesAnnotatedWith(Path.class);

        if (resourceClassSet == null || resourceClassSet.isEmpty()) {
            throw new Exception("No Resources found");
        }

        StringBuilder builder = new StringBuilder();
        builder.append("The following resources were found:\n");
        resourceClassSet.stream().forEach((restClass) -> {
            int compIndex = restClass.getCanonicalName().indexOf(".", 3);
            String component = restClass.getCanonicalName().substring(3, compIndex);

            addToResourcesMap(component, restClass);
            builder.append("\t\tEvent: ").append(restClass.getCanonicalName()).append("\n");
        });

        logger.debug(builder.toString());

    }

    private List<StageDO> generateStages() throws Exception {

        List<StageDO> stages = mapResourcesByComponent.keySet().stream().sorted().map((key) -> {
            return mapResourcesByComponent.get(key).stream()
                    .sorted((a, b) -> a.getCanonicalName().compareTo(b.getCanonicalName()))
                    .map((clazz) -> {
                        return generateYamlClass(false, clazz, theDTOClasses, mapPath);
                    }).flatMap(l -> l.stream()).collect(Collectors.toList());
        }).flatMap(x -> x.stream()).collect(Collectors.toList());

        logger.info("stages: {}", stages);

        return stages;
    }

    public List<StageDO> generateYamlClass(boolean asTable, Class<?> clazz, Set<Class<?>> dtoClasses, Map<String, List<ApiPath>> apiPathMap) {
        @SuppressWarnings("unchecked")
        Path path = clazz.getAnnotation(Path.class);

        String basePath = "";
        if (path != null) {
            basePath = path.value();
            if (basePath.startsWith("/")) {
                basePath = basePath.substring(1);
            }
        }

        Method[] theMethods = clazz.getMethods();
        List<StageDO> stages = new LinkedList<>();

        for (Method method : theMethods) {

            String httpMethod = null;
            GET get = method.getAnnotation(GET.class);
            if (get != null) {
                httpMethod = "GET";
            }
            POST post = method.getAnnotation(POST.class);
            if (post != null) {
                httpMethod = "POST";
            }
            DELETE delete = method.getAnnotation(DELETE.class);
            if (delete != null) {
                httpMethod = "DELETE";
            }
            PUT put = method.getAnnotation(PUT.class);
            if (put != null) {
                httpMethod = "PUT";
            }

            if (httpMethod == null) {
                continue;
            }
            Path methodPath = method.getAnnotation(Path.class);
            String fullPath = basePath;
            if (methodPath != null) {
                String mPath = methodPath.value();
                if (mPath.startsWith("/")) {
                    mPath = mPath.substring(1);
                }

                if (fullPath.endsWith("/")) {
                    fullPath += mPath;
                } else {
                    fullPath += "/" + mPath;
                }
            }

            StageDO stageDO = new StageDO();
            stages.add(stageDO);

            stageDO.setName(method.getName());
            RequestDO requestDo = new RequestDO();
            stageDO.setRequest(requestDo);
            HeadersDO headers = new HeadersDO("application/json");
            requestDo.setHeaders(headers);
            requestDo.setUrl(fullPath);
            requestDo.setHttpMethod(httpMethod);

            DocApi apiAnnotation = method.getAnnotation(DocApi.class);

            if (apiAnnotation != null) {
                stageDO.setDescription(apiAnnotation.description());
            }
            stageDO.setClassName(clazz.getCanonicalName());
            stageDO.setMethodName(method.getName());
            stageDO.setUrl(fullPath);

            List<Class<?>> requestClasses = new ArrayList<>();

            int countParams = 0;
            Parameter[] chkParameters = method.getParameters();
            for (int i = 0; i < chkParameters.length; i++) {

                for (Annotation annotation : chkParameters[i].getAnnotations()) {
                    switch (annotation.annotationType().getSimpleName()) {
                        case "Context":
                        case "Suspended":
                            break;
                        default:
                            countParams++;
                    }
                }
            }

            // In case of a Generic class, the generic type have to be discovered.
            TypeVariable<?>[] genericParameters = clazz.getSuperclass().getTypeParameters();
            Type[] genericParametersDefinition = clazz.getTypeParameters();
            if (clazz.getGenericSuperclass() instanceof ParameterizedType) {
                genericParametersDefinition = ((ParameterizedType) clazz.getGenericSuperclass()).getActualTypeArguments();
            }

            // Map the generic types to its proper types for this class
            // Ex: ID -> Long, DTO -> CategoryDTO
            Map<String, String> paramTypes = new HashMap<>();
            for (int i = 0; i < genericParameters.length; i++) {
                paramTypes.put(genericParameters[i].getName(), genericParametersDefinition[i].getTypeName());
            }

            Parameter[] parameters = method.getParameters();
            Type[] parametersGen = method.getGenericParameterTypes();
            //Type[] genericParameters = theMethod.getGenericParameterTypes();
            String pathParamName = null;

            String extraName = null;

            for (int i = 0; i < parameters.length; i++) {
                Parameter parameter = parameters[i];

                boolean ignoreRow = false;

                StringBuilder annotationBuilder = new StringBuilder();
                for (Annotation annotation : parameter.getAnnotations()) {
                    if (annotationBuilder.length() > 0) {
                        annotationBuilder.append(" ");
                    }
                    switch (annotation.annotationType().getSimpleName()) {
                        case "Context":
                        case "Suspended":
                            ignoreRow = true;
                            break;
                        case "CookieParam":
                            extraName = "Session Token";
                            break;
                        case "HeaderParam":
                            extraName = "User Agent";
                            break;

                    }
                    annotationBuilder.append("@").append(annotation.annotationType().getSimpleName());

                    if ("PathParam".equals(annotation.annotationType().getSimpleName())) {
                        PathParam pathParam = (PathParam) annotation;
                        pathParamName = pathParam.value();
                    }
                }

                if (ignoreRow) {
                    continue;
                }

                String paramGenericTypeName = paramTypes.get(parametersGen[i].getTypeName());
                String paramTypeName = (paramGenericTypeName != null) ? paramGenericTypeName : parameter.getType().getName();

                if (paramTypeName.endsWith("DTO")) {
                    try {
                        Class<?> theClass = Class.forName(paramTypeName);
                        dtoClasses.add(theClass);
                        requestClasses.add(theClass);

                    } catch (ClassNotFoundException ex) {
                        logger.error("error");
                    }

                }

                // do request class
                boolean detailClass = true;
                if (detailClass) {
                    Utils.safeStream(requestClasses).forEach(requestClass -> {
                        // xx apiBuilder.append(NL).append(documentClass(requestClass, false));
                        // RequestDO requestDO = new RequestDO();
                        requestDo.setClassName(requestClass.getCanonicalName());
                        // stageDO.setRequest(requestDO);
                    });
                }

                Class<?> returnType = method.getReturnType();
                if (apiAnnotation != null && apiAnnotation.returnType() != null) {
                    returnType = apiAnnotation.returnType();
                }

                ResponseDO responseDO = new ResponseDO(200);
                stageDO.setResponse(responseDO);

                Class<?> returnClazz;
                responseDO.setClassName(returnType.getCanonicalName());

                try {
                    returnClazz = Class.forName(returnType.getCanonicalName());
                    // xx apiBuilder.append(documentClass(returnClazz, false));
                } catch (ClassNotFoundException ex) {
                    logger.error("Failed to find class for {}", returnType.getCanonicalName());
                }

                dtoClasses.add(returnType);

//                ApiPath apiPath = new ApiPath(fullPath, apiBuilder, httpMethod);
//
//                if (clazz.getCanonicalName().startsWith("lm")) {
//                    String[] split = clazz.getCanonicalName().split("\\.");
//                    if (split.length > 2) {
//                        addToPathMap(apiPathMap, split[1], apiPath);
//                    } else {
//                        addToPathMap(apiPathMap, "unknown", apiPath);
//                    }
//                } else {
//                    logger.error("Do not know what component you are: {}: {}", clazz.getCanonicalName(), clazz.getCanonicalName().startsWith("lm"));
//                }
            }
        }
        return stages;
    }

    @SuppressWarnings("deprecation")
    private void generateYaml(List<StageDO> stages, boolean withHeader) throws ClassNotFoundException, IOException {
        for (StageDO stageDo : stages) {
            StringBuilder builder = new StringBuilder();

            if (withHeader) {
                builder.append("test_name: ").append(stageDo.getRequest().getUrl()).append(NL);

                builder.append("includes:").append(NL);
                builder.append("  - !include config.yaml").append(NL);

                builder.append("stages:").append(NL);

            }
            builder.append(addLogin()).append(NL);

            generateYaml(stageDo, true);

            FileUtils.writeStringToFile(new File("/tmp/test_api_" + getName(stageDo.getRequest().getUrl()) + ".yaml"), builder.toString());

        }
    }

    private String generateYaml(StageDO stageDo, boolean saveResponse) throws ClassNotFoundException, IOException {
        return generateYaml(null, stageDo, saveResponse);
    }

    private String generateYaml(String description, StageDO stageDo, boolean saveResponse) throws ClassNotFoundException, IOException {
        logger.info("stageDo: {}", stageDo);

        StringBuilder builder = new StringBuilder();

        if (description == null) {
            builder.append("  - name: ").append(stageDo.getName()).append(NL);
        } else {
            builder.append("  - name: ").append(description).append(NL);
        }
        builder.append("    request:").append(NL);
        builder.append("      url: \"{tavern.env_vars.URL_UNDER_TEST}").append(stageDo.getRequest().getUrl()).append("\"").append(NL);
        if (stageDo.getRequest().getClassName() == null) {
            builder.append("      json:").append(NL);
        } else {
            builder.append("      json:").append(NL);
            addFields(builder, "      ", stageDo.getRequest().getClassName());
        }
        builder.append("      method: ").append(stageDo.getRequest().getHttpMethod()).append(NL);
        builder.append("      headers:").append(NL);
        builder.append("        player-token: \"{player-token_resp}\"").append(NL);
        builder.append("        session-token: \"{session-token_resp}\"").append(NL);
        builder.append("        content-type: application/json").append(NL);
        builder.append("    response:").append(NL);
        builder.append("      status_code: 200").append(NL);
        String responsePrefix = "";
        if (saveResponse) {
            builder.append("      save:").append(NL);
            responsePrefix = "  ";
        }
        builder.append(responsePrefix).append("      body:").append(NL);
        if (stageDo.getResponse().getClassName() == null) {

        } else {
            addFields(builder, responsePrefix + "      ", stageDo.getResponse().getClassName());
        }

        return builder.toString();
    }

    private void addFields(StringBuilder builder, String prefix, String className) throws ClassNotFoundException {
        try {
            Class<?> theClass = Class.forName(className);

            List<Field> fields = getFieldsFor(theClass);

            for (Field field : fields) {
                builder.append(prefix).append("  ").append(formatFieldName(field.getName())).append(NL);
//            builder.append("        email: \"{email:s}\"").append(NL);
//            builder.append("        password: \"{password:s}\"").append(NL);
            }
        } catch (ClassNotFoundException ex) {
            logger.error("class not found: {}", className);
        }
    }

    private String formatFieldName(String name) {
        //return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, name); 
        //        player-id: "{player_resp.player-id}"

        String fldName = CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_HYPHEN, name);
        switch (fldName) {
            case "player-id":
                return fldName + ": \"{player_resp.player-id}\"";
            case "email":
                return "email: \"{email:s}\"";
            case "password":
                return "password: \"{password:s}\"";

            default:
                return fldName + ": \"\"";
        }
    }

    private String getName(String url) {
        String n = url.replace("/", "_").replace(" ", "_").toLowerCase();
        return n;
    }

    private String addLogin() {
        StringBuilder builder = new StringBuilder();
        builder.append("  - name: Perform player login").append(NL);
        builder.append("    request:").append(NL);
        builder.append("      url: \"{tavern.env_vars.URL_UNDER_TEST}").append("auth/login").append("\"").append(NL);
        builder.append("      json:").append(NL);
        builder.append("        email: \"{email:s}\"").append(NL);
        builder.append("        password: \"{password:s}\"").append(NL);
        builder.append("      method: POST").append(NL);
        builder.append("      headers:").append(NL);
        builder.append("        content-type: application/json").append(NL);
        builder.append("    response:").append(NL);
        builder.append("      status_code: 200").append(NL);
        builder.append("      save:").append(NL);
        builder.append("        body:").append(NL);
        builder.append("          player-token_resp: player-token").append(NL);
        builder.append("          session-token_resp: session-token").append(NL);
        builder.append("          player_resp: player-details").append(NL);

        return builder.toString();

    }
}
