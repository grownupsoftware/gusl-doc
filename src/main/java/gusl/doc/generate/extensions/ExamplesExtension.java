/* Copyright lottomart */
package gusl.doc.generate.extensions;

import gusl.doc.lottomart.lotto.LottoExampleBuilder;
import org.asciidoctor.ast.StructuralNode;
import org.asciidoctor.extension.Reader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 *
 * @author grant
 */
public class ExamplesExtension extends AbstractMacro {

    private static boolean examplesGenerated = false;

    private static LottoExampleBuilder exampleBuilder = null;

    public ExamplesExtension(String name, Map<String, Object> config) {
        super(name, config);
    }

    @Override
    public Object process(StructuralNode parent, Reader reader, Map<String, Object> attributes) {
        String exampleName = (String) attributes.get("name");

        if (!examplesGenerated) {
            generateExamples();
        }

        StringBuilder builder = new StringBuilder(); // exampleBuilder.getExample(exampleName);
        if (builder == null) {
            logger.error("Failed to find example: {}", exampleName);
            return createBlock(parent, "pass", Arrays.asList(new String[]{"Failed to find example " + exampleName + "\n"}), attributes);

        }

        List<String> content = new ArrayList<>();
        content.addAll(Arrays.asList(builder.toString().split("\n")));
        parseContent(parent, content);

        return null;
    }

    private void generateExamples() {
        exampleBuilder = new LottoExampleBuilder();
        // exampleBuilder.buildExamples();
        examplesGenerated = true;
    }
}
