package gusl.doc.yaml.dataobjects;

/**
 *
 * @author grant
 */
public class RequestDO<T> {

    private String url;
    private T json;
    private String httpMethod;
    private HeadersDO headers;
    private String className;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public T getJson() {
        return json;
    }

    public void setJson(T json) {
        this.json = json;
    }

    public HeadersDO getHeaders() {
        return headers;
    }

    public void setHeaders(HeadersDO headers) {
        this.headers = headers;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }

}
