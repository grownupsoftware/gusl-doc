/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gusl.doc.yaml.dataobjects;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author grant
 */
public class UserStoriesDO {

    private String description;
    private List<UserStoryDO> stories = new ArrayList<>();

    public UserStoriesDO(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<UserStoryDO> getStories() {
        return stories;
    }

    public void setStories(List<UserStoryDO> stories) {
        this.stories = stories;
    }

}
