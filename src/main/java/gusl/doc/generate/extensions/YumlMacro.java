package gusl.doc.generate.extensions;

import gusl.core.utils.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.asciidoctor.ast.StructuralNode;
import org.asciidoctor.extension.Reader;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.*;

import static gusl.core.utils.StringUtils.isNotBlank;
import static gusl.doc.generate.extensions.Macro.IMAGE_DIR;
import static java.util.Objects.isNull;

/**
 * @author grant
 */
public class YumlMacro extends AbstractMacro {


    private static final String YUML_SERVER = "https://yuml.me/diagram/";

    private static int imageCounter = 1;

    private final String randomPrefix;

    public YumlMacro(String name, Map<String, Object> config) {
        super(name, config);
        turnOffSecurity();
        randomPrefix = UUID.randomUUID().toString().substring(0, 4);
    }

    @Override
    public Object process(StructuralNode parent, Reader reader, Map<String, Object> attributes) {
        String format = "scruffy";
        String tmpStr;
        String fileName = null;

        logger.debug("yuml attributes: {}", attributes);

        StringBuilder urlBuilder = new StringBuilder();
        //urlBuilder.append(YUML_SERVER);

        tmpStr = (String) attributes.get("format");
        if (isNotBlank(tmpStr)) {
            urlBuilder.append(tmpStr);
        } else {
            urlBuilder.append(format);
        }

        // LR TD
        tmpStr = (String) attributes.get("direction");
        if (isNotBlank(tmpStr)) {
            tmpStr = (String) attributes.get("dir");
        }
        if (isNotBlank(tmpStr)) {
            urlBuilder.append(";").append("dir").append(":").append(tmpStr);
        }

        tmpStr = (String) attributes.get("scale");
        if (isNotBlank(tmpStr)) {
            urlBuilder.append(";").append("scale").append(":").append(tmpStr);
        }

        tmpStr = (String) attributes.get("type");
        urlBuilder.append("/").append(tmpStr).append("/");

        tmpStr = (String) attributes.get("filename");
        if (isNotBlank(tmpStr)) {
            fileName = (String) attributes.get("filename");
        }

        StringBuilder queryBuilder = new StringBuilder();
        reader.readLines().stream().forEach((line) -> {
            if (!line.isEmpty()) {
                queryBuilder.append(line);//.replace("\n", ","));
            }
        });

        logger.debug("--YUML--> URL: {}", urlBuilder.toString());

        // urlBuilder.append(queryBuilder.toString());

        //scale:180
        //scruffy;dir:LR;scale:180
        // so https://yuml.me/diagram/scruffy;dir:LR;scale:180/usecase/
        //nofunky , plain, scruffy
        String imageName;
        try {
            // imageName = saveImage(urlBuilder.toString());
            String actualFilename;
            if (isNull(fileName)) {
                actualFilename = "yuml_" + randomPrefix + "_" + ++imageCounter + ".svg";
            } else {
                actualFilename = fileName;
                if (actualFilename.indexOf(".svg") < 0) {
                    actualFilename += ".svg";
                }
            }
            imageName = sendPOST(urlBuilder.toString(), actualFilename, queryBuilder.toString());
            List<Map<String, Object>> params = new ArrayList<>();
            Map<String, Object> paramValues = new HashMap<>();

            paramValues.put("target", "generated/" + actualFilename);

            return createBlock(parent, "image", "", paramValues);
        } catch (Throwable ex) {
            logger.error("Failed to create yuml images", ex);
            return createBlock(parent, "pass", Arrays.asList(new String[]{"Failed to create yuml image\n"}), attributes);
        }
    }

//    private String saveImage(String urlString) throws MalformedURLException, IOException {
//
//        URL url = new URL(YUML_SERVER + urlString);
//
//        String imageName = IMAGE_DIR + "yuml_" + randomPrefix + "_" + ++imageCounter + ".svg";
//
//        logger.debug("yuml saving to: {} from url: {}", imageName, YUML_SERVER + URLEncoder.encode(urlString, "UTF-8"));
//
//        OutputStream out;
//        try (InputStream in = new BufferedInputStream(url.openStream())) {
//            out = new BufferedOutputStream(new FileOutputStream(imageName));
//            for (int i; (i = in.read()) != -1; ) {
//                out.write(i);
//            }
//        }
//        out.close();
//
//        return imageName;
//    }

    private String saveImage(String svgName, String filename) throws MalformedURLException, IOException {

        URL url = new URL(YUML_SERVER + svgName);

        String imageName = IMAGE_DIR + filename; // "yuml_" + randomPrefix + "_" + ++imageCounter + ".svg";

        logger.debug("yuml saving to: {} from url: {}", imageName, YUML_SERVER + URLEncoder.encode(svgName, "UTF-8"));

        OutputStream out;
        try (InputStream in = new BufferedInputStream(url.openStream())) {
            out = new BufferedOutputStream(new FileOutputStream(imageName));
            for (int i; (i = in.read()) != -1; ) {
                out.write(i);
            }
        }
        out.close();

        return imageName;
    }

    private String sendPOST(String url, String filename, String uml) throws IOException {
        String imageName = IMAGE_DIR + filename;

        String result = "";
        HttpPost post = new HttpPost(YUML_SERVER + url);

        // add request parameters or form parameters
        List<NameValuePair> urlParameters = new ArrayList<>();
        urlParameters.add(new BasicNameValuePair("dsl_text", uml));

        post.setEntity(new UrlEncodedFormEntity(urlParameters));

        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(post)) {

            if (response.getStatusLine().getStatusCode() == 200) {
                result = EntityUtils.toString(response.getEntity());
            } else {
                logger.error("Error creating file: {} uml: {}", filename, uml);
            }
        }

        if (StringUtils.isNotBlank(result)) {
            logger.debug("saving: file {} result: {}", imageName, result);
            saveImage(result, filename);
        }

        // FileUtils.write(new File(imageName), result);

        return imageName;
    }

    /**
     * using https to go to yuml server - don't want cert validation
     */
    private void turnOffSecurity() {
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            @Override
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }

            @Override
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }};

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
        }
    }
}
