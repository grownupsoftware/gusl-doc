/* Copyright lottomart */
package gusl.doc.jira;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.Collectors;

public class PropertiesClient {

    public static final String CONSUMER_KEY = "consumer_key";
    public static final String PRIVATE_KEY = "private_key";
    public static final String REQUEST_TOKEN = "request_token";
    public static final String ACCESS_TOKEN = "access_token";
    public static final String SECRET = "secret";
    public static final String JIRA_HOME = "jira_home";

    private final static Map<String, String> DEFAULT_PROPERTY_VALUES = ImmutableMap.<String, String>builder()
            .put(JIRA_HOME, "https://lottomart.atlassian.net")
            .put(CONSUMER_KEY, "OauthKey")
            .put(PRIVATE_KEY, "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDuKHw+qve3RSKOC3rjMEH4R8tlpUB7xAfZlnruBDqkLa2pyGbNeGvkRZkYQ1U2UmPXg/4nKZgoxhVNRWQjoD1jO7dMEjxusU5KLtLZgC22+VK89gBz6kKjD2X8ECUZIS3HvsNVWkhgV35ex54oNHx6ysBtCklaRwOYeMauGl6vy2MehLY3iQGCWHqbuct+BpU4UL9olC2O0xZiOsP5td6JizytTSOY6hK/ypT1Y3Ovy/88J9ncpmzwwUL02AsszcwRjTtH4kBiIIs9mWjMXLwGEEBAIycD5zFR07xMDO3o6nZpvHiMi5kVY+T7PuxfCkYWK2WDyxY8DoUj9DMPxBbtAgMBAAECggEAC6dxhQOKwa4hZcAMEGgBImwo+N1gTW4nUC977n/IetY9ZEyJM07MQMxjFHvfBJx9hENSQYpYhs5CpphZsSCbQGrgqllOGcCVay3lZX1PP1/t+48yMHnrLt2HCRGlLxifi+G630deuVAuv+aWx7Lh0IRxbCSPuFo3Q9PNLSHfNkzubyjk3jiSpdpp8a7otePVuL4ZkQG+BD4ThPfTMHySR2ZSeTC4Z02FyWFuoY8MaS+SOQbzGEKMTEXI21CNup8LhDN3+TeFL5lnfI6D2qSoi1tfY0fS8XxR+gbRUFOTZfp1fiMsNNVmZViUzqJIVdhvnp/1mZXVge4b6lKPuA/VpQKBgQD4rnisTm4yTLBGUy01A1jdeRxPjFqz06yfs/5OO7d6zg1VZKTKbHumzezQ1Cr3qnHy2MO/SEjR0bitC6f/ZX1gNWBNbWTpAyefHbzA9IpOfAJZ+NNa7Z5DP+pKYOm0gUaEFniDrIVYHJ5PJ/PxDiUDnAwKIbN7lRSvsOHhcGbLWwKBgQD1Krt+IIxU4hMvucDXjY29IQFS9Hd/es23fsrbGHR4VGOmqgdz6ch1IedYIHKt+Q9hCQPTHn34c4/hGTW1yuyg7J7n5ETK8td3+01Bp2gqUziL8aRk7Y2K+p6KpyKbi7ud+2eH/iZntRTrYYjZTNe7aFrjhZRxj54gc27A2W7hVwKBgAJLsgd9Ld0/Af2UNP2hS0e4H3/IfuncEWLkCXv1Zys2Db91D2Ri/f07N21yF/dzVlv0jlIMu8dcFUVbrzBXYoHp3Dq6wMy537rDToPFfiVdVbQ47NKREP3z8BjBrkahTuJXRsOIay62DOwUJVdbVxIidaX+S6bfaHJ6wosPO5OzAoGBAOVXyzsKwZvqPJ6Jur8p+SyygmWSsXK8c1KzNprMfq/N6caQChbB3LbON3c2K3FZqqxQXZaSUK8pk6+0AI2GPeOwVlqBLGVZ1Hy1xiijrQri/OPU8b9EhgM7vTHhdbtidOkttEUAV/bt65rhi74TZ0A2N8fdPucYvYiH01RuUo5TAoGAJGQfAllzTeuxsbzHSAy81qAgs12RQajX9huoQjPEyTkxEHGab6S12z4wbOspcTv7ji3b+oDq1HbCpWx/ArrI3Pp9esI849uymVryaBd5v/etDquqNTM/ya6dSnHJmrn9sSJDBPFAm71bk3aAUcbWWshPMAC785BIVjsFIqTD3jc=")
            .build();

    private final String fileUrl;
    private final String propFileName = "config.properties";

    public PropertiesClient() throws Exception {
        fileUrl = "./" + propFileName;
    }

    public Map<String, String> getPropertiesOrDefaults() {
        try {
            Map<String, String> map = toMap(tryGetProperties());
            map.putAll(Maps.difference(map, DEFAULT_PROPERTY_VALUES).entriesOnlyOnRight());
            return map;
        } catch (FileNotFoundException e) {
            tryCreateDefaultFile();
            return new HashMap<>(DEFAULT_PROPERTY_VALUES);
        } catch (IOException e) {
            return new HashMap<>(DEFAULT_PROPERTY_VALUES);
        }
    }

    private Map<String, String> toMap(Properties properties) {
        return properties.entrySet().stream()
                .filter(entry -> entry.getValue() != null)
                .collect(Collectors.toMap(o -> o.getKey().toString(), t -> t.getValue().toString()));
    }

    private Properties toProperties(Map<String, String> propertiesMap) {
        Properties properties = new Properties();
        propertiesMap.entrySet()
                .stream()
                .forEach(entry -> properties.put(entry.getKey(), entry.getValue()));
        return properties;
    }

    private Properties tryGetProperties() throws IOException {
        InputStream inputStream = new FileInputStream(new File(fileUrl));
        Properties prop = new Properties();
        prop.load(inputStream);
        return prop;
    }

    public void savePropertiesToFile(Map<String, String> properties) {
        OutputStream outputStream = null;
        File file = new File(fileUrl);

        try {
            outputStream = new FileOutputStream(file);
            Properties p = toProperties(properties);
            p.store(outputStream, null);
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            closeQuietly(outputStream);
        }
    }

    public void tryCreateDefaultFile() {
        System.out.println("Creating default properties file: " + propFileName);
        tryCreateFile().ifPresent(file -> savePropertiesToFile(DEFAULT_PROPERTY_VALUES));
    }

    private Optional<File> tryCreateFile() {
        try {
            File file = new File(fileUrl);
            file.createNewFile();
            return Optional.of(file);
        } catch (IOException e) {
            return Optional.empty();
        }
    }

    private void closeQuietly(Closeable closeable) {
        try {
            if (closeable != null) {
                closeable.close();
            }
        } catch (IOException e) {
            // ignored
        }
    }
}
