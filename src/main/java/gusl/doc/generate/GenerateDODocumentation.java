package gusl.doc.generate;

import gusl.doc.AbstractDocumentation;
import gusl.doc.dataobjects.DataObjectsDocumentor;
import gusl.doc.exceptions.DocException;
import org.apache.commons.cli.*;


/**
 *
 * @author grant
 */
public class GenerateDODocumentation extends AbstractDocumentation {

    private static final String OUTPUT_FILE = "generated/dataobjects.adoc";

    public GenerateDODocumentation(String srcRootDir) {
        super(OUTPUT_FILE, new DataObjectsDocumentor(srcRootDir));
    }

    public static void main(String... args) throws Exception {

        Options options = new Options();
        options.addOption("src_root_dir", true, "Source Root Directory");

        String srcRootDir;
        try {
            CommandLineParser parser = new DefaultParser();
            CommandLine commandLine = parser.parse(options, args);
            srcRootDir = commandLine.getOptionValue("src_root_dir");
            if (srcRootDir == null || srcRootDir.isEmpty()) {
                logger.error("No root project directory specified");
                throw new DocException("No root project directory specified");
            }

            logger.info("Project directory: {}", srcRootDir);
        } catch (ParseException ex) {
            throw new DocException("Error parsing options");
        }

        GenerateDODocumentation generator = new GenerateDODocumentation(srcRootDir);
        generator.generate();
    }

}
