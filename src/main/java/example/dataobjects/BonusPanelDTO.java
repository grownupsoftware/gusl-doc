package example.dataobjects;

import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;

@DocClass(description = "Bonus panel")
public class BonusPanelDTO {
    @DocField(description = "Unique Id")
    private Long id;

    @DocField(description = "Name")
    private String name;

}
