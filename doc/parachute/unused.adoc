
* Qualification
** Player
** Deposit
** Usage

NOTE: Not all players want bonus funds nor free spins! And *always* need to opt in.



*Player qualification*
[width=100%",options="header"]
|===
| Terms |
| Registration |
| First deposit by new customers only |
| Any depositing customer |
| Bonus available for first product vertical deposit |
|===

*Deposit qualification*
[width=100%",options="header"]
|===
| Bonus not available for specific payment providers |
| First X deposits |
| Min deposit |
|===

NOTE: Aware that Lottoland would like to offer bonus for specific payment providers

*Usage qualification*
[width=100%",options="header"]
|===
| Accept time limit |
| Usage time limit |
|===

*Deposit amount*
[width=100%",options="header"]
|===
| Min amount |
|===

*Deposit frequency*
[width=100%",options="header"]
|===
| Accept time limit |
| Usage time limit |
|===


|===
| Deposit bonus and Free Spin combinations |
| 100 % of deposit (with maximum) |
| X x Conversion |
| X Wagering|
| Selected games for bonus funds|
| FS wins added to bonus balance (with or w/out wagering)|
| Weighted game contribution for wagering|
| Max winnings|
| Max bonus betting limit (fixed price or % of bonus balance)|
| Max bonus across multiple deposits|
| Cashback on losses, specific game, specific time frame (24 hrs) max cashback|
| Player journey Free Spins spread over X days.|
| Player journey Bonus funds spread over X days.|
|===

Free Spin specific

[width=100%",options="header"]
|===
| Terms |
| Selected games for Free Spins |
| Specific coin value for Free Spins |
| FS spins capped |
|===


|===
| Deposit bonus and Free Spin combinations |
| 100 % of deposit (with maximum) |
| X x Conversion |
| X Wagering|
| Selected games for bonus funds|
| FS wins added to bonus balance (with or w/out wagering)|
| Max winnings|
| Max bonus betting limit (fixed price or % of bonus balance)|
| Max bonus across multiple deposits|
| Cashback on losses, specific game, specific time frame (24 hrs) max cashback|
| Player journey Free Spins spread over X days.|
| Player journey Bonus funds spread over X days.|
|===

Free Spin specific

[width=100%",options="header"]
|===
| Terms |
| Selected games for Free Spins |
| Specific coin value for Free Spins |
| FS spins capped |
|===


[kroki,type="mermaid",width=600,height=200]
----
gantt
title High Level Plan
dateFormat  YYYY-MM-DD
section Onboarding
Understanding           :a1, 2020-09-11, 30d
Another task     :after a1, 20d
section Another
Task in sec      :2020-01-12, 12d
another task     :24d
----

