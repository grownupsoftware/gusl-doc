/* Copyright lottomart */
package gusl.doc.generate.extensions.angular;

import gusl.core.tostring.ToString;

/**
 * @author grant
 */
public class AngularParam {

    private String name;
    private String param;

    public AngularParam() {
    }

    public AngularParam(String name, String param) {
        this.name = name;
        this.param = param;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
