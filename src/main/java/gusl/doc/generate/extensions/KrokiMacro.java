package gusl.doc.generate.extensions;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import gusl.core.json.ObjectMapperFactory;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.core.utils.IOUtils;
import org.asciidoctor.ast.StructuralNode;
import org.asciidoctor.extension.Reader;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.jackson.internal.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import org.glassfish.jersey.message.GZipEncoder;
import org.glassfish.jersey.server.filter.EncodingFilter;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.zip.GZIPInputStream;

import static gusl.doc.generate.extensions.Macro.IMAGE_DIR;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

public class KrokiMacro extends AbstractMacro {

    protected final GUSLLogger logger = GUSLLogManager.getLogger(this.getClass());

    // https://kroki.io/

    private final Client theHttpClient;
    private final ObjectMapper theObjectMapper;

    private static final String SERVER = "https://kroki.io/";

    private static int imageCounter = 1;

    private final String randomPrefix;

    public KrokiMacro(String name, Map<String, Object> config) {
        super(name, config);
        randomPrefix = UUID.randomUUID().toString().substring(0, 4);
        // turnOffSecurity();

        theObjectMapper = ObjectMapperFactory.createObjectMapper(PropertyNamingStrategy.KEBAB_CASE);

        ClientConfig clientConfig = new ClientConfig();
        JacksonJaxbJsonProvider jacksonProvider = new JacksonJaxbJsonProvider();
        jacksonProvider.setMapper(theObjectMapper);
        clientConfig.register(jacksonProvider);

        clientConfig.register(EncodingFilter.class);
        clientConfig.register(GZipEncoder.class);
        // clientConfig.register(new GUSLLoggingFeature(logger));

        theHttpClient = ClientBuilder.newClient(clientConfig);

    }

    @Override
    public Object process(StructuralNode parent, Reader reader, Map<String, Object> attributes) {
        imageCounter++;
        try {

            String diagramType = "graphviz";
            String diagramContent = "digraph G {Hello->World}";

            String tmpStr = (String) attributes.get("type");
            if (nonNull(tmpStr)) {
                diagramType = tmpStr;
            }

            StringBuilder contentBuilder = new StringBuilder();
            reader.readLines().stream().forEach((line) -> {
                if (!line.isEmpty()) {
                    contentBuilder.append(line).append("\n");//.replace("\n", ","));
                }
            });

            Map<String, Object> paramValues = new HashMap<>();
            String krokiName = "kroki_" + randomPrefix + "_" + imageCounter + ".svg";
            String imageName = IMAGE_DIR + krokiName;

            writeToFile(getImage(diagramType, contentBuilder.toString()), imageName);

            paramValues.put("target", "generated/" + krokiName);

            return createBlock(parent, "image", "", paramValues);
        } catch (Throwable ex) {
            logger.error("Failed to create kroki image");
            return createBlock(parent, "pass", Arrays.asList(new String[]{"Failed to create kroki diagram image\n"}), attributes);
        }
    }

    private String getImage(String diagramType, String diagramContent) throws Throwable {
        Invocation.Builder invocationBuilder = getJsonBuilder(SERVER + diagramType + "/svg");

        String response = sendRequest("POST", diagramContent, invocationBuilder, String.class);
        if (isNull(response)) {
            logger.info("ERROR type: {} payload: {}", diagramType, diagramContent);
            throw new Throwable("That went well - response is null");
        }
        return response;
    }

    protected void writeToFile(String content, String fileName) {
        try {
            File file = new File(fileName);
            file.getParentFile().mkdirs();

            Files.write(Paths.get(fileName), content.getBytes());
            logger.debug("Written to: {}", file.getAbsolutePath());
        } catch (IOException ex) {
            logger.error("failed to write file: {}", fileName);
        }
    }

    public Invocation.Builder getJsonBuilder(String path) {
        WebTarget webTarget = theHttpClient.target(path);
        return webTarget.request(MediaType.TEXT_PLAIN);
    }

    private <T> T sendRequest(String method, Object payload, Invocation.Builder invocationBuilder, Class<T> klass) {
        try {

            return invocationBuilder.method(method, Entity.text(payload), klass);
        } catch (WebApplicationException ex) {
            logger.warn("WebApplicationException {} {} {} ", ex.getMessage(), ex.getResponse().getStatus(), getPossiblePayloadFromException(ex));
            return null;
        }
    }

    public <T> Entity<String> buildJsonEntity(T payload) throws JsonProcessingException {
        return Entity.json(theObjectMapper.writeValueAsString(payload));
    }

    public String getPayloadAsString(Response response) throws IOException {
        boolean gzip = response.getHeaderString("Accept-Encoding") != null;

        InputStream ins = gzip ? new GZIPInputStream((InputStream) response.getEntity()) : (InputStream) response.getEntity();
        String result = IOUtils.inputStreamAsString(ins);

        IOUtils.closeQuietly(ins);

        return result;
    }

    public String getPossiblePayloadFromException(WebApplicationException ex) {
        String result = "";

        if (ex.getResponse() != null) {
            try {
                result = getPayloadAsString(ex.getResponse());
            } catch (IOException ignore) {
            }
        }
        return result;
    }

}
