package example.ll;

import java.util.Calendar;

public enum LLPeriod {
    NONE,
    HOUR,
    DAY,
    WEEK,
    MONTH,
    YEAR;
}
