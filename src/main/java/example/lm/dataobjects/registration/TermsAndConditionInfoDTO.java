/* Copyright lottomart */
package example.lm.dataobjects.registration;

import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TermsAndConditionInfoDTO {

    @DocField(description = "The T&C's version - a combination of major.minor")
    private String version;

    @DocField(description = "The locale")
    private String locale;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
