package example.lm.dataobjects.profile;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum VerificationType {
    KYC_BEFORE_FTD(false), //(was BLOCKING) - requires KYC before deposit / purchase
    KYC_TIMED(true);    //(was NON-BLOCKING) -  deposit, purchase and play allowed within certain timeframe.

    private final boolean allowFirstPurchaseOnPlayerVerify;
}
