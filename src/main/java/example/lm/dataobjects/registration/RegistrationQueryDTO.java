package example.lm.dataobjects.registration;

import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

@DocClass(description = "Pre registration request")
public class RegistrationQueryDTO {

    @DocField(description = "Relevant countries and currencies for registration")
    private RegistrationInfoDTO registrationInfo;

    @DocField(description = "A series of regex for UI form validation")
    private RegistrationRegexDTO registrationRegex;

    @DocField(description = "Latest (and relevant) T&Cs version for registration")
    private String termsVersion;

    @DocField(description = "Country Code detected by IP address")
    private String countryHint;

    @DocField(description = "Currency Code associated with country code")
    private String currencyHint;

    @DocField(description = "Supported payment types")
    private List<PaymentTypeRegistrationItem> paymentTypes;

}
