package gusl.doc;

/**
 *
 * @author grant
 */
public interface Generator {

    public void generate(String outputFile);
}
