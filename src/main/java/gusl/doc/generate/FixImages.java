/* Copyright lottomart */
package gusl.doc.generate;

import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

/**
 *
 * @author grant
 */
public class FixImages {

    public static final GUSLLogger logger = GUSLLogManager.getLogger(FixImages.class);

    public void fixImages() throws MalformedURLException, IOException {

        File folder = new File("src/main/resources/webapp/doc/img/player");
        File[] listOfFiles = folder.listFiles();

        for (File file : listOfFiles) {

            if (file.getName().endsWith(".png")) {
                BufferedImage image = null;

                logger.info("file: {}", file.getAbsolutePath());
                image = ImageIO.read(file);

                ImageIO.write(image, "png", new File("src/main/resources/webapp/doc/img/new-player/" + file.getName()));
            }

        }
    }

    public static void main(String... args) throws Exception {
        try {
            FixImages fixImages = new FixImages();
            fixImages.fixImages();
        } finally {
            Thread.sleep(2000);
        }

    }

}
