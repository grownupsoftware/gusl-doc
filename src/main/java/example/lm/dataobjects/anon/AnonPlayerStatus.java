package example.lm.dataobjects.anon;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author grant
 */
public enum AnonPlayerStatus {

    ANON("ANON"),
    LOGIN("LOGIN"),
    REGISTERED("REGISTERED");

    private final String value;

    AnonPlayerStatus(String value) {
        this.value = value;
    }

    public String getName() {
        return value;
    }

    public static List<String> names() {
        List<String> result = new ArrayList<>();
        for (AnonPlayerStatus val : values()) {
            result.add(val.name());
        }
        return result;
    }

}
